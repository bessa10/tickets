function liberaSlaCategoria(cid) {
    if ($("#"+cid).is(':checked') ){

        $("#input1_"+cid).prop("disabled", false);
        $("#input2_"+cid).prop("disabled", false);
        $("#plan_atendimento_"+cid).prop("disabled", false);
        $("#plan_solucao_"+cid).prop("disabled", false);

    } else {

        $("#input1_"+cid).prop("disabled", true);
        $("#input2_"+cid).prop("disabled", true);
        $("#plan_atendimento_"+cid).prop("disabled", true);
        $("#plan_solucao_"+cid).prop("disabled", true);
    }
}

function planAtend(cid) {
	
	if ($("#plan_atendimento_"+cid).is(':checked') ){

        $("#input1_"+cid).val(0);

    } else {

    	$("#input1_"+cid).val('');
    }

}

function planSolu(cid) {
	
	if ($("#plan_solucao_"+cid).is(':checked') ){

        $("#input2_"+cid).val(0);

    } else {

    	$("#input2_"+cid).val('');
    }

}

function detalhes_modal(tid)
{
    var url ='';

    url = window.location.href.replace('cronograma', 'cronograma/detalhes_tickets');

     $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            tid: tid
        },
        success : function(data) {

            console.log(data);
            var html = '';
            var erro = '';
            var check = data[1]['ordem'];

            html += '';
            html += '<table class="table table-striped">'
            html += '<thead>';
            html += '    <tr>';
            html += '        <th class="text-center">Fase</th>';
            html += '        <th>Em progresso</th>';
            html += '        <th>Entregue C.P</th>';
            html += '        <th>Horas previstas</th>';
            html += '        <th>Horas utilizadas</th>';
            html += '        <th>Horas ut. semana</th>';
            html += '        <th class="text-center">Ultima alocação</th>';
            html += '        <th class="text-center">Responsável</th>';
            html += '    </tr>';
            html += '</thead>';

            html += '<tbody>';

            html += '<tr>';
            html += '<td class=""><strong>1. Análise da Demanda</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 1? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>';      
            html += '<td><div class="form-check form-check-inline"><input '+(check == 2 || check == 3 || check == 4 || check == 5 || check ==  6 ? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td class="text-center">'+(data[0][2].previsao_analise != null?data[0][2].previsao_analise:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][2].horas_utilizadas != null?data[0][2].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][2].horas_semanais != null?data[0][2].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][2].ultima_locacao != null?data[0][2].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][2].responsavel != null?data[0][2].responsavel :'-')+'</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<td class=""><strong>2. Estudo técnico</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 2? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td><div class="form-check form-check-inline"><input '+(check == 3 || check == 4 || check == 5 || check ==  6 ? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td class="text-center">'+(data[0][3].previsao_estudo != null?data[0][3].previsao_estudo:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][3].horas_utilizadas != null?data[0][3].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][3].horas_semanais != null?data[0][3].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][3].ultima_locacao != null?data[0][3].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][3].responsavel != null?data[0][3].responsavel:'-')+'</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<td class=""><strong>3. Desenvolvimento</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 3? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td><div class="form-check form-check-inline"><input '+(check == 4 || check == 5 || check ==  6 ? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>';
            html += '<td class="text-center">'+(data[0][0].previsao_dev != null?data[0][0].previsao_dev:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][0].horas_utilizadas != null?data[0][0].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][0].horas_semanais != null?data[0][0].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][0].ultima_locacao != null?data[0][0].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][0].responsavel != null?data[0][0].responsavel:'-')+'</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<td class=""><strong>4. Homologação</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 4? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td><div class="form-check form-check-inline"><input '+(check ==  6 ? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>';
            html += '<td class="text-center">'+(data[0][1].previsao_homolog != null?data[0][1].previsao_homolog:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][1].horas_utilizadas != null?data[0][1].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][1].horas_semanais != null?data[0][1].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][1].ultima_locacao != null?data[0][1].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][1].responsavel!= null?data[0][1].responsavel:'-')+'</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<td class=""><strong>5. Ajustes</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 5? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td><div class="form-check form-check-inline"><input '+(check ==  6 ? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>';
            html += '<td class="text-center">'+(data[0][5].previsao_ajustes != null?data[0][5].previsao_ajustes:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][5].horas_utilizadas != null?data[0][5].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][5].horas_semanais != null?data[0][5].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][5].ultima_locacao != null?data[0][5].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][5].responsavel != null?data[0][5].responsavel:'-')+'</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<td class=""><strong>6. Op. assistida</strong></td>';
            html += '<td><div class="form-check form-check-inline"><input '+(check == 6? 'checked': '') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>'; 
            html += '<td><div class="form-check form-check-inline"><input '+('') +'  class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" disabled></div></td>';
            html += '<td class="text-center">'+(data[0][4].previsao_op_assistida != null?data[0][4].previsao_op_assistida:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][4].horas_utilizadas != null?data[0][4].horas_utilizadas:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][4].horas_semanais != null?data[0][4].horas_semanais:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][4].ultima_locacao != null?data[0][4].ultima_locacao:'0')+'</td>';
            html += '<td class="text-center">'+(data[0][4].responsavel != null?data[0][4].responsavel:'-')+'</td>';
            html += '</tr>';
            
            html += '</tbody>';
            html += '</table>';
            if(check == 4){
                if(data[2]!= 0){
                    html += '<p>* a etapa homologação iniciou em '+data[0][1].primeira_locacao+' e está há '+data[2]+' horas úteis nessa etapa.</p>'; 
                }else{
                    html += '<p>* a etapa homologação iniciou em '+data[0][1].primeira_locacao+' e está há '+data[4]+' minutos nessa etapa.</p>'; 
                }
               
            }
            if(check == 6){
                if(data[2]!= 0){
                  html += '<p>* o ticket ficou em homologação por '+data[2]+' horas.</p>';   
              }else{
                html += '<p>* o ticket ficou em homologação por '+data[4]+' minutos.</p>'; 
              }if(data[3] != 0){ 
                html += '<p>* o ticket está em operação assistida há '+(data[3] != null?data[3]:0)+' horas.</p>';
              }else{
                html += '<p>* o ticket está em operação assistida há '+(data[5] != null?data[5]:0)+' minutos.</p>';
              }
            }

            $("#txt_tid_detalhes").html(tid);
            $("#txt_classificacao").html(data[6].classificacao != null?data[6].classificacao:'');
            $("#div_table_detalhe2").html(html);
        }
    });  
}