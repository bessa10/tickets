$(document).ready(function(){
    $('#tabela-atribuido').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 15,
        "lengthMenu": false,
        "order": [[ 0, "desc" ]]
    });

    $('#tabela-users').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 15,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });

    $('#tabela-kanban').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 15,
        "lengthMenu": false,
        "order": [[ 0, "desc" ]]
    });

    
    $('#tabela-nao-atribuido').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 15,
        "lengthMenu": false,
        "order": [[ 0, "desc" ]]
    });


    $('#tabela-priorizacao').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 1000,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });
});