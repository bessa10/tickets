<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        $this->load->model('Projects_Model');
        $this->load->model('Categories_Model');
        $this->load->library('form_validation');
    }

    public function create()
    {
        $this->Roles_Model->check_permission('project', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('name', 'Nome do projeto', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $pid = $this->Projects_Model->add_project($form);
                $pid_nome = $this->Projects_Model->get_project($pid);

                $cids = $this->input->post('categoria');
                $atendimento = $this->input->post('atendimento');
                $solucao = $this->input->post('solucao');

                $plan_atend     = $this->input->post('plan_atendimento');
                $plan_soluc     = $this->input->post('plan_solucao');

                if(count($cids) > 0) {
                    for ($i = 0; $i < count($cids); $i++) {
                        $cid_nome = $this->Categories_Model->get_category($cids[$i]);
                        $dados                  = array();
                        $dados['cid']               = $cids[$i];
                        $dados['cid_nome']          = $cid_nome->name;
                        $dados['pid']               = $pid;
                        $dados['pid_nome']          = $project->name;
                        // atendimento
                        $dados['txt_atendimento']   = ($atendimento[$i] == 0) ? 'planejado' : 'até '.$atendimento[$i].'hs úteis';
                        $dados['atendimento']       = $atendimento[$i];
                        $dados['plan_atendimento']  = ($atendimento[$i] == 0) ? 1 : 0;
                        // solução
                        $dados['plan_solucao']      = ($solucao[$i] == 0) ? 1 : 0;
                        $dados['txt_solucao']       = ($solucao[$i] == 0) ? 'planejado' : 'até '.$solucao[$i].'hs úteis';
                        $dados['solucao']           = $solucao[$i];

                        $this->Projects_Model->set_categories_projects($pid, $dados);
                    }
                }
                $this->session->set_flashdata(array(
                    'type' => 'success',
                    'msg' => 'Projeto adicionado com sucesso'
                ));

                echo site_url('project/all');
                redirect(site_url('project/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }

        $title = 'Novo Projeto';
        $this->load->view('project/create', compact('title'));
    }

    public function edit($pid)
    {
        $project = $this->Projects_Model->get_project($pid);
        $this->Roles_Model->check_permission('project', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('name', 'Nome do projeto', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Projects_Model->set_project($pid, $form);

                $this->Projects_Model->remove_categories_projects($pid);

                $cids           = $this->input->post('categoria');
                $atendimento    = $this->input->post('atendimento');
                $solucao        = $this->input->post('solucao');

                $plan_atend     = $this->input->post('plan_atendimento');
                $plan_soluc     = $this->input->post('plan_solucao');

                if(count($cids) > 0) {
                    for ($i = 0; $i < count($cids); $i++) {
                        $cid_nome = $this->Categories_Model->get_category($cids[$i]);
                        $dados                      = array();
                        $dados['cid']               = $cids[$i];
                        $dados['cid_nome']          = $cid_nome->name;
                        $dados['pid']               = $pid;
                        $dados['pid_nome']          = $project->name;
                        // atendimento
                        $dados['txt_atendimento']   = ($atendimento[$i] == 0) ? 'planejado' : 'até '.$atendimento[$i].'hs úteis';
                        $dados['atendimento']       = $atendimento[$i];
                        $dados['plan_atendimento']  = ($atendimento[$i] == 0) ? 1 : 0;
                        // solução
                        $dados['plan_solucao']      = ($solucao[$i] == 0) ? 1 : 0;
                        $dados['txt_solucao']       = ($solucao[$i] == 0) ? 'planejado' : 'até '.$solucao[$i].'hs úteis';
                        $dados['solucao']           = $solucao[$i];

                        $this->Projects_Model->set_categories_projects($pid, $dados);
                    }
                }
                $this->session->set_flashdata(array(
                    'type' => 'success',
                    'msg' => 'Projeto editado com sucesso'
                ));
                redirect(site_url('project/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }


        $title = 'Editar Projeto';
        $this->load->view('project/edit', compact('project', 'title'));
    }

    public function remove($cid)
    {
        $this->Roles_Model->check_permission('project', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->Projects_Model->remove_project($cid);
        }
        $this->session->set_flashdata(array(
            'type' => 'success',
            'msg' => 'Projeto deletado com sucesso'
        ));
        redirect(site_url('project/all'));
    }

    public function all()
    {
        $projects = $this->Projects_Model->get_projects();

        $title = 'Projetos';
        $this->load->view('project/all', compact('projects', 'title'));
    }

}
