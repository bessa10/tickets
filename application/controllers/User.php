<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function all()
    {
        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        if($_POST) {
            $filtro = $_POST;
        } else {
            $filtro = null;
        }

        $users = $this->Users_Model->get_users(false, $filtro);
        $roles = $this->Roles_Model->get_roles();

        $title = 'Usuários';
        $this->load->view('user/all', compact('users', 'title', 'filtro', 'roles'));
    }

    public function create()
    {
        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        $this->load->library('form_validation');
        $this->load->model('Departments_Model');

        $this->form_validation->set_rules('name', 'Nome', 'required');
        $this->form_validation->set_rules('username', 'Login', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Senha', 'required|min_length[6]');
        $this->form_validation->set_rules('password2', 'Confirmar senha', 'required|min_length[6]|matches[password]');
        $this->form_validation->set_rules('role', 'Perfil', 'required|numeric');

        $first_user = empty($this->Users_Model->get_users());
        if(!$first_user)
        {
            $this->Roles_Model->check_permission('user', 2);
        }

        $roles = $this->Roles_Model->get_roles();

        $departments = $this->Departments_Model->get_departments();
        

        if($this->form_validation->run() == false) {

            $title = 'Novo Usuário';
            $this->load->view('user/create', compact('roles', 'title','departments'));

        } else {

            $form = $_POST;
            $name_departments = $this->Departments_Model->get_department($form['id_department']);
            $form['department'] = $name_departments->name_dep;
            $this->Users_Model->add_user($form);

            if($first_user)
            {

                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $login = $this->Users_Model->login($username, $password);
                $data = array(
                    'name'          => $login->name,
                    'uid'           => $login->uid,
                    'username'      => $login->username,
                    'email'         => $login->email,
                    'role'          => $login->role,
                    'authenticated' => TRUE
                );

                $this->session->set_userdata($data);
                redirect('/');
            }
            else
            {
                $this->all();
                return;
            }
        }
    }

    public function login()
    {
        $this->load->library('form_validation');

        // Esquema de login utilizando os dados da rede
        if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {

            $usuario_rede = $_SERVER['PHP_AUTH_USER'];
            $senha_rede = $_SERVER['PHP_AUTH_PW'];

            $retorno = $this->Users_Model->login_rede($usuario_rede, $senha_rede);

            if($retorno) {

                $data = array(
                    'uid'           => $retorno['uid'],
                    'username'      => $retorno['username'],
                    'email'         => $retorno['email'],
                    'role'          => $retorno['role'],
                    'id_department' => $retorno['id_department'],
                    'authenticated' => TRUE
                );

                $this->session->set_userdata($data);

                // Verificando se vai redirecionar para home ou atualizar cadastro
                if($retorno['atualizar'] == true) {

                    redirect('user/edit');

                } else {

                    redirect('home');
                }

            } else {

                redirect(base_url());
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('username', 'Usuário', 'required');
            $this->form_validation->set_rules('password', 'Senha', 'required');

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if($this->form_validation->run() != FALSE)
            {
                if($login = $this->Users_Model->login($username, $password))
                {
                    $data = array(
                        'uid'           => $login->uid,
                        'username'      => $login->username,
                        'email'         => $login->email,
                        'role'          => $login->role,
                        'id_department' => $login->id_department,
                        'authenticated' => TRUE
                    );

                    $this->session->set_userdata($data);
                    redirect($_SERVER['HTTP_REFERRER']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Usuário inválido.');
                }
            }
        }

        $title = 'Log In';
        $this->load->view('user/login');
    }

    public function logout()
    {
        session_destroy();
        redirect($_SERVER['HTTP_REFERRER']);
    }

    public function edit($uid = NULL)
    {
        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        if(empty($uid) && $this->session->userdata('uid'))
        {
            $uid = $this->session->userdata('uid');
        }
        elseif(empty($uid))
        {
            show_404();
        }
        else
        {
            $this->Roles_Model->check_permission('user', 2);
        }
        $this->load->model('Departments_Model');
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $form = $_POST;
            $name_departments = $this->Departments_Model->get_department($form['id_department']);
            $form['department'] = $name_departments->name_dep;
            $this->Users_Model->set_user($form);
            redirect(site_url('user/edit/'.$uid));
        }
        $user = $this->Users_Model->get_user($uid);
        $roles = $this->Roles_Model->get_roles();
        $departments = $this->Departments_Model->get_departments();
       

        $title = 'Editar Usuário';
        $this->load->view('user/edit', compact('user', 'roles', 'title','departments'));
    }

    public function remove($uid)
    {
        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        //$this->Roles_Model->check_permission('project', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
           $this->Users_Model->remove_user($uid);
        }

        $this->session->set_flashdata(array(
            'type' => 'success',
            'msg' => 'Usuário deletado com sucesso'
        ));
        redirect(site_url('user/all'));


    }

    public function forgot_password()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $data = array(
                'uid' => $this->Users_Model->get_uid_by_email($this->input->post('email')),
                'reset_token' => uniqid(),
                'reset_token_expire' => date('y-m-d h:i:a', strtotime('+6 hours'))
            );
            $this->Users_Model->set_user($data);
            $this->load->view('user/forgot_sent');
            return;
        }

        $title = 'Forgot Password';
        $this->load->view('user/forgot', compact('title'));
    }

    public function reset_password($token)
    {
        $user = $this->Users_Model->get_user_by_token($token);
        if(!empty($user) && (strtotime('now') < strtotime($user->reset_token_expire)))
        {
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $data = array(
                    'uid' => $user->uid,
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                );
                $this->Users_Model->set_user($data);
                redirect('/login');
            }

            $title = 'Reset Password';
            $this->load->view('user/forgot_reset', array('email' => $user->email, 'title' => $title));
            return;
        }

        $title = 'Token Expired';
        $this->load->view('user/forgot_expired', compact('title'));
    }
}
