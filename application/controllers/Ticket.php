<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $url = $_SERVER['REQUEST_URI'];

        $array = explode('/', $url);

        if (!in_array('view', $array) && !in_array('comment', $array) && !in_array('encerrar_chamado_cron',$array)) {
            if (!$this->session->userdata('uid')) {
                $this->session->sess_destroy();
                redirect('login');
            }
        }


        $this->load->model('Tickets_Model');
        $this->load->model('Statuses_Model');
        $this->load->model('Categories_Model');
        $this->load->model('Projects_Model');
        $this->load->model('Versions_Model');
        $this->load->model('Attachments_Model');
        $this->load->model('Notifications_Model');
        $this->load->model('Users_Model');
        $this->load->model('Torre_Model');
        $this->load->model('Classificacao_Model');
    }

    public function all()
    {
        if (!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        if ($this->input->post()) {
            $filtro = $this->input->post();
        } else {
            $filtro = null;
        }

        $projects = $this->Projects_Model->get_projects();
        $users = $this->Users_Model->get_users();

        $tickets = $this->Tickets_Model->get_tickets($filtro);
        $pagination = array('total' => count($tickets), 'limit' => PER_PAGE);
        $tickets = $this->paginate($tickets);

        $workers = $this->Users_Model->get_users(true);

        $title = 'Todos os Tickets';
        $this->load->view('ticket/all', compact('tickets', 'pagination', 'title', 'users', 'projects', 'filtro', 'workers'));
    }

    public function me()
    {
        if ($this->input->post()) {
            $filtro = $this->input->post();
        } else {
            $filtro = null;
        }
        $projects = $this->Projects_Model->get_projects();
        $users = $this->Users_Model->get_users();
        $tickets = $this->Tickets_Model->get_tickets($filtro);
        $pagination = array('total' => count($tickets), 'limit' => PER_PAGE);
        $tickets = $this->paginate($tickets);

        $workers = $this->Users_Model->get_users(true);

        $title = 'Meus Tickets';
        $this->load->view('ticket/me', compact('tickets', 'pagination', 'title', 'users', 'projects', 'filtro', 'workers'));
    }

    public function advanced()
    {
        $users = $this->Users_Model->get_users();
        $workers = $this->Users_Model->get_users(true);
        $statuses = $this->Statuses_Model->get_statuses();
        $categories = $this->Categories_Model->get_categories();
        $projects = $this->Projects_Model->get_projects();

        if ($this->input->get()) {
            $form = array(
                'author' => $this->input->get('author'),
                'worker' => $this->input->get('worker'),
                'status' => $this->input->get('status'),
                'category' => $this->input->get('category'),
                'project' => $this->input->get('project'),
                'and-author' => $this->input->get('and-author'),
                'and-worker' => $this->input->get('and-worker'),
                'and-status' => $this->input->get('and-status'),
                'and-category' => $this->input->get('and-category'),
                'and-project' => $this->input->get('and-project'),
                'created_from' => $this->input->get('created_from'),
                'created_to' => $this->input->get('created_to'),
                'modified_from' => $this->input->get('modified_from'),
                'modified_to' => $this->input->get('modified_to'),
                'exclude' => $this->input->get('exclude')
            );
            $tickets = $this->Tickets_Model->search($form);
            $pagination = array('total' => count($tickets), 'limit' => PER_PAGE);
            $tickets = $this->paginate($tickets);

        } else {
            $pagination = null;
            $tickets = null;
        }

        $title = 'Busca Avançada';
        $this->load->view('ticket/advanced', compact('tickets', 'users', 'statuses', 'categories', 'projects', 'pagination', 'title', 'workers'));
    }

    public function view($tid)
    {
        $categories         = $this->Categories_Model->get_categories();
        $projects           = $this->Projects_Model->get_projects();
        $ticket             = $this->Tickets_Model->get_ticket($tid);
        $statuses           = $this->Statuses_Model->get_statuses();
        $last_version       = $this->Versions_Model->get_last_version($tid);
        $versions           = $this->Versions_Model->get_versions($tid);
        $next_status        = $this->Statuses_Model->get_next($ticket->sid);
        $attachments        = $this->Attachments_Model->get_attachments($tid);
        $atend_iniciado     = $this->Tickets_Model->find_atend_iniciado($tid);
        $locais_trabalho    = $this->Tickets_Model->get_locais_trabalho();
        $torre              = $this->Torre_Model->get_Torre($ticket->torre);
        $classificacao      = $this->Classificacao_Model->get_classificacao($ticket->classificacao);
        $verifica_timesheet_homolog = $this->Tickets_Model->get_timesheet_homolog_user($this->session->userdata('uid'),$tid,0);

    
        

        if(!$locais_trabalho) {
            $locais_trabalho = $this->Tickets_Model->list_locais_trabalho();
        }

        $atividades         = $this->Tickets_Model->get_atividades($ticket->cid);
        $apoio_ticket       = $this->Tickets_Model->find_apoio_ticket($tid);
        $workers_apoio      = $this->Tickets_Model->list_workes_apoio($tid);
        $users_apoio        = $this->Tickets_Model->list_workes_apoio($tid, true);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->Roles_Model->check_permission('ticket', 2);

            $form = $_POST;

            $this->Tickets_Model->set_ticket($tid, $form);
            
            if ($ticket->ticket_tid == 4) {
                $dados['status'] = 3;
                $ticket->worker = null;
                $status_dias_uteis = $this->Tickets_Model->valida_dias_uteis($tid, $dados);
            }

            $this->session->set_flashdata(array(
                'type' => 'success',
                'msg' => 'Ticket alterado com sucesso'
            ));

            redirect("/ticket/view/$tid");
        }

        $title = $ticket->title;
        $this->load->view('ticket/view', compact('ticket', 'versions', 'statuses', 'categories', 'projects', 'next_status', 'attachments', 'title', 'atend_iniciado', 'last_version', 'locais_trabalho', 'apoio_ticket', 'workers_apoio', 'atividades', 'users_apoio','verifica_timesheet_homolog','torre','classificacao'));

    }


    public function edit($tid)
    {
        $this->load->library('form_validation');

        $categories     = $this->Categories_Model->get_categories();
        $projects       = $this->Projects_Model->get_projects();
        $ticket         = $this->Tickets_Model->get_ticket($tid);
        $statuses       = $this->Statuses_Model->get_statuses();
        $versions       = $this->Versions_Model->get_versions($tid);
        $attachments    = $this->Attachments_Model->get_attachments($tid);
        $users          = $this->Users_Model->get_users();
        $workers        = $this->Users_Model->get_users(true);
        $gravidade      = $this->Tickets_Model->list_gravidade();
        $urgencia       = $this->Tickets_Model->list_urgencia();
        $tendencia      = $this->Tickets_Model->list_tendencia();
        $torre          = $this->Torre_Model->get_torres();
        $classificacao  = $this->Classificacao_Model->get_classificacoes();

        if ($ticket->author == $this->session->userdata('uid')) {
            $this->Roles_Model->check_permission('ticket', 3);
        } else {
            $this->Roles_Model->check_permission('ticket', 4);
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('title', 'Em qual problema...', 'required');
            $this->form_validation->set_rules('description', 'Por favor, conte mais sobre o problema...', 'required');
            $this->form_validation->set_rules('category', 'Categoria', 'required');
            $this->form_validation->set_rules('valor_gravidade', 'Gravidade', 'required');
            $this->form_validation->set_rules('valor_urgencia', 'Urgência', 'required');
            $this->form_validation->set_rules('valor_tendencia', 'Tendência', 'required');
            $this->form_validation->set_rules('horas_previstas', 'Horas previstas', 'is_numeric');
            $this->form_validation->set_rules('previsao_ajustes', 'Ajustes', 'is_numeric');
            $this->form_validation->set_rules('previsao_op_assistida', 'Operação assistida', 'is_numeric');

            if ($_FILES && $_FILES['attachments']['name'][0]) {
                $attachments = $this->upload_file();

                if ($attachments === FALSE) {
                    return;
                }
            }

            if ($this->form_validation->run() != FALSE) {
                $form = $_POST;
            
                $before = $ticket;
                if (!empty($attachments)) {
                    $form['attachments'] = $attachments;
                }

                // Caso o chamado for finalizado ou cancelado, iremos verificar se tem atendimento iniciado para ser encerrado
                if ($form['status'] == "-1" || $form['status'] == "4") {

                    // Verificando se existe atendimento iniciado por você no mesmo ticket
                    $atend_iniciado = $this->Tickets_Model->find_atend_iniciado($tid);

                    if ($atend_iniciado) {

                        //finalizar atendimento
                        $dados_update['tid'] = $tid;
                        $this->Tickets_Model->set_atendimento($dados_update, true);
                    }
                }

                $this->Tickets_Model->set_ticket($tid, $form);

                if (!empty($attachments)) {
                    $this->session->set_flashdata(array(
                        'type' => 'success',
                        'msg' => 'Ticket alterado com sucesso'
                    ));

                    $this->session->set_flashdata('attachments', $this->Attachments_Model->get_attachments($tid));
                    //redirect("/attachment/details/$tid");
                    redirect("/ticket/view/$tid");
                }

                $this->session->set_flashdata(array(
                    'type' => 'success',
                    'msg' => 'Ticket alterado com sucesso'
                ));

                redirect("/ticket/view/$tid");
            } else {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }

        $title = 'Edit Ticket';
        $this->load->view('ticket/edit', compact('ticket', 'versions', 'statuses', 'categories', 'projects', 'attachments', 'title', 'users', 'workers', 'gravidade', 'urgencia', 'tendencia','torre','classificacao'));
    }

    public function create()
    {
        $this->load->library('form_validation');

        $this->Roles_Model->check_permission('ticket', 3);
        $torre = $this->Torre_Model->get_torres();
        $classificacao = $this->Classificacao_Model->get_classificacoes();
        $categories = $this->Categories_Model->get_categories();
        $projects = $this->Projects_Model->get_projects();
        $projects_comum = $this->Projects_Model->get_projects_visivel();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('title', 'Em qual problema...', 'required');
            $this->form_validation->set_rules('description', 'Por favor, conte mais sobre o problema...', 'required');
            $this->form_validation->set_rules('category', 'Categoria', 'required');
            $this->form_validation->set_rules('project', 'Projeto', 'required');
            $this->form_validation->set_rules('horas_previstas', 'Horas previstas', 'is_numeric');
            $this->form_validation->set_rules('previsao_analise', 'Análise e Demanda', 'is_numeric');
            $this->form_validation->set_rules('previsao_estudo', 'Estudo Técnico', 'is_numeric');
            $this->form_validation->set_rules('previsao_dev', 'Desenvolvimento', 'is_numeric');
            $this->form_validation->set_rules('previsao_homolog', 'Homologação', 'is_numeric');
            $this->form_validation->set_rules('previsao_ajustes', 'Ajustes', 'is_numeric');
            $this->form_validation->set_rules('previsao_op_assistida', 'Operação assistida', 'is_numeric');

            if ($_FILES && $_FILES['attachments']['name'][0]) {
                $attachments = $this->upload_file();

                if ($attachments === FALSE) {
                    return;
                }
            }

            if ($this->form_validation->run() != FALSE) {
                $form = $_POST;
                //$form['author'] = $this->session->userdata('uid');

                if (!empty($attachments)) {
                    $form['attachments'] = $attachments;
                }

                $tid = $this->Tickets_Model->add_ticket($form);

                // CC i.e. subscribe others to notifications
                foreach ($this->input->post('cc') as $uid) {
                    $this->Notifications_Model->subscribe($tid, $uid);
                }

                $ticket = $this->Tickets_Model->get_ticket($tid);
                $this->Versions_Model->add_version($tid, '<i>Ticket created</i>', $ticket, $ticket);

                // Subscribe self to ticket
                $this->Notifications_Model->subscribe($tid, $this->session->userdata('uid'));

                if (!empty($attachments)) {
                    $this->session->set_flashdata('attachments', $attachments);
                    //redirect("/attachment/details/$tid");
                    redirect("/ticket/view/$tid");
                }

                redirect("/ticket/view/$tid");
            } else {
                $this->session->set_flashdata('error', 'Existem erros ao cadastrar o ticket.');
            }
        }

        $users = $this->Users_Model->get_users();
        $title = 'Novo Ticket';
        $this->load->view('ticket/create', compact('categories', 'projects', 'title', 'users','projects_comum','torre','classificacao'));
    }

    public function upload_file()
    {
        $attachments = array();
        $categories = $this->Categories_Model->get_categories();

        $this->load->library('upload');

        $files = $_FILES;
        $files_count = count($_FILES['attachments']['name']);
        for ($i = 0; $i < $files_count; $i++) {
            $_FILES['attachments']['name'] = $files['attachments']['name'][$i];
            $_FILES['attachments']['type'] = $files['attachments']['type'][$i];
            $_FILES['attachments']['tmp_name'] = $files['attachments']['tmp_name'][$i];
            $_FILES['attachments']['error'] = $files['attachments']['error'][$i];
            $_FILES['attachments']['size'] = $files['attachments']['size'][$i];

            $this->upload->initialize($this->set_upload_options());
            if (!$this->upload->do_upload('attachments')) {
                $this->session->set_flashdata('error', $this->upload->display_errors('', ''));
                $this->load->view('ticket/create', compact('categories', 'title'));
                return false;
            } else {
                $attachments[] = $this->upload->data();
            }
        }

        return $attachments;
    }

    private function set_upload_options()
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './attachments/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|csv|xls|xlsx|html|pdf';

        return $config;
    }

    public function status()
    {
        $statuses = $this->Statuses_Model->get_statuses();
        if ($this->input->get('status')) {
            $status = $this->input->get('status');
            $tickets = $this->Tickets_Model->get_by_status($status);
        } else {
            $tickets = $this->Tickets_Model->get_by_status();
        }

        $title = 'Tickets por Status';
        $this->load->view('report/status', compact('tickets', 'statuses', 'title'));
    }

    public function user()
    {
        $users = $this->Users_Model->get_users();

        if ($this->input->get('user')) {
            $user = $this->input->get('user');
            $tickets = $this->Tickets_Model->get_by_user($user);
        } else {
            $tickets = $this->Tickets_Model->get_by_user();
        }

        $title = 'Tickets por Usuário';
        $this->load->view('report/user', compact('tickets', 'users', 'title'));
    }

    public function category()
    {
        $categories = $this->Categories_Model->get_categories();
        if ($this->input->get('category')) {
            $category = $this->input->get('category');
            $tickets = $this->Tickets_Model->get_by_category($category);
        } else {
            $tickets = $this->Tickets_Model->get_by_category();
        }

        $title = 'Tickets por Categoria';
        $this->load->view('report/category', compact('tickets', 'categories', 'title'));
    }

    public function project()
    {
        $projects = $this->Projects_Model->get_projects();
        if ($this->input->get('project')) {
            $project = $this->input->get('project');
            $tickets = $this->Tickets_Model->get_by_project($project);
        } else {
            $tickets = $this->Tickets_Model->get_by_project();
        }

        $title = 'Tickets por Projeto';
        $this->load->view('report/project', compact('tickets', 'projects', 'title'));
    }

    public function paginate($results)
    {
        $page = $this->input->get('page');
        $offset = ($page - 1) * PER_PAGE;
        $length = PER_PAGE;
        $pages = ceil(count($results) / $length);
        if ($page) {
            return array_slice($results, $offset, $length);

        }
        return array_slice($results, 0, $length);
    }

    public function kanban()
    {
        $statuses = $this->Statuses_Model->get_statuses();
        $kanban_tickets = $this->Tickets_Model->get_kanban_tickets();

        $tickets_chamados = $this->Tickets_Model->get_kanban_tickets();

        $tickets_desenvolvimento = $this->Tickets_Model->get_kanban_tickets(true);

        $title = 'Kanban';
        $this->load->view('ticket/kanban', compact('kanban_tickets', 'statuses', 'title', 'tickets_chamados', 'tickets_desenvolvimento'));
    }

    public function set_status()
    {
        $this->load->library('form_validation');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('tid', 'Ticket ID', 'required');
            $this->form_validation->set_rules('status', 'Ticket status', 'required');

            if ($this->form_validation->run() != FALSE) {
                $tid = $this->input->post('tid');
                $status = $this->input->post('status');
                $status = $this->Statuses_Model->get_status_by_label($status);
                $data = array('status' => $status->sid);
                $this->Tickets_Model->set_ticket($tid, $data);
                echo json_encode(array('success' => 'Ticket status changed successfully.'));
            } else {
                echo json_encode(array('error' => 'Missing Ticket ID or target status.'));
            }
        }
    }

    public function inicia_atendimento($tid = null)
    {
        $this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('tid', 'Ticket ID', 'required');
            $this->form_validation->set_rules('tipo', 'Tipo operação', 'required');

            if ($this->form_validation->run() != FALSE) {

                $tid = $this->input->post('tid');
                $tipo = $this->input->post('tipo');

                $form = $_POST;

                //var_dump($form);die;

                // Verificando se existe atendimento iniciado por você no mesmo ticket
                $atend_iniciado = $this->Tickets_Model->find_atend_iniciado($tid);
                $ticket = $this->Tickets_Model->get_ticket($tid);

                if ($tipo == 1) {

                    # Inicia atendimento
                    if ($atend_iniciado) {

                        $msg    = 'Você já começou o atendimento para este ticket.';
                        $type   = 'danger';
                        //$retorno = array('type' => 'danger', 'msg' => 'Você já começou o atendimento para este ticket.');

                    } else {
                        // parar atendimento de outro ticket
                        $this->Tickets_Model->fechar_atendimento_outro_ticket($tid);

                        // Gravar inicio atendimento
                        $this->Tickets_Model->set_atendimento($form, false);

                        // Caso não for atendimento de apoio iremos verificar as questões abaixo
                        if($form['user_apoio'] != "1") {
                            
                            if ($ticket->started == null) {
                                $dados['started'] = date('Y-m-d H:i:s');
                                $this->Tickets_Model->set_ticketid($tid, $dados);
                            }

                            if ($ticket->ticket_tid == 1){
                                $dados['status'] = 3;
                                $this->Tickets_Model->set_ticketid($tid, $dados);
                            }

                            if ($ticket->worker == null) {
                                $this->Tickets_Model->set_worked($tid);
                            }
                        }

                        $msg    = 'Atendimento iniciado.';
                        $type   = 'success';
                        
                        //return array('type' => 'success', 'msg' => 'Atendimento iniciado.');
                    }

                    $this->session->set_flashdata(array(
                        'type'  => $type,
                        'msg'   => $msg
                    ));

                    redirect("/ticket/view/$tid");

                } elseif ($tipo == 2) {
                    // Finaliza atendimento
                    if ($atend_iniciado) {

                        //finalizar atendimento
                        $this->Tickets_Model->set_atendimento($form, true);

                        $retorno = array('type' => 'success', 'msg' => 'Atendimento parado.');

                    } else {

                        $retorno = array('type' => 'danger', 'msg' => 'Não foi possível parar o atendimento, por favor tente novamente.');
                    }

                } else {

                    $retorno = array('type' => 'danger', 'msg' => 'Operação inválida - 001');
                }

                echo json_encode($retorno);
                die;

            } else {

                $retorno = array('type' => 'danger', 'msg' => 'Operação inválida - 002');

                echo json_encode($retorno);
                die;
            }
        }
    }

    public function comment($tid)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('comment', 'de atendimento', 'required');

        $statuses = $this->Statuses_Model->get_statuses();
        $categories = $this->Categories_Model->get_categories();
        $projects = $this->Projects_Model->get_projects();
        $ticket = $this->Tickets_Model->get_ticket($tid);
        $last_version = $this->Versions_Model->get_last_version($tid);
        $versions = $this->Versions_Model->get_versions($tid);
        $next_status = $this->Statuses_Model->get_next($ticket->sid);
        $users = $this->Users_Model->get_users();
        $workers = $this->Users_Model->get_users(true);
        $attachments = $this->Attachments_Model->get_attachments($tid);
        $atend_iniciado = $this->Tickets_Model->find_atend_iniciado($tid);

        $ticket = $this->Tickets_Model->get_ticket($tid);

        if ($this->form_validation->run() != FALSE) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->Roles_Model->check_permission('ticket', 2);
                $form = $_POST;
                $this->Tickets_Model->set_ticket($tid, $form);

                if ($ticket->ticket_tid == 4) {
                    $dados['status'] = 3;
                    $status_dias_uteis = $this->Tickets_Model->valida_dias_uteis($tid, $dados);

                }


                $this->session->set_flashdata(array(
                    'type' => 'success',
                    'msg' => 'Ticket alterado com sucesso'
                ));

                redirect("/ticket/view/$tid");
            }
        } else {
            //$this->session->set_flashdata('error', 'Existem erros no preenchimento.');
        }

        $title = $ticket->title;
        $this->load->view('ticket/comment', compact('ticket', 'versions', 'users', 'statuses', 'categories', 'projects', 'next_status', 'attachments', 'title', 'workers', 'atend_iniciado', 'last_version'));

    }

    public function ultimo_registro_timesheet()
    {
        $this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('tid', 'Ticket ID', 'required');
            $this->form_validation->set_rules('tipo', 'Tipo operação', 'required');

            if ($this->form_validation->run() != FALSE) {

                $tid = $this->input->post('tid');
                $tipo = $this->input->post('tipo');

                $form = $_POST;

                // Verificando se existe atendimento iniciado
                $atend_iniciado = $this->Tickets_Model->find_atend_iniciado($tid);


            } else {

                $retorno = array('type' => 'danger', 'msg' => 'Operação inválida');

                echo json_encode($retorno);
                die;
            }
        }
    }

    public function novo_desenvolvimento()
    {
        $this->load->view('header');
        $this->load->view('ticket/novo_desenvolvimento');
        $this->load->view('footer');
    }

    public function migracao()
    {
        $this->load->view('header');
        $this->load->view('ticket/migracao');
        $this->load->view('footer');
    }

    public function tickets_atrasados()
    {
        $list_tickets = $this->Tickets_Model->list_tickets_atrasados();

        foreach ($list_tickets as $row) {

            $atraso             = false;
            $planejado          = false;
            $proximo_dia_util   = null;
            $horas_restantes    = 0;
            $dados_diff         = null;
            $dados_update       = null;

            $dth_atual          = date('Y-m-d H:i:s');

            $valor_gut = valor_gut($row->valor_gravidade, $row->valor_urgencia, $row->valor_tendencia);

            $primeiro_atend = $this->Tickets_Model->primeiro_atendimento($row->tid);

            if($valor_gut < 25) {
                // Buscando as horas de acordo com o catálogo
                $dados_catalogo = $this->Categories_Model->get_catalogo_ticket($row->project, $row->category);

                if($dados_catalogo) {

                    if($dados_catalogo->plan_solucao == 1) {
                        // Caso o ticket for planejado não terá atraso
                        $solucao = 'planejado';

                        $planejado      = true;
                        $atraso = false;
                    
                    } else {
                        // Caso não, iremos somar as horas na data de criação do chamado
                        $horas_solucao  = $dados_catalogo->solucao;
                        $horas_atend    = $dados_catalogo->atendimento;

                        // Chamando a função que vai retornar a data limite somando as horas úteis
                        $dth_limite_soluc = $this->retornar_data_limite($row, $horas_solucao);
                        $dth_limite_atend = $this->retornar_data_limite($row, $horas_atend);
                        
                        // Verificando atraso na solução
                        if(strtotime($dth_atual) > strtotime($dth_limite_soluc)) {

                            $atraso = true;

                            // Pegando diferença entre as datas
                            $dados_diff = diff_atraso($dth_atual, $dth_limite_soluc);

                        } else {

                            // Caso existir o primeiro atendimento, iremos verificar se ultrapassou o prazo
                            if($primeiro_atend) {

                                if(strtotime($primeiro_atend->data_atend) > strtotime($dth_limite_atend)) {

                                    $atraso = true;
                                    // Pegando diferença entre as datas
                                    $dados_diff = diff_atraso($primeiro_atend->data_atend, $dth_limite_atend);
                                }

                            } else {

                                if(strtotime($dth_atual) > strtotime($dth_limite_atend)) {

                                    $atraso = true;
                                    // Pegando diferença entre as datas
                                    $dados_diff = diff_atraso($dth_atual, $dth_limite_atend);
                                }
                            }
                        }
                    }

                } else {

                    $horas_solucao    = true;
                    $horas_atend      = 0;

                    $dth_limite_soluc = null;
                    $dth_limite_atend = null;

                    $atraso = true;
                }

            } else {

                $horas_solucao  = horas_uteis_gut($valor_gut);
                $atraso = null;

                // Chamando a função que vai retornar a data limite somando as horas úteis
                $dth_limite_soluc = $this->retornar_data_limite($row, $horas_solucao);
                
                // Verificando atraso na solução
                if(strtotime($dth_atual) > strtotime($dth_limite_soluc)) {

                    $atraso = true;

                    // Pegando diferença entre as datas
                    $dados_diff = diff_atraso($dth_atual, $dth_limite_soluc);
                }
            }

            $dados_update['tkt_atrasado']       = ($atraso == true) ? 1 : 0;
            $dados_update['dth_limite_soluc']   = $dth_limite_soluc;
            $dados_update['dth_limite_atend']   = $dth_limite_atend;
            $dados_update['diff_days']          = $dados_diff['diff_total_days'];

            // Atualizando o ticket
            $this->Tickets_Model->atualiza_ticket($row->tid, $dados_update);

            echo '<pre>';
            echo 'TID - ' . $row->tid . '<br>';
            echo 'Title - ' . $row->title . '<br>';
            echo 'Status - ' . $row->status . '<br>';
            echo 'Category - ' . $row->category . '<br>';
            echo 'Project - ' . $row->project . '<br>';
            echo 'Data criação - ' . $row->created . '<br>';
            echo 'GUT - ' . $valor_gut . '<br>'; 
            echo 'Atendimento - ' . $horas_atend . ' horas úteis<br>';
            echo 'Data primeiro atendimento - ' .$primeiro_atend->data_atend  . '<br>';
            echo 'Data limite atendimento - '.$dth_limite_atend.' <br>';
            echo 'Solução - ' . $horas_solucao . ' horas úteis<br>';
            echo 'Data limite solução - '.$dth_limite_soluc.' <br>';
            echo 'Atraso - ' . $atraso . '<br>';

            if($dados_diff != null) {
                echo 'Dias de atraso - ' . $dados_diff['diff_total_days'].'<br>';
            }
            print_r($dados_update);
            echo '</pre><hr>';
        }
        
        echo 'tickets atualizados';
        die;
    }

    /* Função responsável por retornar a data limite somando as horas úteis da solução */
    public function retornar_data_limite($row = null, $horas_uteis)
    {
        $data_limite    = date('Y-m-d', strtotime($row->created));
        $data_criacao   = date('Y-m-d', strtotime($row->created));
        $hora_criacao   = date('H:i:s', strtotime($row->created));

        // Caso o horário de criação for menor que 8h da manhã iremos definir a criação para este horário
        $data_criacao2 = $data_criacao . ' 08:00:00';
        $hora_criacao = (strtotime($row->created) < strtotime($data_criacao2)) ? '08:00:00' : $hora_criacao;

        // Fazendo um laço enquanto existir horas para ser adicionada
        while ($horas_uteis > 7) {
            
            $data_limite = proximo_dia_util($data_limite);

            $dia_semana = date('N', strtotime($data_limite));

            if($dia_semana == 5) {
                // Sexta
                $horas_uteis = $horas_uteis - 8;

            } else {
                // Seg a Qui
                $horas_uteis = $horas_uteis - 9;
            }
        }

        $horas_restantes = $horas_uteis;

        if($horas_restantes > 0) {
            // Somando horas caso existir sobra
            $hora_limite = date('H:i:s', strtotime('+'.$horas_restantes .' hour', strtotime($hora_criacao)));

        } else {

            $hora_limite = $hora_criacao;
        }

        // Verificando se após somar o horário ultrapassou o final do expediente que é 18h (seg a qui) e 17h (sex)
        $dia_semana_limite  = date('N', strtotime($data_limite));
        $inicio_expediente  = '08:00:00';
        $final_expediente   = ($dia_semana_limite == 5) ? '17:00:59' : '18:00:59';

        $dth_limite     = $data_limite . ' ' . $hora_limite; 
        $dth_limite_exp = $data_limite . ' ' . $final_expediente; // final expediente

        if(strtotime($dth_limite) > strtotime($dth_limite_exp)) {
            // Caso a data ultrapassar o expediente, iremos somar 1 dia útil e as horas após 08h
            $start_date = new DateTime($dth_limite);
            $since_start = $start_date->diff(new DateTime($dth_limite_exp));

            /*
            echo $since_start->days.' days total<br>';
            echo $since_start->y.' years<br>';
            echo $since_start->m.' months<br>';
            echo $since_start->d.' days<br>';
            echo $since_start->h.' hours<br>';
            echo $since_start->i.' minutes<br>';
            echo $since_start->s.' seconds<br>';
            */

            $dif_hour   = $since_start->h;
            $dif_min    = $since_start->i;

            $data_limite = proximo_dia_util($data_limite);

            // Somando a diferença em horas
            $hora_limite = date('H:i:s', strtotime('+'.$horas_restantes .' hour', strtotime($inicio_expediente)));

            $dth_limite = $data_limite . ' ' . $hora_limite;

            // Somando a diferença em minutos
            $dth_limite = date('Y-m-d H:i:s', strtotime('+'.$dif_min .' minutes', strtotime($dth_limite)));
        }

        return $dth_limite;
    }

    public function adicionar_apoio($tid)
    {
        if($this->input->post()) {

            $uids = $this->input->post('worker_apoio');
            $erro = false;

            if(count($uids) > 0) {

                for ($i = 0; $i < count($uids); $i++) {

                    // Verificando se já existe apoio do usuário no ticket
                    $verifica_apoio = $this->Tickets_Model->verifica_apoio_ticket($uids[$i], $tid);

                    if(!$verifica_apoio) {

                        $dados['uid']   = $uids[$i];
                        $dados['tid']   = $tid;
                        $dados['tipo']  = $this->input->post('tipo');

                        $this->Tickets_Model->insert_apoio_ticket($dados);

                        $msg    = 'Apoio cadastrado com sucesso';
                        $type   = 'success'; 
                    }
                }

                $msg    = 'Apoio cadastrado com sucesso';
                $type   = 'success'; 
            
            } else {

                $type   = 'danger';
                $msg    = 'Não foi possível adicionar o apoio';
            }

        } else {

            $type   = 'danger';
            $msg    = 'Não foi possível adicionar o apoio';
        }

        $this->session->set_flashdata(array(
            'type'  => $type,
            'msg'   => $msg
        ));

        redirect('ticket/view/'.$tid);
    }

    public function excluir_apoio($tid)
    {
        if($this->input->post()) {

            $uid = $this->input->post('exc_uid');

            $dados['excluido']      = 1;
            $dados['dth_excluido']  = date('Y-m-d H:i:s');

            $this->Tickets_Model->excluir_apoio_ticket($dados, $uid, $tid);

            $type   = 'success';
            $msg    = 'Usuário removido com sucesso';

        } else {

            $type   = 'danger';
            $msg    = 'Não foi possível excluir o usuário';
        }

        $this->session->set_flashdata(array(
            'type'  => $type,
            'msg'   => $msg
        ));

        redirect('ticket/view/'.$tid);
    }

    public function aprovar_tkt_kanban($tid)
    {
        $ticket = $this->Tickets_Model->get_ticket($tid);



        if (!$ticket) {

            $this->session->set_flashdata(array(
                'msg'   => 'Não foi possível aprovar o ticket, por favor tente novamente',
                'type'  => 'danger'
            ));

            redirect(base_url());
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $dados_ajuste['tid'] = $tid;
            $dados_ajuste['finished'] = date('Y-m-d H:i:s');


            $this->Tickets_Model->encerra_homolog_ticket($dados_ajuste);

            $dados_grava['tid']  = $tid;
            $dados_grava['uid']  = $ticket->worker_uid;
            $dados_grava['started']  = date('Y-m-d H:i:s');
            $dados_grava['tipo'] = 1;

            $iniciar_homolog = $this->Tickets_Model->grava_timesheet_homolog($dados_grava); 

            $dados['tkt_aprovado']  = 1;
            $dados['uid_aprovado']  = $this->session->userdata('uid');
            $dados['dth_aprovado']  = date('Y-m-d H:i:s');

            $update = $this->Tickets_Model->atualiza_ticket($tid, $dados);

            $dados2['tid'] = $tid;
            $dados2['uid'] = $ticket->worker_uid;
            $dados2['started'] = date('Y-m-d H:i:s');
            $dados2['finished'] = date('Y-m-d H:i:s');
            $dados2['id_atividade'] = 8;

            $this->Tickets_Model->marca_timesheet($dados2);
            $this->Notifications_Model->homologacao_finalizada($tid);

            if ($update) {
                
                $msg    = 'Ticket homologado com sucesso';
                $type   = 'success';

            } else {

                $msg    = 'Não foi possível aprovar o ticket, por favor tente novamente';
                $type   = 'danger';
            }

            $this->session->set_flashdata(array(
                'msg'   => $msg,
                'type'  => $type
            ));

            redirect('ticket/view/'.$tid);

        } else {

            $this->session->set_flashdata(array(
                'msg'   => 'Não foi possível aprovar o ticket, por favor tente novamente',
                'type'  => 'danger'
            ));

            redirect('ticket/view/'.$tid);
        }
    }

    public function iniciar_homologacao()
    {
        
        $ticket = $this->Tickets_Model->get_ticket($_POST['tid']);
        $att_status = $this->Tickets_Model->receber_ultimo_status_detalhes($_POST['tid']);
        $valida_homolog_timesheet = $this->Tickets_Model->valida_timesheet_homolog($_POST['tid'],$_POST['uid']);
        

        if (!$ticket) {

            $this->session->set_flashdata(array(
                'msg'   => 'Não foi possível aprovar o ticket, por favor tente novamente',
                'type'  => 'danger'
            ));

            redirect(base_url());
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            

            $dados['tid']  = $this->input->post('tid');
            $dados['uid']  = $this->input->post('uid');
            $dados['started']  = date('Y-m-d H:i:s');
            $dados['tipo'] = 0;

            if($valida_homolog_timesheet == null){

              $iniciar_homolog = $this->Tickets_Model->grava_timesheet_homolog($dados);  
            }else{
                $msg    = 'Não foi possível aprovar o ticket, por favor tente novamente';
                $type   = 'danger';
            }
            
           
            if ($iniciar_homolog) {
                $this->Notifications_Model->homolog_ticket($_POST['tid']);
                $dados2['tid'] = $this->input->post('tid');
                $dados2['uid'] = $this->input->post('uid');
                $dados2['started'] = date('Y-m-d H:i:s');
                $dados2['finished'] = date('Y-m-d H:i:s');
                $dados2['id_atividade'] = 4;

                $this->Tickets_Model->marca_timesheet($dados2);
                
                $msg    = 'Homologação iniciada com sucesso';
                $type   = 'success';

            } else {

                $this->session->set_flashdata(array(
                'msg'   => 'Já foi registrado o timesheet de homologação',
                'type'  => 'danger'
            ));

            redirect('ticket/view/'.$_POST['tid']);
            }

            $this->session->set_flashdata(array(
                'msg'   => $msg,
                'type'  => $type
            ));

            redirect('ticket/view/'.$_POST['tid']);

        } else {

            $this->session->set_flashdata(array(
                'msg'   => 'Não foi possível aprovar o ticket, por favor tente novamente',
                'type'  => 'danger'
            ));

            redirect('ticket/view/'.$_POST['tid']);
        }

    }

    public function solicitacao_ajuste()
    {

           
        if($this->input->post()) {
             $ticket = $this->Tickets_Model->get_ticket($_POST['tid']);

             

            $dados['tid'] = $this->input->post('tid');
            $dados['user'] = $this->input->post('user');
            $dados['comment'] = $this->input->post('comment');
            $dados['difference'] = '{}';
            $dados['created']  = date('Y-m-d H:i:s');

            $this->Tickets_Model->inserir_ajuste_comentario($dados);

            $dados_ajuste['tid'] = $this->input->post('tid');
            $dados_ajuste['uid'] = $this->input->post('user');
            $dados_ajuste['finished'] = date('Y-m-d H:i:s');


            $this->Tickets_Model->encerra_homolog_ticket($dados_ajuste);

            $dados2['tid'] = $this->input->post('tid');
            $dados2['uid'] = $ticket->worker_uid;
            $dados2['started'] = date('Y-m-d H:i:s');
            $dados2['finished'] = date('Y-m-d H:i:s');
            $dados2['id_atividade'] = 9;

            $this->Tickets_Model->marca_timesheet($dados2);
            $this->Notifications_Model->ajuste_ticket($_POST['tid']);

            $type   = 'success';
            $msg    = 'Ajuste solicitado com sucesso';

        } else {

            $type   = 'danger';
            $msg    = 'Não foi possível solicitar um novo ajuste';
        }

        $this->session->set_flashdata(array(
            'type'  => $type,
            'msg'   => $msg
        ));

        redirect('ticket/view/'.$_POST['tid']);
    
    }

       
}
