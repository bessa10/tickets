<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imap extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        $this->load->model('Tickets_Model');
        $this->load->model('Attachments_Model');
        $this->load->model('Users_Model');
        $this->load->model('Settings_Model');
        $this->load->model('Versions_Model');
    }

    public function index()
    {
    	/* Ler um e-mail */
		//Conecta-se ao MailServer
		$host = "smtp.office365.com"; 
		$usuario = "sistemas@cetrus.com.br";
		$senha = "@Sys#20cetrus";

		//recebe a conexao
		//$mbox = imap_open("{".$host.":143/novalidate-cert}INBOX", $usuario, $senha)or die("can't connect: " . imap_last_error());

    	$mbox = imap_open("{".$host.":143/novalidate-cert}INBOX/smtp",$usuario, $senha)or die("can't connect: ".imap_last_error());

		// Percorrendo por todas as mensagens da caixa
		for($messageNumber = 1; $messageNumber <= imap_num_msg($mbox); $messageNumber++){

			// Separando em estrutura
			$structure = imap_fetchstructure($mbox, $messageNumber);
			// Pegando as partes do e-mail
			$flattenedParts = $this->flattenParts($structure->parts);
			// Pegando dados do header
			$header = imap_headerinfo($mbox, $messageNumber);

			$email_from = $header->from;

			$username = $email_from[0]->mailbox;

			$remetente = $email_from[0]->mailbox . '@' . $email_from[0]->host;

			$elements = imap_mime_header_decode($header->subject);

			for ($i=0; $i<count($elements); $i++) {
			    //echo "Text: {$elements[$i]->text}\n\n";
			    $subject_novo = $elements[$i]->text;
			}

			$email_resposta = false;
			$veri_assunto1 = strtoupper(substr(trim($subject_novo), 0, 4));
			$veri_assunto2 = strtoupper(substr(trim($subject_novo), 0, 3));
			$veri_assunto3 = strtoupper(substr(trim($subject_novo), 0, 2));

			// Verificando se o assunto é de resposta
			if($veri_assunto1 == 'RES:' || $veri_assunto2 == 'RE:' || $veri_assunto3 == 'R:') {

				$email_resposta = true;
			}

			$anexos = array();
			$texto_email = '';

			foreach($flattenedParts as $partNumber => $part) {

				switch($part->type) {
					
					case 0:
						// the HTML or plain text part of the email
						$message = $this->getPart($mbox, $messageNumber, $partNumber, $part->encoding);
						// now do something with the message, e.g. render it

						$message = preg_replace("/<img[^>]+\>/i", "", $message);

						$message = preg_replace("/<table[^>]+\>/", "", $message); 

						$message = str_replace('a:link', '', $message);

						$texto_email .= utf8_encode($message);

					break;
				
					case 1:
						// multi-part headers, can ignore
					break;
					case 2:
						// attached message headers, can ignore
					break;
					case 3: // application
					case 4: // audio
					case 5: // image
					case 6: // video
					case 7: // other

						$filename = $this->getFilenameFromPart($part);

						if($filename) {

							// it's an attachment
							$attachment = $this->getPart($mbox, $messageNumber, $partNumber, $part->encoding);
							// now do something with the attachment, e.g. save it somewhere

							$arquivos_nao_inserir = $this->arquivos_nao_inserir();

							// Só vai inserir o arquivo se não estiver na lista de bloqueio
							if(!in_array(base64_encode($attachment), $arquivos_nao_inserir)) {

								$nome_arquivo = date('YmdHis').'_'.$filename;

								$nome_arquivo = str_replace(' ', '_', $nome_arquivo);

								// Especifique o local onde deseja salvar o arquivo
								$file_path = 'attachments/'.$nome_arquivo;

								// Caso for imagem iremos converter para poder salvar
								if($part->type == 5) {

									// Pegando os nomes dos anexos para depois salvar no banco de dados e atrelar ao chamado
									$anexos[] = array(
										'nome_anexo' 		=> $nome_arquivo,
										'string_64encode'	=> base64_encode($attachment),
										'is_image'			=> 'Y'
									);

									// Load GD resource from binary data
									$im = imageCreateFromString($attachment);	

									// Salve o recurso GD como PNG na melhor qualidade possível (sem compressão)
									// Isso removerá quaisquer metadados ou conteúdo inválido (incluindo o backdoor do PHP)
									// Para bloquear qualquer exploração possível, considere aumentar o nível de compressão
									imagepng($im, $file_path, 0);

								} elseif($part->type == 3) {

									$anexos[] = array(
										'nome_anexo' 		=> $nome_arquivo,
										'string_64encode'	=> base64_encode($attachment),
										'is_image'			=> 'N'
									);

									$fp = fopen($file_path,"w");
									
									fputs($fp,$attachment);

									fclose($fp);
								}
							}
						}

					break;
				}
			}

			/*
			echo '<h2>Remetente: ' . $remetente . '</h2><br>';
			echo '<h2>Assunto: ' . $subject_novo . '</h2><br>';
			echo '<h4>Data: ' . date('d-m-Y H:i:s', strtotime($header->date)) . '</h4><br>';
			echo 'Texto e-mail: <br><br>' . $texto_email . '<br>';
			echo '<h5>Imagens em anexo</h5><br>';
			*/
			//echo '<hr>';

			// FORMATANDO OS DADOS PARA ABRIR O CHAMADO

			$usuario_email = $this->Users_Model->find_user_imap($username, $remetente);

			// Caso o usuário do e-mail enviado não existir, iremos cadastrar o mesmo
			if(!$usuario_email) {

				$this->load->helper('string');

				$password = random_string('alnum', 8);

				$author_id = $this->Users_Model->cadastrar_usuario_rede($username, $password);
			
			} else {

				$author_id = $usuario_email->uid;

			}

			// Não vai inserir caso conter a palavra Ticket Nº no título
			if (strpos(utf8_encode($subject_novo), 'Ticket Nº') !== false || $email_resposta == true) {
				// Mover ticket para comentário
				
				if(imap_mail_move($mbox,$messageNumber,"INBOX/comentarios")) {

       				imap_delete($mbox, $messageNumber);
       			}

       		} else {

				$field_chamado['title']			= utf8_encode($subject_novo);
				$field_chamado['description']	= $texto_email;
				$field_chamado['status']		= $this->Settings_Model->get_setting('start_status');
				$field_chamado['author']		= $author_id;

				$tid = $this->Tickets_Model->add_ticket_email($field_chamado);

				if($tid) {

					// Caso inseriu o ticket, iremos adicionar uma versão
					$ticket = $this->Tickets_Model->get_ticket($tid);

	           		$this->Versions_Model->add_version($tid, '<i>Ticket created</i>', $ticket, $ticket);

	           		if(count($anexos) > 0) {

	           			foreach ($anexos as $key => $value) {
				
							$field_anexo['tid']			= $tid;
	           				$field_anexo['filename']	= $value['nome_anexo'];
	           				$field_anexo['original']	= null;
	           				$field_anexo['title']		= null;
	           				$field_anexo['description']	= null;
	           				$field_anexo['upload_by']	= $author_id;

	           				$this->Attachments_Model->add_attachment($field_anexo, $value['is_image']);
						}
	           		}

	           		
	           		if(imap_mail_move($mbox,$messageNumber,"INBOX/abertos")) {

       					imap_delete($mbox, $messageNumber);
       				}
				}
			}
		}

		imap_expunge($mbox);
		imap_close($mbox);

		echo 'chamados abertos';

		die;

    }

    public function teste()
    {
    	/* Ler um e-mail */
		//Conecta-se ao MailServer
		$host = "smtp.office365.com"; 
		$usuario = "sistemas@cetrus.com.br";
		$senha = "@Sys#20cetrus";

		//recebe a conexao

		//$mbox = imap_open("{".$host.":143/novalidate-cert}INBOX", $usuario, $senha)or die("can't connect: " . imap_last_error());

		$mbox = imap_open("{".$host.":143/novalidate-cert}INBOX/smtp",$usuario, $senha)or die("can't connect: ".imap_last_error());
		echo "<pre>";
		print_r($mbox);
		echo "</pre>";

		$messageNumber = 5;

		// Separando em estrutura
		$structure = imap_fetchstructure($mbox, $messageNumber);
		// Pegando as partes do e-mail
		$flattenedParts = $this->flattenParts($structure->parts);

		// Pegando dados do header
		$header = imap_headerinfo($mbox, $messageNumber);

		/*echo '<pre>';
		print_r($flattenedParts);
		echo '</pre>';
		die;*/

		$email_from = $header->from;

		$username = $email_from[0]->mailbox;

		$remetente = $email_from[0]->mailbox . '@' . $email_from[0]->host;

		$elements = imap_mime_header_decode($header->subject);

		for ($i=0; $i<count($elements); $i++) {
		    //echo "Text: {$elements[$i]->text}\n\n";
		    $subject_novo = $elements[$i]->text;
		}

		$email_resposta = false;
		$veri_assunto1 = strtoupper(substr(trim($subject_novo), 0, 4));
		$veri_assunto2 = strtoupper(substr(trim($subject_novo), 0, 3));
		$veri_assunto3 = strtoupper(substr(trim($subject_novo), 0, 2));

		// Verificando se o assunto é de resposta
		if($veri_assunto1 == "RES:" || $veri_assunto1 == "RE:" || $veri_assunto1 == "R:") {

			$email_resposta = true;
		}

		$anexos = array();
		$texto_email = '';

		foreach($flattenedParts as $partNumber => $part) {

			switch($part->type) {
				
				case 0:
					// the HTML or plain text part of the email
					$message = $this->getPart($mbox, $messageNumber, $partNumber, $part->encoding);
					// now do something with the message, e.g. render it

					$message = preg_replace("/<img[^>]+\>/i", "", $message);

					$message = preg_replace("/<table[^>]+\>/", "", $message); 

					$message = str_replace('a:link', '', $message);

					$texto_email .= utf8_encode($message);

				break;
			
				case 1:
					// multi-part headers, can ignore
				break;
				case 2:
					// attached message headers, can ignore
				break;
				case 3: // application
				case 4: // audio
				case 5: // image
				case 6: // video
				case 7: // other

					$filename = $this->getFilenameFromPart($part);

					if($filename) {

						// it's an attachment
						$attachment = $this->getPart($mbox, $messageNumber, $partNumber, $part->encoding);
						// now do something with the attachment, e.g. save it somewhere

						$arquivos_nao_inserir = $this->arquivos_nao_inserir();

						// Só vai inserir o arquivo se não estiver na lista de bloqueio
						if(!in_array(base64_encode($attachment), $arquivos_nao_inserir)) {

							$nome_arquivo = date('YmdHis').'_'.$filename;

							$nome_arquivo = str_replace(' ', '_', $nome_arquivo);

							// Especifique o local onde deseja salvar o arquivo
							$file_path = 'attachments/'.$nome_arquivo;

							// Caso for imagem iremos converter para poder salvar
							if($part->type == 5) {

								// Pegando os nomes dos anexos para depois salvar no banco de dados e atrelar ao chamado
								$anexos[] = array(
									'nome_anexo' 		=> $nome_arquivo,
									'string_64encode'	=> base64_encode($attachment),
									'is_image'			=> 'Y'
								);

								// Load GD resource from binary data
								$im = imageCreateFromString($attachment);	

								// Salve o recurso GD como PNG na melhor qualidade possível (sem compressão)
								// Isso removerá quaisquer metadados ou conteúdo inválido (incluindo o backdoor do PHP)
								// Para bloquear qualquer exploração possível, considere aumentar o nível de compressão
								imagepng($im, $file_path, 0);

							} elseif($part->type == 3) {

								$anexos[] = array(
									'nome_anexo' 		=> $nome_arquivo,
									'string_64encode'	=> base64_encode($attachment),
									'is_image'			=> 'N'
								);

								$fp = fopen($file_path,"w");
								
								fputs($fp,$attachment);

								fclose($fp);
							}
						}
					}

				break;
			}
		}

		// Visualização das imagens
		/*foreach ($anexos as $key => $value) {

			echo '<img src="'.base_url().'attachments/'.$value['nome_anexo'].'"><br>';

			echo base64_encode($value['string_img']);

			echo '<br><br><hr>';
		}

		/*echo '<h2>Assunto: ' . utf8_encode($subject_novo) . '</h2><br>';
		echo '<h4>Remetente: ' . $remetente . '</h4><br>';
		echo '<h4>Data: ' . date('d-m-Y H:i:s', strtotime($header->date)) . '</h4><br>';
		echo 'Texto e-mail: <br><br>' . $texto_email . '<br>';
		echo '<h5>Imagens em anexo</h5><br>';
		echo '<hr>';
		die;*/

		// FORMATANDO OS DADOS PARA ABRIR O CHAMADO

		$usuario_email = $this->Users_Model->find_user_imap($username, $remetente);

		$author_id = $usuario_email->uid;

		// Caso o usuário do e-mail enviado não existir, iremos cadastrar o mesmo
		if(!$usuario_email) {

			$this->load->helper('string');

			$password = random_string('alnum', 8);

			$author_id = $this->Users_Model->cadastrar_usuario_rede($username, $password);
		}

		// Não vai inserir caso conter a palavra Ticket Nº no título
		if (strpos(utf8_encode($subject_novo), 'Ticket Nº') !== false || $email_resposta == true) {
			// Mover ticket para comentário

			/*if(imap_mail_move($mbox,$messageNumber,"INBOX/comentarios")) {
       			imap_delete($mbox, $messageNumber);
       		}*/

		} else {

			$field_chamado['title']			= utf8_encode($subject_novo);
			$field_chamado['description']	= $texto_email;
			$field_chamado['status']		= $this->Settings_Model->get_setting('start_status');
			$field_chamado['author']		= $author_id;

			$tid = $this->Tickets_Model->add_ticket_email($field_chamado);

			if($tid) {

				// Caso inseriu o ticket, iremos adicionar uma versão
				$ticket = $this->Tickets_Model->get_ticket($tid);

	       		$this->Versions_Model->add_version($tid, '<i>Ticket created</i>', $ticket, $ticket);

	       		if(count($anexos) > 0) {

	       			foreach ($anexos as $key => $value) {
			
						$field_anexo['tid']				= $tid;
	       				$field_anexo['filename']		= $value['nome_anexo'];
	       				$field_anexo['original']		= null;
	       				$field_anexo['title']			= null;
	       				$field_anexo['description']		= null;
	       				$field_anexo['upload_by']		= $author_id;

	       				$this->Attachments_Model->add_attachment($field_anexo, $value['is_image']);

					}
	       		}

	       		/*if(imap_mail_move($mbox,$messageNumber,"INBOX/abertos")) {
	       			imap_delete($mbox, $messageNumber);
	       		}*/
			}
		}

		//imap_expunge($mbox);

		imap_close($mbox);

		echo 'chamado criado';
    }





    public function flattenParts($messageParts, $flattenedParts = array(), $prefix = '', $index = 1, $fullPrefix = true) 
    {

		foreach($messageParts as $part) {
			$flattenedParts[$prefix.$index] = $part;
			if(isset($part->parts)) {
				if($part->type == 2) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.', 0, false);
				}
				elseif($fullPrefix) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.');
				}
				else {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix);
				}
				unset($flattenedParts[$prefix.$index]->parts);
			}
			$index++;
		}

		return $flattenedParts;
				
	}

	public function getPart($connection, $messageNumber, $partNumber, $encoding) 
	{
	
		$data = imap_fetchbody($connection, $messageNumber, $partNumber);
		switch($encoding) {
			case 0: return $data; // 7BIT
			case 1: return $data; // 8BIT
			case 2: return $data; // BINARY
			case 3: return base64_decode($data); // BASE64
			case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5: return $data; // OTHER
		}
	}

	public function getFilenameFromPart($part) {

		$filename = '';
		
		if($part->ifdparameters) {
			foreach($part->dparameters as $object) {
				if(strtolower($object->attribute) == 'filename') {
					$filename = $object->value;
				}
			}
		}

		if(!$filename && $part->ifparameters) {
			foreach($part->parameters as $object) {
				if(strtolower($object->attribute) == 'name') {
					$filename = $object->value;
				}
			}
		}
		
		return $filename;
		
	}


	function mail_get_parts($imap,$mid,$part,$prefix)
	{   
	    $attachments=array();
	    $attachments[$prefix]=mail_decode_part($imap,$mid,$part,$prefix);
	    if (isset($part->parts)) // multipart
	    {
	        $prefix = ($prefix == "0")?"":"$prefix.";
	        foreach ($part->parts as $number=>$subpart)
	            $attachments=array_merge($attachments, mail_get_parts($imap,$mid,$subpart,$prefix.($number+1)));
	    }
	    return $attachments;
	}
	function mail_decode_part($connection,$message_number,$part,$prefix)
	{
	    $attachment = array();

	    if($part->ifdparameters) {
	        foreach($part->dparameters as $object) {
	            $attachment[strtolower($object->attribute)]=$object->value;
	            if(strtolower($object->attribute) == 'filename') {
	                $attachment['is_attachment'] = true;
	                $attachment['filename'] = $object->value;
	            }
	        }
	    }

	    if($part->ifparameters) {
	        foreach($part->parameters as $object) {
	            $attachment[strtolower($object->attribute)]=$object->value;
	            if(strtolower($object->attribute) == 'name') {
	                $attachment['is_attachment'] = true;
	                $attachment['name'] = $object->value;
	            }
	        }
	    }

	    $attachment['data'] = imap_fetchbody($connection, $message_number, $prefix);
	    if($part->encoding == 3) { // 3 = BASE64
	        $attachment['data'] = base64_decode($attachment['data']);
	    }
	    elseif($part->encoding == 4) { // 4 = QUOTED-PRINTABLE
	        $attachment['data'] = quoted_printable_decode($attachment['data']);
	    }
	    return($attachment);
	}

	public function arquivos_nao_inserir()
	{	
		$arquivos = array();

		// imagem 1 assinatura
		$arquivos[] = 'iVBORw0KGgoAAAANSUhEUgAAAD0AAACUCAYAAAAgcSHPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDk2NkNBOTM4NzA3MTFFOUE1QTZBOTI2OTQ3NTdBMTUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDk2NkNBOTI4NzA3MTFFOUE1QTZBOTI2OTQ3NTdBMTUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTlCNEEzMUI3MDM0MTFFOUI4QjVFQTVGNDRDNThGNDkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTlCNEEzMUM3MDM0MTFFOUI4QjVFQTVGNDRDNThGNDkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6egUm3AAACVUlEQVR42uzdv0sbYRzH8e+TBOKSIFwp6D9gl64uyahTlxY6FRdxccjSpUOHav+CDgqOBcVBh9IuhdJIHOLkkkGQSGkRaYYQhBDaGGLu+twgSH7UJJDWe77vB57kLlyGVz5Pvve9EDgjmVwgvaMinc5LMbF9qZZl0Ai+fZUojtiA12clHt+zzzt2GnFsJP7+kZgleTj3Xa6v1lxCx+4+wry2jw90ocPVkJhatVN6psPoMO0FO6VnOo0WeaRteYdjWiM6qREtoJ1uTvqNo81bOxvuJ/3p1aKu5Z1/+1Tld7pi5zMXLkBGQc94nvehUCisRx0+ciFLpVJvisXiWalU2lV1ykqn09tRbljGPU/HstnsE3XNSTKZXFaHNsbMa2xDPY3oOBccoEGDBg0aNGjQoCeMDoKgZ5I0aNCgKWQkDRo0aND/sHrf7JM0aNCgaUNJGjRo0P8LTSEDDRo0HRlJgwYNGjTVm6RBgwZNISNp0KBBU71JGjRo0PcaTSFjeYMGHfmOrLuYkTRo0KDpyEgaNGjQoCeM9n3ficpN0qBB04b23SZp0KBBg+ZHBJIGDRr0hND80YblDRo0hYykQYMGfR8KWXdBI2nQoJ1CNzWiz9W1ob7vf1GXdKvV2tKGPrCzHFV0Yoz31BuNxgtN1bvebrfDO3pWtbSh+VqtFt6H+jjq5+lRlrcb92wdJenn7w6d6ciMZHLB0Md2L/migrsS03uDdhP9WyP61CX0cOdpP/isD93+9V7X8vaDj/bxh6akm1L/uaKnenc6F1I5eWy3Ll1D/xFgANuVTH/wbpabAAAAAElFTkSuQmCC';

		// imagem 2 assinatura
		$arquivos[] = 'iVBORw0KGgoAAAANSUhEUgAAAKcAAACUCAYAAAD2+DnvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QzFDN0IxNDA4NzBCMTFFOUI5RjVGRjEzRUI5NTNBREYiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QzFDN0IxM0Y4NzBCMTFFOUI5RjVGRjEzRUI5NTNBREYiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTAwMkU2REY3MDM0MTFFOTgyMDc5QzcyRjkwOTZGRkMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTAwMkU2RTA3MDM0MTFFOTgyMDc5QzcyRjkwOTZGRkMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7GWdisAAAcHklEQVR42uxdCXgURb7/d89MAgQIityHAgFBFAWE58UKHgjr+gR1XU9EUNH3xBV1+ThW8JMPouKFoLt4PFTW9VhU/OShsgoBURAMAXwBhIBCIBwBgUBISOZ4VdVdPdU1fc50TyZQ/3z1TWa6p7u66le//1H/qpFisRjwsm/fPhAiBGODlvbd+jn/YtteAOHq1tC6x2cQCPZP9v5B0QVCfJBO0Ob8dSBLzVK5iCzaUYjH0gha5K1OFZiCOYU4ll1bfoCON01xcuqTCJgtvbinJGxOIU5szmg0Sso5t8+0+1oFAmcTL+4v1LoQV1IyfxxCjWRVmnh1LwFOIa5ly1sPp+U+ApxCkpKNf3tAgFNI5sqPs0YKcArJXPnuudsFOIVkrnzz9HABTiGZK4smDhHgFJK5smDcQE+v52aG6BxUpqIyUP1fiBBfxSk4X0LlUdFcQozEaJYxXeCch8pI0QVC0glMJzbnSwKYQupKZBsbU6hyIRkJzqmieYTUpXq3Aucw0eRCMpU5m4nmEZKp4BQiRIBTiBABTiECnEKECHAKEeAUIkSAU4gQAU4hApxChAhwChHgFCJEgFOIEJNMeD+zm4XUH2E38uLf88UP3AjmFCLUuhAhApxCBDiFCBEOkZCMcIjsCt71WDhEQoRaFyJEgFOIEAFOIQKcQoQIb11IXXrr/HE/cCOYU4hQ60KECHAKEeAUIkQ4REIy0iEycpAEcwoRal2IEAFOIUIEOIUIh0jIaeMQGZ0rmFOIUOuZIiuKdsCxEydFbwlwZpYsXv0z3DV7MYyeuVAAVIAzc+TLNSUw9p2VEMhqAD+UHYUxLy6C41U1oteEQ1S3sqTwFxj3z9UQzMqCCKqPhMrafcfgwVlfwN8eGQKNG2aJ3qtjh8jvmaKMZM6vi3bBhH8VQlYoi5TGasH/F+47DmNf/VowqFDr6ZelG3bDk59uUICJWLMRKoFspQRRaYBK0YFK+PPry+F4da3oQQHO9Miyn/bC059vghACZlAFZwCVkMqaDdVX/PkGBNBxb66ESgFQAU6/paB4P+R/sZWAEoOxgfoa4t6zxzeVn4DH3l4tACrA6Z+s2HwQZn79C3F+QlkKAIMMGBsw71mgYlbddLAKxs//UQBUeOvey8qth+H5gl0IbCGI0q1NUJFRiYDyXtK2PIm/x8dk9f8th6pgwvtFkH97b8jJDnpSr627yqFwcylUcLHV7me3gD7dO0CTRtkk7opfjWTvwaPktc1ZuY7ux8Zw2WvienTr2CKlZwkfPw7Bxo219ydKtkPF+g0QQZ9TyW7dBs4acq2tt27lxeMtaXwHZ7rk+5Kj8Mq3ewkLYkAGmQfFoJN4cDLHAgxAMWh/PnQSJn/0E0y/9YKUALro22J48ePvYUf5URjUswNc0qOjCp5qWLy2hHyOwbMkfwQ8+14BLFyzzfA6fc9pCYMv7gr5C75zfO8l+ffA4InvmB4fMfB8+MudgzTwbhw7DsrXFkLPJx6F1rfclHA+Pd7n+WfgjCsuIyDdNuM5KFtaAC369YXmfXuT8yq2lsDB9z6EARw4M5I50yGrdxyD2avKQUaMGVCBB6AficrnoPucsmpUPY6PUdCWHK6BKR9vhmk394BGLgGKmeuRlxfCsuJSGNa/K7z311sTWO8JBIz3v1oHb3xRSI6VHlDYcc6DQ+HKvnmE5VjBILq4RwftfduzmmiAfvj3feE/B5yvY0z6Pwb2x9NHaJ8XbtkNM+YvhXcL/o+8f/r+IeT16Jat5LXpRRcaPhM93uSiXgSYGx9+FCr37IX+c+dAkwt7aee1q09q3Xdg/noCXl1zGELBkKoqWGYEwoxRUBiSZcsow6T85xS0JUdq4KnPtsFTN3ZDAA04BubI6R9C4a8HCDvRzjeS26/rA38Y0FMBDTqfgKlHewLEvt3b295r4y/Kd/oh0PLqeu6nq8jrped10LMwum7+mKGEVTFAcf1q9u2HmmPHIKddO2iU18VQlePjZ3TvRlT6vgWfwGEEVsyyLDCFQ8TI2p3VMLewggAzgIoUUl5lVPBnwZBS6PGQ+l5WPwup36FOk4ydo6DiIEnqZyVHamHaoh1woibiqE5PvvElARpmTCtgsoyI2QxL5xa5ju1KLNgswILtVl4271KA271jS4N76mfEKpHdiKV53z6G96navoO8nqkeP7D8W/Ka0zVPOESGwCytgTfXVyLQBTU7EnMbVdGYAQMqe8YYlS0xDCmhj43MAP6zkqO1MH3xTpg0tCM0yjJnUAwyajeOv2Og42ffsnM/ee11TgvH36GAxmobT7/y36Os2gcxJX9s7eb4d/Gxym0lCmi7dja8/9F16xUw5uXpjkcqjjuu72mzwO3H3bUw76dqCAYCEAgEScFsSf4PBglLBgMKW+L3wVBQY1NcZJZNVYYNMMcJ24bo9xU23V4RgfwlZVBVY+5F/uOrQvI6rF8eYsCmjp/n550HNfXs+DsqM15q8B1sWmBWVZi4acKxl5GThmXiXYPI62/riiztTXo8J68zeW3YprXC3G/9DzEJ6i1z+gHMuWsPw2/79kBV5XE4cexowjnZDXMglJ0NjZrmQqPGudCgUQ6xIRUmRGzJ2KVAGBY09tW8do2B48d+RQB97pt9MP7q1tAwK3EsLlyrMNBVfbu6eqbinfsTGDHu+DQ1BDoFdLezWxqGrigTs1JQtB3mIGCWV1bDK8jxonYtdXYaGtibRsfb/fFm2LdsBbE714wYDV0fGAWtDDz80wqcP+2phmmfbIDDB/ZZnld1vAIVgIpDSieFshtA49xm0Lx1e8jKySEAlYkZoICVOkxOTIMdx6Iws6AcpgxuZQgILOe6jCVSZ2jyu8sSjs17fJghOFdu2mV6r8ItpeT1m+JSGD3jI/L/0k2lRI1jpp396I3aNauQvYmdHRwOMhLqLLHHMUj7vjYLNk6YTDz24hdmwYEVK6HnjKdBZmKgpxU4G8snIXKkHCLRiKvvRaoqoRqVg4htQ9kN4YzmLaBZqzbQMKeJGoSnXj1oMVFJBTC1OxWbFV2rugpaybWWgW83gW7KlFchr/qtSbc6jgjQGKnRvTbvVMA+77FhUFBYAnO+KCTnzkPX5wP9OIBOnJ0+vQ3vVbF+o6Lyu+m1AQZov7ffhNK358P29z4gMdANY8dB73lvnJ7eeqc2ufDKfQOgKXJKasPRpMqJykrYs+tXKF67CtZ/txR++bkYyst2wYmKI4gyoyAjOxbbshIqclCxZ2sQICsOlkPp1mLoFSyHMdd0SqiblKQdv1UFUg8D9Wwm23YqLN3vHOPvbPy1XAsbPX7nQAJ8DOgpb3yZaE8WKc5OIxOVfqSoSHOGEjocseTZDz8EvaZOIu+xmj+2YePp6613atMU5tx/JfzX3GVw8ERqeZiRkxHYX1bq+Pzb+neCx2/pa/hMOUx4pgLZdGZTkbz8oKrg7h2de+pr1e/0Ryqa/87egxWEVaknjmXa/dfB2snziU08bF0J/K53HIgnyvYq4Oxi7KkfKlynePIXXmBav+bXDYbcDxcQ27QCgb1xrwtS8tb98NjT5q13bpsLr40ZCGdmByCMVHw6yp8uPhsB82Iy52tU8to31wC5HKlS+nMldg2/tFgBWlcXpsCazSqgz078zrZShTXPY45h+3LizZeR/8f+/UsCYN7ZyWrdKtF2R/Yotilzu3czPM5KVpMmCogvukgE4Tu3bQavPTgImuO4I04S8LHc0a8TPIaASYEly7Jh+e+hitPwzpIi8t5MKKD3HDii2aoY3GbA5wt2brB0adc8YRCsVYF7bgc9cG8b3FtT70++8VVinZjEDSo7336XvHa89Y+2/XGirEzRIF06nb7gpBkruHRpdwa8/vC1BKDRSMSXchsC5hO39gdJkkjBoGOBwjLh7QgAWJ1i73vqm19p3+HL/t+Ow4atZbB+a5kWDDcDPF9Kdh/S7teuZbOEQfA9ZdWOLRNA/fR91xJ2x+DG05v4s2bnKo7Ojtmv6c4tyZ8Je5cuJ8fPHHy1pSbYg5wizLCd7/zT6eets+lTFCBUCEAfuQ5Gv/wFHPB4LdDdl3aF8bddooEK14O/P5veldMgBLMeuQH+/MrnMH95MUmRGz6gp2bjrSjaDt8glY+PzR4zBNaotuNhZKO+vnC1YR0eGHaJ7v06xrtn24R68UWqM9QVqXUeuG1bNCPqfdL8ZfDsJ6tgUN886Dx6FKwbPxFKP18MFdtKSHbR/oIVBGwhpKq7T55ArnPwy38T56jl0CHE/lTU/g4oW/Ax+W6HG66HDg+NMU11M0qJo4Bn//crZU4yMmR37CDzsjG37Mhey0pFUtm+5zCMfHkx7K3yZj366Eu7wYQ7Ltd1Pm4wu7rgemOH6H+/2wSfrNwMRTvj8U/MWlf17ABXI7Bef8V58NRbS6C0vML0Wjehc/B5rLz4/nIoRte8pk8XwtQ6e3PXQXjmnwWQm5MNL469wfS69L70vCPfrYJts2YTQJJB1q4NtLv+99D65mEaE2Igbsl/VrNRqbTo1wdaDx2KnKJrHPUpLRGklXCpra0lpaamBqqrq+HkyZNQVVVFyoi5a/wF5/Dhw+GFF16IuQEkZSq3UrL7N7j3pUVQlqIXP/qyc2HiXVfogOkUnH4816kgbsCJXzE47/77D77bnJJZaICldNbRSLYD89qfCfPG/QFaNQgm7fyMQqocA5Ok0HH1wO/dhjmoGUBBbWavCjHXnH6Ck9wFjxLcIeFwmPyPX3EFUgWkEUDfffwGaNHQPUDvvawbTLr7d1q9jICWSqOxQBVghYRfCWbbwGsN40jfUSAGcaqb6mSw7OkNQJvD/MdvVAAaizoqIy8/FyaPuNJWdXsJIB6srCY51cBKn42SFPt8FBP01Q/Tx8xbl/kONxoh1A5x4wTZMej8J4bBbTM/hSOV1k7SPQO6w1/vGUjuHwgEbEHhF2j4TuGjFPXFXjVTyxSA1GRi+97sl9z8Zs6oE6DxLOIFg+R1aA7v/2U45DYKgZpalFBGXNEDnhypxPEwMJ3UM12MVh9MAJ7tef/Ba7PNU3A+//zznnaM207pigD6wfiboEnDUIIqx8Cccu9VhDGdMnU6wZlp9mp9AWLSNmcyHWzm8Tq9RtcOZ8FHE26GXLybnMqYdw9AwBx1dVLhoUwQK7CmClQ+UF7fgJhWcHoBVAzQDybcghg0CwGzJ0wdda0243MqxB6TDVmZgbA+A9GxQ2QV4/RCFbENRh0qM8BhG3TxtDugdfOm5FzWIE+GXTIdrDwA+fqz8/1+P6ObVDk/2jZY1x3CRwSMPF0MTN5bTLax6xuTsHWmXjM/g+VHjDFjmTMTgMozR6rAZJNAMh2M1KZmQcnaj0ZMybPsqQDaoNsOThf7sFlF9ZHxklGf9LlpeIzGcCko2fd2IDQDrRcDPWPBmU72oQ2MO4SCtD566U5Ykn8uoxgubQenMWgz5jRLb8s0tg1mesexzEFnpJL1QtPJ/G5ZkhW7GG6qoTQz5jTb6rCunEnXC9z83H6E7yCjaUkr+8uJ1FWMlG033rbm62cVkWBzG/wYZEbXpFqLpswZLUPxA8AZyZx205JsXDDT7SfacXw2vhUp2D2Pmf3pd4iLai8jzeXHjJecTEX9ZE4305JsVlAmmiSsPWkHOMqsbp69rp+bJQY/Av4ZBc5kVC6bdOKkXn7Xn58y9OvZU5mMOCVDSX6rv1RUNAtQq072OuJgZks60xIKkPFWPQE5QN5H6bWIsyQ7euZTNcyW1I4fXjtFdk6AWzah05xWU3xeJVrQ++D/wxF3+0EFVHCFgkEVpHRwkd3woaYmDHJATei1eGY3plAyz2i1yUQ8noodpjQ4RFHmhkGT0etVSMaLaclkWdS8TsbBawyYWFR1WhCA8B/dMAy/D7i8Fx2UZAvxqHqNQPy6waD6HFF1E110XDZoJ6oN0hmFIN47zpInpozSBl7f3hCctAHwqKU/rxI3esFTlejEO00WoGbmAr5rOKI0akA27mxWpWoxV/z8AS/NAXVwyhLjCUsaMciyUg+s7iVZ0kwBxfOXEuxoP2O4UQrGSFQzR3B7BIhtnd5lGkwn45vGGyyqMkc0FldHqagMv0c7ZVHaqXGVKmmq0sy2Y8NAXoZtqAmA7UyZAZ1ZPfDnRJvFlP/xvxQglDC8Di/h+0Uj6qYJ6hYGRDMEcL/hNvXfzg266+g4UPEGmbSBrDq4LoHJ1481IcJhZKcFZKJCKUul6uA4ffbacJgM7PjGD/ZtgbVZTH0mfC5tb0oYikkQSFq9U9NCY2OVtXXr1uuPtx7TjdIwCwTZ2hlxuvbHvWMV0+xl8nDcgGETSfBrMBjQOiYcjmomi1+DhpgSiN1CIT0wlbolsiU+n30GfI6m5lXVTgkDXydMns3Z7BnfVkbRgboOUyW9PycfkmFtN8VOZWdG9MnFTlZLugGkZrsSp0SyfA5qn2GzRJ9FrjCTBOB5cJv+vhKuGh0QbNYQZSgjz8zoc9yedEBRQBHgkr4IQG1tGLVFlJgNFPSGzChLlv1eb5ONreKFrPpn7SNsUGeFUg+tsuraiZfMhz/Y5a76ZGfFuvLStqfOjKTuZc/ez8rOtDWxFDRqap6tM2ZmMvDwAIzENCZO9l4ZxZxeSlwVRQgwWWDxjeo0tOOkkfnsH32yBRBWISEg1kliOtsrRsdMqTgV+pgkD1TDtlMZUrZoJOIsRSnw9R48/jCE7k9ZM0JMMVmAk/d6aUewjcN6/7z616lrB7MlZuxoVh/aqUadnQqj0YEkqyEW3vnDJk1ENSfsoh2ypAwWCNglgkiahoqreebeKsuyWscpMZxy4GTtG6L6sVqL0Qij+XdqToY1gCo/piV7Asbkwk8x3WBxrMINYqQJ9ULVxsD0MqtK5yypwXqj6c0EL58jhYwH54QJE2D69BmOpjH5+CFVR8RoVxnKrvGJ14gaKhQKaKDGihAb/Do1BXowmi1NcKPyzexmfP9I1Fql8oORdjy9tpHzR9mNhrVonqRZ9MLtVCt1liLq4FLCZmGD5R2UXYGE1YzMJbc/WJCufE6JBnbdqGz+wexYQT9tp9hhICkNTOKQrJpHxr0EnE2VMttYOHWS5MhBokzFd6xRu9ApSL4OND5pFAJKhsXZmCiuF2VQ41344qDMNJUfNA1iQvKZQnbTkjwo6XeUWZN4Z8QANE9cNorTMTajUxvRjT2J62B2PrWJjY4ZDUx1Ct4UZGxuKvtd/KI8p+Ry4EHC1Kfd9Cav8sHjyIUn4MzPz9fHBF2A1Gr2J5EpY7opQkmSE7xqM9bQzVbx4SUP7SjeQeIdHqfPT00cOyZnJwq8sKG1qU+QHE9+sIF9bHJg/SHXAUqD9iNQcgxS046JqcY3HYnqj6jyTozEOUzKbSRHak3vhDgHq5NFb+ysjB1LGwHArfdP28SrVDiq5mMxiWinoMPZOcq+RKshMyuS5p3yXM0QmYGUXeBE1RPtlPivWUi2ToyZUe3GOdGP/jhYcePSmRrcyPRaSqfh9zahKlB+Y9N0NoexM9njhLWk5GZQ4tfC8/ER0/RFN22Cp0SNHCQrstFyTDG4VfMrEon6Pktk+LSTJk2yZFI2HY1WSstNJNt0R8gUGu4UPFuBA9F2a0yMnAV25GsdnWSYhQ4MnCPJMgdN+6IzKGYFA4MGxc2AmTjwVDszxZ1KCEg92kk6SKIH4DoxWufAqX2p5HO6j5ak7K07bQh2Z2PlOxLJ9nHbIWwoxgygyUwvsjaixMy2sMa/YgLY26lmdTAzd5zYmW5YlJ1hSiVagckiorJfsp45UflkYAOc1JYIpwecMTNbh1e9eJ94alMq04EKImLgwwb2sqR5rk4uTRvLEvQEkEoHOQml8B68WWTCSRqcKzDQ6IVHu5+w14lZRBEcgQjVI+xDJpfsjH2Mf94F6PQaZQ+fNkblwUEb1IotjTLGrVQVBTG1KdnFZmYOkpUDCJK3YRji1ET1PxaQatsqbK8EPVJlPUWjpFGt87+cwKqrqJoCRjufvGJjmVn64GYfTvJoDhuasFw0YpiNFNU6UJlRopeM2dhFbJA6Pnui5Eji3gsYBM+xXY1taqNdOwKy7MmgjKmzPQSMeEBokY34QE1l/ZDmxEmKHcoupjP7UQLeEVIc34jrNVRJh5IMbSg2TskzUkxNX5NAN7/Lh0f4h5bUMIdbEyAgm8cfjS4VTSKZgxj/kj5hgj4X6YxAICFriKhzSU6OeYzyT9H9NC89Fm93PlxWg3M4JcnRkmLelqXTp3QCBANVdhTTBuIQ+TU3bzlDZBY8dxr4NYvv8T8ZEglHFHXqEqRu4o9eBLNpWyjJvDIBCusgOVHnLKB4kLjpZHbakdaPDnocLpJsYsH8vdmJBdp3VvXRZsjQJcPpjHNOnz7dNShpEgHtGDYZ1mxJKxsqMdpaxjrgT82LGGG3VEev4x3o1BxNklChggDXAbMbjTiYLS32ewBp7WUyT281MADiqzdp3+GQoGaX6thSb8/7tR4saOXlggQpz0PbsaiVKcFPb9KltJrKxQ6MCg67pFwnHWu3ewbvmQfps4WjUB1W0v0UW1iq06Res+0izVeZxrTYJ2s3kghBNG63x9kSPbfk/3K3oHl4xVtPU2NRFyzHAjIOUkgYzTT+GI0ZT3OSGaEUn4ldF85PjWaFAgn3Iwvm5LiB5MXabjeJ0Gy2kx2zsXkKbPoeeU6VKWmYUAnnGQ/atIBTvdkRVJol04GWcUpuYRZNeNVN+XGL1vgkj3jAX8+4SjZNos1HQjABKeltdshGAnRTBSlx8CbsIUoGI+NWg37Rnz6wntwgcaMRsIQJs8tAE0DMGJcuK1HqJquZVwHtNzDTkcdp660jWYjKSM9VDmeL0nlbfm2R7WyNyY8bUGZJZefjGAU04x+mujDPeIDFEuKLVvZ5at6/pMVmQ5bPIrkCnJ+7jFjV8h234HSyIItvQIWVYinZafr8RyWDhl1L4yQkQtSf2tjsSkXFDvXHfjQEbCyW4LiYmUJWjpfVALfbGZqGl9z8fGO6wVmAysuoPOqGFa0WZLGNSTs/yC144/dlSgaoIXWVJzuTw89/83YjC0ZZ0ifnpnOLQXYxms5D1pZX4/aJJtQ7GYfJKgWSLMRjfpSsLsROV41DZSAqF6WkYmhIyqYxzfZlSkbVUWCSh8TLY1WWpuCn4SA7285NiMQsVONpvFWWPFlG4SSxuS52r3MDTiyDUOWnOmXQGOj3bIyBfprTzRQlG6gPU6C6iCbg82prIyQeyeeThilQNccrPl8tqcdk7Ai4AFwgbasYrXML3IKUeulm+bZmhU+ZrIsfLDiiMuhnqNyDyjBLLz6meMx0NHrRX8o0YqKKcxKWIis6Y0azNsadQC9Hwymn8o9zmal6limt9p5nwZi2ILyFDYrLvSBEiLXy9GbAiLYU4pXcN2+9t2wumlSIF3Lna6u8NzVEswpJVW55qcAfO1g0rZBU5IZnvvLPSRPNKyRZGTJtkb8RBNHEQpKRgZMX+H4PAU4hruXyJ95TEpqNy26v7hMUTS3EjfzHQ3PsThkFDXOXCOYUkonyb8SerwrmFJJWufjOKc5OrDz0CJzVuS36b7hgTiGZJlHYWXgLRMIzIIXpzP8XYABbl+6oti1rdQAAAABJRU5ErkJggg==';

		$arquivos[] = 'iVBORw0KGgoAAAANSUhEUgAAAHcAAAAzCAYAAACt8JfwAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAACDVSURBVHhe7VxnmJTV2b6nz872wnaWIgiCipXYxQaK7UpiMERNokbUqDFBjdh7iSXR4Gc0JjHGEhU1Xho1lhAJxBYwIh2klwV2ge1l6nff57zvzswCBr5P/3DtyzU7M2855+nP/TznDP4UD/Qde6IETvTviVz18WQl0KfcPdgS+pTbp9w9WAJ7MGt9ntun3D1YAnswa32e26fcPVgCezBrfZ7bp9ydS0ANLo8up+KAh5/Y70p4/UjwVJx/PPwepgmpD5bSRd6T5NUET3g9Pvj4tHm+1+HN+p7ZRNv+7uQOnxUFu3Cksmf60ic8vWfa2d2i8cvG1TiWJ/vJ8pTJmT5nc+qccUWxI6H1Ime3PVdj9x43mUpQtzH4/BrOgxj/NvH1y4eeQce2LbjrtiuR76OyyUkqScV649SxlCy2/BSDx2E1m7oe8cgyZCVm5vTsLp96z1Y/x93lruqOONqR0mSdu9qpdWjMIDn9pKVW3OvQJ0rPfBa/mRxmU8arKes8Ozx6xJJ+areUm35MhFmbSyWoIl7wBnOoPKqKNEixN//mVTz13Ivwx6NI5kZw6+QLURLy8gkfQmIh3mk93RdwmMwmOcuAdF8vxWbeLaFk88z7PbvF2k4klnn6v3ljxr078C49bU/bcay3MnqZb9u74U7PuMzu1HPTF3ZLAvYxWTBDsF4KqTyZpPe1RdnLDALbGA3vfPKveOKFVxAo7sfrPvzulfeQCBTg1p9MQHmQltpJFZNI4+MevrwKzs6hMLkd4WnPzgpdRlrWq3uHsMQOg/0u6PAruMUr20o6IdzoUCds+hHJSROpPFaxbtjJZGBHwURO6ziusXXn2KmOeX23lGvHU2xVPtM7qUiQaHotoy62UMG//ONb+N2LryFUUoFgKBfJaAp+fwn++Jd/IJkK4PbLvomqCO+O0mPp1fDRSCQNE/JEqsZ1LNxjA1Um/1l8K1SnHDpcwzOP2kye9hY78g4cKutc7+v/l+9ujElbqDuKaJWSrRStkjPiUWboybJge699pXN+NlLZsYp3WbnuyqARtXSR0DszhzeAbs7ZTj386pnp+P20d5BXXI1kgG7sDSPqp4XmElTllOHZ1/4BTyKKu382AWUhKtcbonKpaKMxjpygv0nRmiBDCDKltNDSVus8SLuwvmAOY9ZJJ9Rle3SPujMsxMNIYUChAYNO4HRcw5Bg8r2lx82TriH2eJBzj73foc+rHOkYqeCl0aTDq0OmqzQjT4cGB5P2KD/TmL3yeZfW7SSyvYJ3S7nG+jm44YVOn0z54GUsbupKYurz7+P5v76PQFElcnMi9OgUoimCJebUFBOxYENueS2mvTsTwXASUy5hiA5HbPhKCjlTwFQutWsFbWJBdnzannyKW1FEHmEEZk0gHu226dxPA9L4jt5S/Oz1+5hRaEQyKvsUIwjnoTKE5s1IrtZchZO+JGlj9nCijGuMGtsObsaTbPgvkeQcPo+pBLwuCCOQNHTI7GTIcmQFPmUhnk9wDvMei3MeL9OWT2JB0kQnXjMyUirU4XXu4wwOZnEuZL3tsnK99Ch5iPHWuJ8K8xsiG0nzE6/MwtOvvEPFVsEfzkeQivWTqCDZ66a1xin8FHN0oLAYyAGef2cmYiE/Jl/4HdQFvSYqK7z6A2EKPmoM3M2l2URnK1uqSNHzYmQ8GPCjK0oUTusO+Bg1pHRJx0jUxQZUkgmNlChp9Cjxy1DJi1EeFR+PEvXzvEeaNFbMdzenW0J7DNwr7+RYMppELEFj4neJiOMxGxFgcg4aUoDPS34eKtgGJirFBAQaAufoJs8hRjpVEjYbk04ZA2US8smJTJnhGIosQtWGExl0TY5gDKfHXM2HXVauHo13E+H6Qwy5AVPHNtGQfvPiLEx7YyaKagYjxjAc50SpZIwRlzUs0TMDr5mkO5akNdKTcwqQXz4IL737CaKeCO648DSU0cGiAlJOVAga7WYmoQwVu2GSp8RWnEwm+WhLVxwRFtTK0lFeyKFQeh9JIug4hZ0TEFXZh1GUjpDB8r0OKsa9Lt04mE8UyukDJNcNBLomuqTEhLyXc9IH6RAxG1mcgGHmoIF5WTtTVJRVDAkaWJgGbniXYYrXri74gjRWlZn0anOOEUkRETGCUTUTZFQZ2MSNSbusXHmtnxPEGIpFvFDx4y9/glfe+wTh8jri3gCCFGgqGoWvIAdR5U8S6CMDPhIUFpTmuQRDdcJbgKKSArw7fR4KaYWTzz8d5QRZypw+WrAs1Vi4c9hgK55dQGFLCX3r5vihgAdhzr2+qQ2fz1+O5ctXorOjDVEKQ2Fy8KBBGDZ8OAbUlCIcDmDVpiY6dtTxdMnG+otCXHFhPrY2tdCTGUEURKkQ8e2l9pL00I4ue17erRBbWV6K9s4ouru70N7eQaeKIcbyMJyXg8qSMlTl0ZBouCa6UpGdLe2Ic4z8wlIau7yeXmjKG90AtDY20NN9CIeoZCkwGEb3hk34dOYMbN64HtFoFzrJV3lNLQ4/8QQU1vTPyl6OHxvJUblpzNbbrc1TNsHauE8qgrSSzTSYP742G2/MmE1UXI2EPwcBhTEqirJj2WPrWS9Di4+m7adSpWTl53hSQCuALoYxf34I7838DJFgAJMmjkN1Ho1DQrYTGgLTOtZ3l1b7SUcBFbu+rRtPPfsX/HPWv1Dbvz9G7jMEFQMGGEGvXb0GTz3/CpYsWYF7f3EzDjp4FH446Vp0dnZgr70GU8DM0bT+LY1bsffQoZgw4Qw88sjjaG5pQV5+PnLzC9Ha2o5169ahpLgI/cpL0Nbawvu3Yfjwkbj0snNx+60PorWtFQMGDkAkFEQHlbxufT3DaAwXX/A9fGfssZRLAB0Nm/Cr2+9AF5U/5c47kVtQZNN7nB5Jb2xcuQoP3XQrDjrkUJx1xRXGKOb/5XW8+sILqBtYh7JhQ1CSV4WmlmZsadiMpq3bUFhb56QfK7HMeEc3YqhluEx5QqazpItyZ5/q2CRLFeMtooCWxJxU3wU887cFeGPWXPhLiYqDKneY53hPgGE4QA9qS8RY81LLDEkeD11c4MDHcfSuNhU5CpDZOMuoZGQonn1/IVoRweRzjkVtRNZM32Ae8nKshNTogAuBLuUlAQrlcUXej5ZswjU33onO9jbccPXFOOXYb9CLLXZ2DWPDxDPwl9emY0hlFRrWbsG82XMxceKZuJ6NFaHzBIUdbe9EXigHFeVFGHTdj0z4VGMmTMP90xsf4eYbH8KUq07Ej88dT8+JMTJ0obCgACsaPfjnjM9wzjnjcOfVk5AXJm7oSmD95kZMvn8qLr7hTsT9YZx/8uGIb96Cba+/ibqTjkaYhiL50Owpey8xSgCtS1aj650PMOC4E5lXQljywt/wP/fch2/+7EcYe873bCMh40jQmSxosFGN7uRiS9dzFcc1QaZf8BStyaMKWxKkPBMUwqZuYNp7i/H6+x8jUl6NTpYynfLMcBBeeierHnpqEhGGsKgSPJ8XupZiXYvyB4QmLUDwM393JUPIrxqMf3w8j/m5G1edN44oWoCGQuJ9KYESzq0wbdKQkCtHk+3MWbYRP7z4GoRycvD04w9i/7oS491xRQmBpYRQcxTVZYW47IKzjLc/9OS7DONhjDthDPauLrf5MUtiKewzdKATJUKG7o7WJHIjBTj+qMPQv7JMrua4APAOI4+XAO7Io0ejhqnGHPSDopL+uPrnl+Ods87HW+/PNMptW12PEhrFMd84iGGdDkVHMchZSZdC7li1CqW5Yex3wH6Mz02Y/vhTOOSAgzD2B+eS8W7SIqxj27uiS8av5GTod2roTLzAq1Sehacm15v8phDMMJFi00EASa7cQH6em74E7344h8qoRCfP+5kfc4gmpCwvQ0iSHpcwKI+eyVfSACR5m6NcKZovlQdG6aSwgB6MWBe8DFEzZy8guPDisnNOQj8ynKDHBjl9jAajGk/5z0svUGHV0NyJG266m+GpHi/++bcYScWK6QTDbYilmJCq32n/JOPdSPp5jtcXLluGSGEh6gbX0ZQ4Nl/yB5/sWJqW1OgRSZYdKRrvVqaPufMWoKaqDMOHDjZPpBhnUsl8Xg9g8bK5VGSIqWCQGZ9uAk83pciqQIk2QBkpquloWPoF2snf4FGjbILhXF5GPQ8rBni6sHrZInT3L0F45GC0LluJNXPm4nvjJ9NeSQujoYdE+mQMAmuOrqw19aAS+9U5qFyylqFcm9vZymao6KSyklIsJfDyrBWYuXQFcqqqKOBcKsiHDhLmp3GkBOlJgBxSQvXyIR8hrIfeaZTotOL0WePru/nsKFlAxpdbAh9BxPT/rCDqfh+XTDgO/VQuc8wAgYffqT1lufLiN6d/hOkzZuHKKy/F0ftTGKw98vwUJkOykKlCfyLZbUCmh+lEwmyh9JevXk8wUoOKqmIjIB1u4jEmbhoWLEuUUvi1ubUTK1atxNCh/VFepITVwvPqqiWI8ANYuuwLVFWXoLS0CHREo0hWeeaY+c8PEGNuPGPsMUYb6/4zH/l1/VEwZC+agFAuZ+wiwqC2Yl1tWLZ4KUr2HUqLz0OC8wYov40rvsB+NH7TKlc0pazEow4bDbPiThbSJwf00EzdU7G2X0wrIXhqIOcvvL8Gb374OTq8Cqcsc5inCSZJWwCRSC4nJqKk9XkIJjwqM2JqL9rw6yrSEOMU9O45i0ZBcNOFYF4RlZIHf04h3vt0FeU7Gxd/6xBUM8QpkqhMkMHRdE0J9sKrbxGR5uOM08YZ+iPqhLGcEMtqbPgVeZRO6IUp0i1rX7+5HQuWrsSppx2HAQW0HKMM55A3cHhjfPS/hGlh8pn6FqzfUI8JZ4w3JYs9LGCsb+zAypXrMPqY/VFF8KXD3MJB357+bzz15DO49PsTcdqY0Yg2dWD10mWo3Wc4Y3YhedISqRI7Z+GkrQ1bsIWGd/SZY8wA+QNrUTNiCF56bRpKRg/EwWeeCRQWUbbWJP2MAD3986zKIo02/C7qTNsCBaFEzTzS3JnAY9Nm4aV/fI6tBD8E+QYQeeUlgQgSXfwuT2XsDOQyRBfnIpQXoeEVI4/1bE4wxOKdTQZaXEKIkFYT17tjTprTSw0Uk9mm9i4aSIThMIDCqgH44NOFyPV24YJTD0UFx/YJJFG5Ut/S1Zvwn4XLsN+oA1DXv9QoyIR7WTTBoa3o9V1X5GdMIfy0gmi0raMTTdsa2et+i7V3B8Kkv4vo9oD998URhx5kxlFJpu6qhluydDnLohhG7UeP0sGolCAfHtK5cu1qNDQ0ozC/GI3NHcQPKaxZvgFzP5mHd999B98/+yxcfeF55MODrRvWooFGcuB3TjKyFRI2PSHHy1toJN7GNgwZPMSaV00lTplyBebcci3uuuoanP/hbBx14eUoHrW/qW1Nw0OND9fezLttgLhQ0gydiSzNDeqm0Kre+Nt7eOKxP8BTPpR9fnoVu0A5QnhkNhHtQI68g6ElGouic1sn2tpajKI3+jawl5yL4mKFq1Lk5ebye9jmYCIhea6UrOVCtdliJDYSifDZMMeV10dRVVONf33wCQYWeHDe+CNNd0cKC1B5q9asw+bGZowZsxeK89m4lpGYqCLkyJqURpCgZ8hwxIe8TIa0ZOkajhMnGMvFyjX1iLc3wBfzoLWlDdUVXMHinTFFJi8jkCOihfMXoqgoDwPrSq0YE+wYKY3whi9WNHDOAGLtUVx1/a2YO3cxRh86HocdOhL3P3QL9h3Ajp0j/HWffYoY26IDR47gGQI1GYjCRNiqZ/W/GbLZA+jHFq3oTcQ6UTf+ONw84Am8dv8DeOOpF/HJx/Nx9rXXYN8zTzXhOUG5+RTaeynY/Wp6G66CrcYpDFNQJ3H4gfuYfPHi3+cQHdch2aXyhciRPeEkC+lOKjgo7+T4ZJfCZBuuO4HuVDsRZiu2Md9sIJLNz8tDQUEhFU2wwO85LBdChPWm12tSgGB2AO30qnx6qdDnykWzcdiwGhyy3xBzn9BHisDIS+W2sO5UyI3kFUBYxDLg9G6pTHdJTUjQLq150cFb5s9bglK2QKdMvgT7Vqk5ag9FL7X7WBCZFqubg9kyx4J5i1BbWUpARSRuwhyNk0QzK+HzBauRlxfG5Ct/iDXrl+Hs712BJcuW4467L0E1ARX1Q4egLKnhL+bOQ25hAQoHDTY0qZcs4SdolD46y9o589CvrgZ5ZVXC+0iGcxGjAdQeOBo//vUjmDv8Sfz+0Ufxu7vvwTX9q1FzJEsrpjMfS7VsxJ/2ZXLvxH7XoQ24opIYmocNrMFNky9iqPRh2tsfoKB8L9Mgb21rQpB1WDyURFe83fRh5SECBqY7JvBHfcUIBNq6O9BJWL+5fh1WMESHwzkUSC5yqGTVun6+gmz5dXczGrC2jXc0Y8MXi3DUgcNw4XdPxj5VhcbQPMynBsjw8PN+GYOaDz3pxnitzUMKqaobTUimp6oD1c4UsnjhIgyoLcfgShqQMQgKMU6jpBuqHvemokTn9BqOJVS6dmM7Vq1YhSNYOxfnCtXzGUU1vjWxBFy0fDGG7l2NfiVB1JWMxB23TcElF9+NOx9+GQ9M+TZKqUb11D1sNjQuXIyKQXWIVFQYLpIG2Nilitj6DWiYvwR1B42Et7yccmCbVDmVfKjnHSoqxqgrL8N5jGi/fvghfD77U9QcdQQjECOA2HbDjNFrlnJFseRg9z2ZLqVCBm/q6upGXUEIt/30HDa32fB/6xOUVFYy7aTQzDaYep5dbKl5+KxfK0DMIz6+m44Lw1tAbumulphgEKdHN6G9ZZstkTiXjwSKAQ89L4eIZdvaFRh/4tG47aqJGF6ez24OW5ecQ16rRruOurqBCLJztLmx0WzdsSveIlwFk2nZmrBpul0OMKpfvxXr16xlB+rbTC1ydJVvLJFiNBSh+6BE7shC+ZoyWLpoCWltwcjhQ0yP3KRyepoKs3UNTfhi9TKcfvxRppSSt33r9BMw419L8NQf/4ATjt4L540+wPDfsXEDtrIM2uf448G2lxlIYVkUqhu+au58bFm7DkdfcyHDEREkQ4ZdWiXG4FuAoddbkIuBRx6Bst8+jpYtjYZd1fLZJZEVg3tQE6b4yTpprIlWksPQEKWiBhRGcN/PL6DQfHj5zXe5SFBL2M+VD5Y7SQNg+E5hBakENcANaHIs3PUss7IhtpwOu4Kl1m8VBtmtNM8ZxY45Ag9cdz4GFQWYyykLtheVAjzKg7w3zmcOGlaB0aP2wpyPPqB3NWKf6jIuIOi6FhY5loN6E6TPy6iienbBohVob20mYKlz9iqpBFGtTl7V3VFu5lJdQgsYjDCidv6yFaYTVte/0khILJj9J2R57aq12LppA0aMGGbG0/l81vw3X/sDzPz3R7jrpqkY94eHUT4wD93tMbQyigVKidB9zJM0ZHluirKTHBb8fRa8+RGMPOYITsLOnJTuC9m+A5lR5aLaPkaBaAWsqrra6ovRJmkMWguwTlGkVRQnVdFwbEtLSuhZRaBAwjl55nyQhaIY688dblOv+S4Kki3489sfwl8yAJ0JNbZNtU5HVbO9g4xzQtbH8ZSErSjgNDDIjDbQqd9ralXlbc1JkKHGZ1fDWnzzhCNw7/UXYCgVq2sh5isplPHbeJddRfGihMRffs7pmPTjGbj7vt9i6i+vR456xEYDTjOCDClytDlMz/lsDnnyYxRrYvGpvAmfVrGc8ofASHhDyjU1Mf/MJiL30JNq+rNPrcijf7Qe1bHLP1uIAMPGUJYsJtoZHwSjTQHbkFfgosvuxi0PkLYHJyNcUoXW4giWs+GB5s3wEGiaxECDXP7Xt/Hms8/ju5ddipxKLgJoeZHpLploNfT4iT+8SjFczvzgvb8jwt75gcezbmZrOMroGKUR2JVjrR2TJ6YZE0qsPjOKJHtuu8MGEKCCHnzLlEvQRsW98PZH8JVaZannqbwbj7PxwfjtCSvUGpjUc+izVkMUprSInujsNKg8xD1UzfVrcdpJx+B29nQHFHNxgfdm1p9G/AIf6mSptKLHnTb2cFw/5Urcc9+vzQrTzwlq+hfm9exk0UrNdKLtRSs3YOI5EzDn80XcIVJMUJOLrSy7YrSEGFPLtpYm0+zvX1lhygu/Om6cv35zMz79dDaqCaSG7VUr2XIBxEN84OGiRwqzP5qNsuJ81NYU27jHP8ZhaMzjTz8MJ304Ho/97nEceugQXHDuGTjkhLF440+/x/6PPolDvz3BIOKlH/+bAOk+DBh9ML5x/nlmE8FGNooK+5UiUlzlSJ2OQwA5/elnMetfs3DRlKuRP2iA6er5aPSqBSwktWu6mULvcdad6LXntBK3PKO8IIz7byTIYl9z2lszEC6toVUnORcXuVnyxGNa2hOWVNNB9JnkZxK/XELFd7KtDQEtadEA2jetxqknHoX7br0YFXk+I7hchkp76CHrF9Yl9dHuBJFX/mTS2aitrcBUruJcsOA/OPLg/VFOwbRw1Wb+ouXY0tyG01n8r2/Yhq1cagtEgrj3wcdpUFySCxAM8r7GzZuw3z574/abrkUud2km6aIpjl2/tp5otxkHH3kgqwMvF0XUotTc3KDAfLtxwzrsO3wwSosKbARSIBCNBKIRetslV5yF1V98jAceexhHHTQC37nyaqadTXhq6h/wz5feIcon6qeBjx53Es6achWCJcVMP91469XXsGLJMgwZMRJVXDKMtbZhI9PD1m3bMOmOmzHipONsZUBnkqfKSSUlIzFb7PQcu6xcRTHtdEhROVVEjsqLKTYBXnlzBjxF3AwXKUaU0Bw5BAxEvsoHDgKxswuB0mvjBFRaq9Q+5+7N9Tjl+MPw4E0Xc7mPVJmtJG7KcHGAszuK5ZA6ZqYhogKeJZiOCePH4NgjDsa8BYvYvltM/pLIZ7fo3HPPxt57D0G/fiVYv6UNjz16r4nunWzrpZTDiWLDBCpBhvnCXJZnHC/FFKFlSWYVjBxWh+effgLVlSXsyCknM+w5q00FvP/RqfdzOTCCQlYNxmO1oEHc4BPuIMOHEwe8+sRDaGTXqYiKyykrxWW/fhT1cz/DxkULUEBHqBgxHEWHsCmhBno3HYJ1/rcmnosNi5dhzcoV6G5uRRF5GXryWAxikyVIQzbJQV5mQC+TgbuzxFGyifaOendZuaYDJB4o3CjzXy0V/AuCrAjR0J9eeZupl7k3SW+UGRt8bpKU47KcjkBIDull2AsR3TazNDrl5DEsGSZiYJFdfdGKj1evDJDnpFFn35Fdohc6V0tSoET4tqQoH+OOHI1T+HL9Xc/pGlffUFOah1q+RL8MW6/tGDewm6NrmZP+UMb2ZOWIQWYMbeqTiSUSetJL4+HKTSHzI48oBe2j25qixgmLYW2b0RjsvA3hyyA6zVmSi/5jjzQvW4ImmIK410z7pvhwiKCuiIvvRVW1GCFkbWSo+l1hUxsC2s0qnRo5HqU9gVkBKGded2XP5W2XlavyKCwkYUKsVdzg0gL84rpLiFR9eO6vM7iHqpZ5tYVLWMR23PZidhIYiKleL+2ZQoqQqda1q3DayeNw3y0XYHiBzXEm+KpANqja9Vq3Q6GLslMCDIIyrVtqt1RmTk8/pWymhQnWh6QrYNCmtTFFBRPCFF7NT0PMngqlSWc3hB1Tnucahz7bJhALICF9p7PkLnMrPRg6zOBOClFnjI0J7rOxpaA2MAjrOPMbTZvNbvqtAbcF00G0OdDui3IcQ1Qr+pE4QdJkUMCWTqCGjvoKmkqDpgOcGdbWJI4xZXz+0o+GAWcgW2/avkglO0r333CpAVTPcUHcl19GYwqbkkddGJMqZdLa+EYQ0NHUgG+efBTuue0i1tAWDlgLcwd3yegN9NyEYvunRoEu5jdqsqoyQZzC1d4ks6lMKFJXnbRt5jFDu30dgTWeMLq2+0CsIVmDs+ZlaTPbbh1BmP1Qjsf0KMR1XTFk1rBlRI5iMwRvH9P6kf2tlIko7iYsA1SoUK1q8Xk2Q827KJNRax+aNb1MnnrLajeVa7lz+XSDJTtZRK4lBBz3XDfJ5LFnXnodAS5UJ0MFdolKXRgK20cr7NpajzO55eShu36Kcm6pUQfCWGCGg6YzRprg9Cf3RqnVNVn3PdMoZNGZDDspwr3FPGInVn41wna2kNolP/dGfdRYmQQ6gjVG5ArFfbfzxpUuDGP20G36ZostZz7KzSDczMMhm6ZpoplM0yrWzuk+0ROzXONyxnDN3x1yl8NyNhUKT1aoWgRQu64y7GP+vAgB1rtPvjXb5F+flgBVaLPT1dWxDWeMP4ngaRJqtFdKuwHVmjPLIq6GrVQz1dVLRc60mVxleLyrBHeppWcg1xg1emaot0WE8VKHHynXXVtx45PpukncjE52SOXXzHjogD5dkqc6ynHMh0pVpFHNrkcdFWTGT5MXbMTIpFSz2jaHtJ6mvEcmX6lyXaM1DFpC1HBPcE9RjCCrIhKiB1/B5exn8PLrHyPJ5T+uoSDZ0Y4TxrCleD1DcREb3QJD3a3QjxLY1+ErbcFu7symO2vibDvr8Z4MM1CnyhnAKC7Di93f27iqcYOzHTQ9RnY8sApJ02YH7/kFgkORqxijFCf6286oVrTcrQGOMbvbO42SpVy9GxMwH01kd63LnTgzyjn8ZZqYJSMtq93z3Ew3chgKMFdoQ7hZwuO5ioJ8TGVp42Np8eLz0wySPH3c8bj/5kmoLuKSHm8SavZxAcEiwayY3JPGsqey38zf7Vw5wwwyrmU93+t8pqmkBSDPsd9MieHwZ6jTH21szzyXpqjXp3Qqtm7uYHQ3H/e8O4NpUBeIOYo1Buny6rLnnnSJcN63V0n6zO4p16HHfTPzOhYYML/NtUcZdzA+fOMPkdO2zvRzH7n9cvTj0pgM0Nkh4iBpu3KTeezAfnrNmvF1O22nr2WbTPYQO76Wnf+2u0e6/y/UZl3P+uIWYDtgxQqx58J2826HSXYujt5X/l/K3dk0/HEBu0xe3Hvr1aYuLqFi9WOxnrVX19a/RDm7zkLfnTuTwNeiXIXdBNuIZezgaIJOflat6G43z/aB3fLVPk3uhgS+FuV69MsD6ixFF1Zvx/waQbsuzPKge5hEthuk9t26uxL4WpTrc5octkIziMF2dzKQnCW0z2t3V2G7c//XolwLstzf2arYd9CIo9yd1bG7Q3jfvf9dAl+Lcrf7z0Z6OWifv/53xXwFd7BABcZ+BQNlDMGGuN3Hlj6yTCj9Jf2Lm6+Wgr7RjATm/C+37iEkiTcVFgAAAABJRU5ErkJggg==';

		$arquivos[] = '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAARABEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDB0+0utf1uG23yST3UvzyfeIBOWY/QZNWfE2lR+H9YNpavdgxDIknUISQT8y4/h44NVdGuk0/XLK6nLIkE6tJtHIAPPH9KXX71NR1u/vInd4pp3eMtnO0njg9K933ufyseXpy+Z6T/AGxqP/P3J+dFVvIm/wCeMn/fJorgtHsdV2cN4p/5GnUv+vhv51U0r/kL2f8A13T+dFFegvg+Ryv4j6GooorwT0z/2Q==';

		$arquivos[] = 'iVBORw0KGgoAAAANSUhEUgAAABIAAAARCAYAAADQWvz5AAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAAAK8SURBVDhPjVRLS1VRFP7OPq97r17NkHz1ICxq0msgBNnAURiI/yGaBQ1CyCbqSKxBETQKGjho5CDIQQ2qkUGmBb5IQTEsuXjz5s2b933O6Vv7XLNErMU5Z++1zzrf+ta31z4Wbi7YiC4NQFk9CAwHngcoA+C1rxlKXpcQePfQFAxYiK+cg+H2ohzwjYeIayJf+A+wwGdC04bp3EbSe2YBfh1UlWqozmPwaivam2LoG1vFyEQSvtDaj5nPhG61heKPgwRSPgplXD5Zg2sXmsg2QH87MDqdQjbPQPMfNQozYhCI5ii8+JzB0MRXtB2K4cH4WliemB+QVYWZx7n4ouEuHUMgU2ErU8CdkUXAYlCRWWxKpj8QSemXCHDA5e0Am0UgzVveVywE8hgYszHUeQzdLXHcGE/gzeQa7nYdR/eJWvS9X9Pgg5ea4ZO9sL31+gtefUjuAmIy0eJsczVOt9SgoW5Dl3OqPsa7Ck87jsCyFHJFD1HuKmpd3OfalZUMEj9FIxain8KQYJtSAveqXOZIvyAjJx51uT66jLG5FDovNuJhx2GcqY/gPPVMpPMksQ1UIbj3/hh4srCB4XcJYKuEl7PrSLY1oJF6uaKfVPOb0Xaluln1IzSNbGAlWwrnLEux5JLs3g7GHkCGVEqeuv1l22U0ETdljUhcMjkqvVt/8w810okNWKa4Ngz5kIFK+xZM7YccBCSMszjfYR8CCVMmcB07xLSEAbPb4isoeyefADkuewkOASVBWKacNcUVIJdF78gcHkVtfExldQf3P5/H4yoX8+lcWIljYulbFl3D04jbJiZXN4FIBCjnlZy1DZSyPgKlZhdTmJUjwCCwb2aW05iRZuVcr5FNNlfG20/rJMK4KDvdlAOpvlvIHJ3i/2iI/HsQcRzoQ1gxMtDi/2kidIT5t/9HXlH+R1O/AIs95yEl3QIcAAAAAElFTkSuQmCC';

		$arquivos[] = '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAARABIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDRnt7vU9Yu1iVp5hI7YzyQD2/wpbvQ7+xsIb2aIiOXsOqem70qa0vINO8Qy3NwJwYrhyPJIz1PBB7Vb8Q+Km1aEWttG8NvnLlj8z+3Havf5qvPGMV7p87y0nCUpP3jb0++ujp1rmdyfJTkn2FFRadFIdMtT5bf6lO3sKK4JKPMz0YuXKjnfE//ACMN3/vf0rLX74+tFFepS/hx9Dyav8SXqeu23/HrF/uD+VFFFfOPc+lWx//Z';


		$arquivos[] = 'iVBORw0KGgoAAAANSUhEUgAAABEAAAARCAYAAAA7bUf6AAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAAAOxSURBVDhPJZR7iBVVHMc/Z+bMnblz987du+uaj81dYSvRCIMk0ahMrKXSJEKIFRIqKEKJ6PVH6F9R9E8QPUxo6bXgH4WlSQoFbWlFQX+kPRVjd93W9Hp39+59zMydmdPv6sCPw5wz53t+38cZ/cLwidvPLCp+GPregEUbMoNKUnJS+STDk3KvjEZGg5eCjmVMbJw0m8yZeERfUM6YE5p+P6wRL4TE803KOUOgDL65CuSmmYBmWLEgNDPyno9dzKOUu0KZ7GPt1eL+YhgRVhZY2qvY8vh1rNqwmEIph63AMqDIMEYR12JmTkzz5+jvxJUGTm8g63pABzRIq036ejKeOLCZeLbG9JFfcaQTK05AKNhaoyxNeKnKsnuGuPuDzZzcdYz27DxWwUf7qknYrHDv8+sx9TqHRsYIWhZuM8RqNXFcaaeeyMcu/bs28Pc7J1n99G2seWwNp/Z9j5e3BKQtGngRQxv7OXfwF66pR5R9i67rA1bu3o63JCCcmmXi3S8Y2NhHJW1ROXyalU9uossT8CRCd6dNCqpNrqDxszbF+hzlVUu4aexRwnPTVI9+S+9961j73lP88fDrtE5VuXbvduyCg2cJ3VRAutohXaKLRcfCGoV4mqHndtKenOLMAy9jpQ6z+7/mhmMvMfjsVs4+9CZ2EotG4hiRJCKH7vEqlOwqlkzmc1X6ls3gD5aYP3iE7txF/NW3Uvv5LxYOfUfpwbvE3gwVh3QerUIB8dCLyhfosmev2OgF0lXpPKY6TWnbnaSfjWKmviRf9OR9PcnUZeyogdYidgeEmMzqaFKu4DErmRA6pRSzPKT9yWsU9r3PotFRwq+Ok9s0jB5czPyLe8nlbKxOgIwkO0uw7UTcKdVw4wqEdbQb4S6JxdrTxK/uwNm6m8K2LWSXZqg/8wr2wgJaulJigJF0qyjEuEW0F0gWGhfJzo6j1w5jTr6B8mqSjR9p7x/HtPJkcwkq7ZHQ9eIE3RTuv5nW+G+ohgCVDdoJxN5ygDn1NurGMZw9hzET30jWFXYYy4lSsXiX5ElDl+Itd4DvMf/W53J/CiTiqvbLLqq7XzbVpIs9AvQIat0OcIoi3VXupEZAhPvcHPEPP1E7cBQzJ/tKK0RcETgJiufdxUG/yQ/IHuE4NQb/fiq/BFmWu2OaEWZe5v9r0P6nQXQmlvu0Antpn1C2iKz2hK7VVo50LzcfOT2+wOYR4nJ6JqeLU3GEarZQ+QYm17FWcuFmpOccnMstEh1NJlHPzv8BV66qv8P9uwsAAAAASUVORK5CYII=';

		$arquivos[] = '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAARABIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCnKo1HxLMl5O4EtxIGkzkjk46/So9V0s6W0X+kxzCQEgxnpg45rX0O9tdP1/VHup47Zy7CJ5VJGRJkg8HqKteJtZtL7QfI+32tzP5qMohjwV67jnA4rzuVcrfU+x9rUjVjGK93T+tv1Or0e+u30SxZrh2ZraMkk5JO0UVFoscn9haf8jf8e0fb/ZFFdaeh4FRR536nB+Mf+Rrv/wDrp/SsaP8A1qf7wooril8TPp8P/Bj6L8j3+1/49If+ua/yooor0lsfDy3Z/9k=';
		
		return $arquivos;

	}
}