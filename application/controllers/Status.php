<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }
        
        $this->load->model('Statuses_Model');
        $this->load->library('form_validation');
    }

    public function create()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('label', 'Título', 'required');
            $this->form_validation->set_rules('description', 'Descrição', 'required');
            $this->form_validation->set_rules('place', 'Ordem', 'required|numeric');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Statuses_Model->add_status($form);
                echo site_url('status/all');
                redirect(site_url('status/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }

        $count = count($this->Statuses_Model->get_statuses());

        $title = 'Novo Status';
        $this->load->view('status/create', compact('count', 'title'));
    }

    public function edit($sid)
    {
        $this->Roles_Model->check_permission('status', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('label', 'Título', 'required');
            $this->form_validation->set_rules('description', 'Descrição', 'required');
            $this->form_validation->set_rules('place', 'Ordem', 'required|numeric');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Statuses_Model->set_status($form);
                redirect(site_url('status/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }
        $status = $this->Statuses_Model->get_status($sid);
        $count = count($this->Statuses_Model->get_statuses());

        $title = 'Editar Status';
        $this->load->view('status/edit', compact('status', 'count', 'title'));
    }

    public function remove($sid)
    {
        $this->Roles_Model->check_permission('status', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->Statuses_Model->remove_status($sid);
        }
        redirect(site_url('status/all'));
    }

    public function all()
    {
        $statuses = $this->Statuses_Model->get_statuses();

        $title = 'Todos os status';
        $this->load->view('status/all', compact('statuses', 'title'));
    }

    public function reorder()
    {
        $ordering = $this->input->post();

        foreach ($ordering as $place) {
            echo $place['place'];
            echo $place['id'];
            $this->Statuses_Model->set_place($place['id'], $place['place']);
        }
    }

}
