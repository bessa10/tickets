<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }
        
        $this->load->model('Notifications_Model');
    }

    public function subscribe($tid)
    {
        $uid = $this->session->userdata('uid');
        $this->Notifications_Model->subscribe($tid, $uid);
        $this->session->set_flashdata('subscribed', TRUE);
        redirect("/ticket/view/$tid");
    }

    public function unsubscribe($tid)
    {
        $uid = $this->session->userdata('uid');
        $this->Notifications_Model->unsubscribe($tid, $uid);
        $this->session->set_flashdata('unsubscribed', TRUE);
        redirect("/ticket/view/$tid");
    }

}
