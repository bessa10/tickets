<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronograma extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        $this->load->model('Tickets_Model');
        $this->load->model('Torre_Model');
        $this->load->model('Classificacao_Model');
        //$this->load->model('Statuses_Model');
        //$this->load->model('Categories_Model');
        //$this->load->model('Projects_Model');
        //$this->load->model('Versions_Model');
        //$this->load->model('Attachments_Model');
        //$this->load->model('Notifications_Model');
        //$this->load->model('Users_Model');
    }

    public function index()
    {
        $this->load->library('form_validation');

        $export_cronograma = false;

        $list_tickets = $this->Tickets_Model->list_tickets_priorizacao();

        $data_hoje = date('Y-m-d');

        $data_nova = date('Y-m-d', strtotime($data_hoje . ' -7 day'));

        $tickets = array();

        $ult_data_prevista  = null;
        $ult_ordem          = null;
        $data_prevista      = null;
        $data_inicio        = null;
        $data_hoje          = date('Y-m-d');

        //$horas_homolog = $this->Tickets_Model->horas_homolog_op(1172);

        //echo $horas_homolog;die;

        foreach ($list_tickets as $row) {
            
            $ultimo_registro = $this->Tickets_Model->receber_ultimo_status($row->tid);
            
            if($row->ordem > 0) {

                // Caso for a primeira ordem
                if($row->ordem == 1) {

                    if($data_inicio == null) {

                        $data_inicio = ($row->primeira_interacao != null) ? $row->primeira_interacao : $row->created; 

                        $dados_tabela['ult_antes_dt_prev']  = $row->created;

                    } else {

                        $data_inicio = ($row->primeira_interacao != null) ? $row->primeira_interacao : $row->created; 

                        $dados_tabela['ult_antes_dt_prev']  = $data_inicio;
                    }

                    $dados_tabela['ult_antes_dt_prev']  = ($data_prevista == null) ? $data_inicio : $data_prevista; 

                    $data_prevista = somar_horas_uteis($data_inicio, $row->horas_previstas);

                    $dados_tabela['ult_ordem'] = $ult_ordem;

                    $ult_data_prevista =  $data_prevista;

                    $dados_tabela['ult_data_prevista']  = $ult_data_prevista;

                    $dados_tabela['data_prevista'] = $data_prevista;

                } else {
                    // outros

                    // caso a ordem for a mesma da anterior, iremos calcular com o mesmo inicio do registro anterior
                    if($row->ordem == $ult_ordem) {

                        $dados_tabela['ult_ordem'] = $ult_ordem;

                        $dados_tabela['ult_antes_dt_prev'] = $dados_tabela['ult_antes_dt_prev'];

                        $ult_data_prevista =  $data_prevista;

                        $data_prevista = somar_horas_uteis($dados_tabela['ult_antes_dt_prev'], $row->horas_previstas);

                        $dados_tabela['ult_data_prevista']  = $ult_data_prevista;

                        $dados_tabela['data_prevista']      = $data_prevista;

                    } else {

                        $dados_tabela['ult_ordem'] = $ult_ordem;

                        $dados_tabela['ult_antes_dt_prev'] = $data_prevista;

                        $ult_data_prevista =  $data_prevista;

                        $data_prevista = somar_horas_uteis($data_prevista, $row->horas_previstas);

                        $dados_tabela['ult_data_prevista']  = $ult_data_prevista;

                        $dados_tabela['data_prevista']      = $data_prevista;
                    }
                }

                $ult_ordem = $row->ordem;

                $dados_tabela['ordem']              = $row->ordem;
                $dados_tabela['tid']                = $row->tid;
                $dados_tabela['name_author']        = $row->name_author;
                $dados_tabela['dep_author']         = $row->dep_author;
                $dados_tabela['torre']              = $this->Torre_Model->get_Torre($row->torre_ticket);
                $dados_tabela['titulo_ticket']      = $row->titulo_ticket;
                $dados_tabela['project']            = $row->project;
                $dados_tabela['created']            = $row->created;
                $dados_tabela['status']             = ($ultimo_registro)?$ultimo_registro->nome_atividade: 'Não iniciado';
                $dados_tabela['iniciado_em']        = ($ultimo_registro)?$ultimo_registro->started: 'Não iniciado';
                $dados_tabela['cor_status']         = $row->cor_status;
                $dados_tabela['horas_previstas']    = $row->horas_previstas;
                $dados_tabela['horas_utilizadas']   = $row->horas_utilizadas;
                $dados_tabela['dev_alocado']        = $row->dev_alocado;
                $dados_tabela['primeira_interacao'] = $row->primeira_interacao;

                // Verificando se o ticket está atrasado
                if(strtotime($data_prevista) < strtotime($data_hoje)) {
                    $dados_tabela['atrasado'] = 1;
                } else {
                    $dados_tabela['atrasado'] = 0;
                }

                // Pegando as horas de apoio
                $users_apoio = $this->Tickets_Model->users_apoio_ticket($row->tid);

                $html_horas     = '';
                $horas_apoio    = 0;

                $html_horas .= "<tr>";
                $html_horas .= "<td>".$row->dev_alocado."</td>";
                $html_horas .= "<td>";

                $horas_owner = $this->Tickets_Model->horas_timesheet_apoio($row->worker_id, $row->tid);

                if($horas_owner) {

                    foreach($horas_owner as $row_hora) {
                        
                        if($row_hora->horas > 0) {
                            
                            $html_horas .= ($row_hora->atividade) ? $row_hora->atividade." - " : "";
                            $html_horas .= ($row_hora->horas > 0) ? $row_hora->horas." hora(s)" : "";
                            $html_horas .= "<br>";

                        } else {

                            $html_horas .= "";
                        }
                    }
                
                } else {

                    $html_horas .= "";
                }

                $html_horas .= "</td>";
                $html_horas .= "</tr>";

                if($users_apoio) {

                    foreach ($users_apoio as $user) {

                        $html_horas .= "<tr>";

                        $dados_hora = $this->Tickets_Model->horas_timesheet_apoio($user->uid, $row->tid);

                        $html_horas .= "<td>".$user->name."</td>";
                        $html_horas .= "<td>";

                        if($dados_hora) {

                            foreach ($dados_hora as $row_hora) {
                                
                                if($row_hora->horas > 0) {
                                
                                    $html_horas .= ($row_hora->atividade) ? $row_hora->atividade." - " : "";
                                    $html_horas .= ($row_hora->horas > 0) ? $row_hora->horas." hora(s)" : "";
                                    $html_horas .= "<br>";

                                } else {

                                    $html_horas .= "";
                                }
                            }

                        } else {

                            $html_horas .= "";
                        }

                        $html_horas .= "</td>";
                        $html_horas .= "</tr>";
                    }
                }

                $dados_tabela['users_apoio'] = $html_horas;

                $tickets[]   = $dados_tabela;
            }
        }

        foreach ($list_tickets as $row) {
            $ultimo_registro = $this->Tickets_Model->receber_ultimo_status($row->tid);
            
            
            if($row->ordem == null) {

                $dados_tabela['ult_ordem'] = $ult_ordem;

                $dados_tabela['ult_antes_dt_prev'] = $data_prevista;

                $ult_data_prevista =  $data_prevista;

                $data_prevista = somar_horas_uteis($data_prevista, $row->horas_previstas);

                $dados_tabela['ult_data_prevista']  = $ult_data_prevista;

                $dados_tabela['data_prevista']      = $data_prevista;

                $ult_ordem = $row->ordem;

                $dados_tabela['ordem']              = $row->ordem;
                $dados_tabela['tid']                = $row->tid;
                $dados_tabela['name_author']        = $row->name_author;
                $dados_tabela['dep_author']         = $row->dep_author;
                $dados_tabela['torre']              = $this->Torre_Model->get_Torre($row->torre_ticket);
                $dados_tabela['titulo_ticket']      = $row->titulo_ticket;
                $dados_tabela['project']            = $row->project;
                $dados_tabela['created']            = $row->created;
                $dados_tabela['status']             = ($ultimo_registro)?$ultimo_registro->nome_atividade: 'Não iniciado';
                $dados_tabela['iniciado_em']        = ($ultimo_registro)?$ultimo_registro->started: 'Não iniciado';
                $dados_tabela['cor_status']         = $row->cor_status;
                $dados_tabela['horas_previstas']    = $row->horas_previstas;
                $dados_tabela['horas_utilizadas']   = $row->horas_utilizadas;
                $dados_tabela['dev_alocado']        = $row->dev_alocado;
                $dados_tabela['primeira_interacao'] = $row->primeira_interacao;

                // Verificando se o ticket está atrasado
                if(strtotime($data_prevista) < strtotime($data_hoje)) {
                    $dados_tabela['atrasado'] = 1;
                } else {
                    $dados_tabela['atrasado'] = 0;
                }

                // Pegando as horas de apoio
                $users_apoio = $this->Tickets_Model->users_apoio_ticket($row->tid);
                $detalhes_chamado = $this->Tickets_Model->receber_ultimo_status_detalhes($row->tid);


                $html_horas     = '';
                $horas_apoio    = 0;

                $html_horas .= "<tr>";
                $html_horas .= "<td>".$row->dev_alocado."</td>";
                $html_horas .= "<td>";

                $horas_owner = $this->Tickets_Model->horas_timesheet_apoio($row->worker_id, $row->tid);

                if($horas_owner) {

                    foreach($horas_owner as $row_hora) {
                        
                        if($row_hora->horas > 0) {
                            
                            $html_horas .= ($row_hora->atividade) ? $row_hora->atividade." - " : "";
                            $html_horas .= ($row_hora->horas > 0) ? $row_hora->horas." hora(s)" : "";
                            $html_horas .= "<br>";

                        } else {

                            $html_horas .= "";
                        }
                    }
                
                } else {

                    $html_horas .= "";
                }

                $html_horas .= "</td>";
                $html_horas .= "</tr>";

                if($users_apoio) {

                    foreach ($users_apoio as $user) {

                        $html_horas .= "<tr>";

                        $dados_hora = $this->Tickets_Model->horas_timesheet_apoio($user->uid, $row->tid);

                        $html_horas .= "<td>".$user->name."</td>";
                        $html_horas .= "<td>";

                        if($dados_hora) {

                            foreach ($dados_hora as $row_hora) {
                                
                                if($row_hora->horas > 0) {
                                
                                    $html_horas .= ($row_hora->atividade) ? $row_hora->atividade." - " : "";
                                    $html_horas .= ($row_hora->horas > 0) ? $row_hora->horas." hora(s)" : "";
                                    $html_horas .= "<br>";

                                } else {

                                    $html_horas .= "";
                                }
                            }

                        } else {

                            $html_horas .= "";
                        }

                        $html_horas .= "</td>";
                        $html_horas .= "</tr>";
                    }
                }


                $dados_tabela['users_apoio'] = $html_horas;

                $tickets[]   = $dados_tabela;
            }
        }
       
        $this->form_validation->set_rules('cod_tid[]', 'Ticket', 'required');
        $this->form_validation->set_rules('ordem[]', 'ordem', 'is_numeric');

        if($this->form_validation->run()==false) {

            if ($this->input->post('exportar_cronograma')) {

                $export_cronograma = true;
            }

            if(form_error('ordem[]')) {
                $this->session->set_flashdata(array(
                    'type' => 'danger',
                    'msg' => form_error('ordem[]')
                ));
            }

            if ($export_cronograma == false) {

                $this->load->view('header');
                $this->load->view('cronograma/index', compact('tickets'));
                $this->load->view('footer');

            } else {
                
                $this->load->view('cronograma/export_cronograma', compact('tickets'));
            }

        } else {

            $tids   = $this->input->post('cod_tid');
            $ordens = $this->input->post('ordem');
            $title = $this->input->post('title');

            if(count($tids) > 0 && count($tids) == count($ordens)) {

                $this->Tickets_Model->delete_ordens();

                for ($i = 0; $i < count($tids); $i++) {
                    $dados = null;

                    if($ordens[$i] > 0) {

                        $dados['ordem'] = $ordens[$i];
                        $dados['title'] = $title[$i];
                        $dados['tid']           = $tids[$i];
                        $dados['uid_insert']    = $this->session->userdata('uid');

                        $this->Tickets_Model->grava_ordem_tid($dados);
                    }
                }
            }

            $this->session->set_flashdata(array(
                'type'  => 'success',
                'msg'   => 'Priorização cadastrada com sucesso'
            ));

            redirect('cronograma');
        }
    }

    public function detalhes_tickets($tid = null)
    {   
        if ($tid == null) {

            $tid = $this->input->post('tid');
        }
        $ticket = $this->Tickets_Model->get_ticket($tid);
        $classificacao = $this->Classificacao_Model->get_classificacao($ticket->classificacao);
        if($classificacao != null){
            $classificacao_dado = $classificacao;
        }else{
            $classificacao_dado = 'sem classificao';
        }

        $detalhes = [];
        
        $detalhes = [$this->Tickets_Model->receber_detalhes_modal($tid),$this->Tickets_Model->receber_ultimo_status($tid),$this->Tickets_Model->horas_homolog_op($tid, 0),$this->Tickets_Model->horas_homolog_op($tid, 1),$this->Tickets_Model->min_homolog_op($tid,0),$this->Tickets_Model->min_homolog_op($tid,1),$classificacao_dado];

        echo json_encode($detalhes);
        die;
    }
}