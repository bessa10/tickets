<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Research extends CI_Controller {

    public function __construct()
    {
        parent::__construct();



        $this->load->model('Researches_Model');
        $this->load->model('Users_Model');
        $this->load->model('Tickets_Model');

    }
    public function create($tid)
    {
        //Validar

        $ticket = $this->Tickets_Model->get_ticket($tid);

        if ($ticket) {


            $validaPesquisa = $this->Researches_Model->valida_pesquisa($tid);

            if($validaPesquisa) {
                $this->session->set_flashdata('success', 'Pesquisa já foi preenchida.');
                redirect('research/agradecimento');
            }

            $this->load->library('form_validation');

            $this->form_validation->set_rules('atendimento', 'de atendimento', 'required');
            $this->form_validation->set_rules('satisfacao', 'de satisfação', 'required');


            if ($this->form_validation->run() != FALSE) {
                $dados = array(
                    "user_id" => $ticket->author_uid,
                    "ticket_id" => $tid,
                    "atendimento" => $this->input->post("atendimento"),
                    "satisfacao" => $this->input->post("satisfacao"),
                    "sugestao" => ($this->input->post("sugestao") == true ? $this->input->post("sugestao") : Null),
                    "date" => date('Y-m-d H:i:s')
                );



                $this->Researches_Model->add_research($dados);
                $this->session->set_flashdata(array(
                    'type' => 'success',
                    'msg' => 'Cadastro preenchido com sucesso'
                ));

                redirect('research/agradecimento');

            } else {
                $title = 'Novo Formulario';
                $this->load->view('research/create', compact('count', 'title'));
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');

            }

        }else{
            $this->session->set_flashdata('error', 'Ticket não existe.');
            redirect(base_url());
        }
    }

    public function all()
    {
        if($this->input->post()) {
            $filtro = $this->input->post();
        } else {
            $filtro = null;
        }

        $users = $this->Users_Model->get_users();
        $forms = $this->Researches_Model->get_researche();



        //$tickets = $this->Tickets_Model->get_researches();
        $pagination = array('total' => count($forms), 'limit' => PER_PAGE);
        $forms = $this->paginate($forms);

        $title = 'Todos os Formularios';
        $this->load->view('research/all', compact('forms','pagination','title','users','satisfacao','filtro', 'atendimento'));
    }

    public function paginate($results)
    {
        $page = $this->input->get('page');
        $offset = ($page - 1) * PER_PAGE;
        $length = PER_PAGE;
        $pages = ceil(count($results) / $length);
        if($page)
        {
            return array_slice($results, $offset, $length);

        }
        return array_slice($results, 0, $length);
    }

    public function agradecimento()
    {

        $this->load->view('research/agradecimento');
    }
}
