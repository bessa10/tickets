<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }

        $this->load->model('Categories_Model');
        $this->load->library('form_validation');
    }

    public function create()
    {
        $this->Roles_Model->check_permission('category', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('name', 'Nome da categoria', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Categories_Model->add_category($form);
                echo site_url('category/all');
                redirect(site_url('category/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }

        $title = 'Nova Categoria';
        $this->load->view('category/create', compact('title'));
    }

    public function edit($cid)
    {
        $this->Roles_Model->check_permission('category', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('name', 'Category name', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Categories_Model->set_category($form);
                redirect(site_url('category/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'There are errors in the category.');
            }
        }
        $category = $this->Categories_Model->get_category($cid);

        $title = 'Editar Categoria';
        $this->load->view('category/edit', compact('category', 'title'));
    }

    public function remove($cid)
    {
        $this->Roles_Model->check_permission('category', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->Categories_Model->remove_category($cid);
        }
        redirect(site_url('category/all'));
    }

    public function all()
    {
        $categories = $this->Categories_Model->get_categories();

        $title = 'Categorias';
        $this->load->view('category/all', compact('categories', 'title'));
    }

    public function all_json()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            $pid = $this->input->post('pid');

            $categories = $this->Categories_Model->get_categories_project($pid);

            echo json_encode($categories); die;

        } else {

            return false;
        }
    }

}
