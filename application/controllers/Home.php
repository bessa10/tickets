<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function index()
    {
        $this->load->model('Users_Model');
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');
        $this->load->model('Projects_Model');

        $this->load->helper('date');

        if(empty($this->Users_Model->get_users()))
        {
            redirect('user/create');
        }

        $summary = $this->Tickets_Model->group_by_status();

        $versions = $this->Versions_Model->get_latest_versions(5);
        $recent = array();
        foreach ($versions as $key => $version) {
            $recent[$key] = $this->Tickets_Model->get_ticket($version->tid);
            $recent[$key]->date = $version->created;
        }

        $next_up = array();

        $next_up_statuses = array_filter(explode(',', $this->Settings_Model->get_setting('next_up_statuses')));
        if ($next_up_statuses) {
            $next_up = $this->Tickets_Model->search(array('status' => $next_up_statuses));
            shuffle($next_up);
            $next_up = array_slice($next_up, 0, 5);
        }

        if($this->input->post()) {
            $filtro = $this->input->post();
        } else {
            $filtro = null;
        }

        $meus_tickets               = $this->Tickets_Model->meus_tickets();
        $tickets_atribuidos         = $this->Tickets_Model->tickets_atribuidos($filtro);
        $tickets_kanban             = $this->Tickets_Model->tickets_atribuidos($filtro, true);
        $tickets_nao_atribuidos     = $this->Tickets_Model->tickets_nao_atribuidos();
        $tickets_em_apoio           = $this->Tickets_Model->tickets_em_apoio($filtro);
        $fechar_ticket_homolog      = $this->encerrar_chamado_cron();



        $projects = $this->Projects_Model->get_projects();
        $users = $this->Users_Model->get_users();

        $workers  = $this->Users_Model->get_users(true);

        $this->load->view('home', compact('summary','recent','next_up','tickets_atribuidos','tickets_nao_atribuidos','projects','filtro','users', 'tickets_kanban','workers', 'tickets_em_apoio', 'meus_tickets'));
    }


    public function teste_email()
    {
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();

        $mail->Host     = 'smtp.office365.com';
        $mail->SMTPAuth = true;

        /*
        $mail->Username = 'moodle@cetrus.com.br';
        $mail->Password = 'cBJCtdb4-WvG';
        */
        $mail->Username = 'sistemas@cetrus.com.br';
        $mail->Password = '@Sys#20cetrus';

        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;

        $mail->SMTPDebug = 2;

        $mail->Debugoutput = 'html';

        $mail->setFrom('sistemas@cetrus.com.br', 'Sistemas');

        $mail->addCC('bruno.chaves@cetrus.com.br', 'Bruno');

        // Add a recipient
        $mail->addAddress('brunobessachaves@gmail.com');

        // Add cc or bcc
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // Email subject
        $mail->Subject = 'Enviar e-mail via SMTP usando PHPMailer no CodeIgniter';

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = "<h1>Enviar e-mail via SMTP usando PHPMailer no CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }


     function send(){
        // Load PHPMailer library
        //$this->load->library('phpmailer_lib');

        $this->load->library('my_phpmailer');

        // PHPMailer object
        $mail = $this->my_phpmailer->load();

        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'smtp.office365.com';
        $config['smtp_user']    = 'postmaster@cetrus.com.br';
        $config['smtp_pass']    = '25l4r4q4n37363o4i4w2j443z2s2';
        $config['smtp_port']    = 587;
        $config['smtp_email']   = 'postmaster@cetrus.com.br';
        $config['smtp_name']    = 'Cetrus Tickets';

        //$mail->SMTPDebug = 1;
        $mail->isSMTP();
        $mail->Host = $config['smtp_host'];
        $mail->SMTPAuth = true;
        $mail->Username = $config['smtp_user'];
        $mail->Password = $config['smtp_pass'];
        $mail->SMTPSecure = 'tls';
        $mail->Port = $config['smtp_port'];


        $mail->setFrom($config['smtp_email'], $config['smtp_name']);
        $mail->addAddress('brunobessachaves@gmail.com', 'Bruno Bessa');
        $mail->addReplyTo($config['smtp_email']);

        $subject = 'Enviar e-mail via SMTP usando PHPMailer no CodeIgniter';

        $body = "<h1>Enviar e-mail via SMTP usando PHPMailer no CodeIgniter</h1>
        <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";

        $mail->isHTML(true);
        $mail->Subject  = utf8_decode($subject);
        $mail->msgHTML(utf8_decode($body));

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }

    public function modelo_email()
    {
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');
        $this->load->model('Notifications_Model');

        $tid = 541;
        $type = 'comment';

        $subscibers = $this->Notifications_Model->get_subscribed($tid);

        $ticket = $this->Tickets_Model->get_ticket($tid);
        $version = $this->Versions_Model->get_latest_version($tid);

        $html_message = $this->load->view('email/ticket', compact('ticket', 'version', 'type'), TRUE);

        echo $html_message;
    }

    public function gut_email()
    {
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');
        $this->load->model('Notifications_Model');

        $tid = 463;

        $subscibers = $this->Notifications_Model->get_subscribed($tid);

        $ticket = $this->Tickets_Model->get_ticket($tid);

        $html_message = $this->load->view('email/informar_gut', compact('ticket'), TRUE);

        echo $html_message;
    }

    public function encerrar_chamado_cron()
    {
        $tickets = $this->Tickets_Model->get_tickets();
        if($tickets){
        foreach($tickets as $ticket){
            if($ticket->previsao_op_assistida == null){
                $op_assistida = 176;
            }else{
                $op_assistida = $ticket->previsao_op_assistida;
            }
            $verifica_timesheet_homolog = $this->Tickets_Model->get_timesheet_homolog($ticket->tid,1);
            if($verifica_timesheet_homolog){
            $obter_horas = $this->Tickets_Model->horas_homolog_op($ticket->tid,1);
            if($obter_horas == null){
                $obter_horas = 0;
            }
           if($obter_horas >= $op_assistida){
                $dados['status'] = 4;
                $dados['completed'] = date('Y-m-d H:i:s');
                $encerrar_chamado = $this->Tickets_Model->valida_dias_uteis($ticket->tid, $dados);

                $dados_ajuste['tid'] = $ticket->tid;
                $dados_ajuste['uid'] = $ticket->worker_uid;
                $dados_ajuste['finished'] = date('Y-m-d H:i:s');
                $this->Tickets_Model->encerra_homolog_ticket($dados_ajuste);

                //echo $ticket->tid.' foi encerrado pois tinha'.$obter_horas.'de'.$op_assistida.'<br>';

            } else{
                // echo $ticket->tid.' não foi encerrado pois tinha'.$obter_horas.'de'.$op_assistida.'<br>';
            }

            }

        }}else{
            //echo 'teste';
        }

    }
}
