<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('uid')) {
            $this->session->sess_destroy();
            redirect('login');
        }
        
        $this->load->model('Roles_Model');
    }

    public function create()
    {        
        $this->load->library('form_validation');

        $this->Roles_Model->check_permission('role', 2);

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('label', 'Título do perfil', 'required');
            $this->form_validation->set_rules('permission_ticket', 'Permissão de ticket', 'required');
            $this->form_validation->set_rules('permission_category', 'Permissão de categoria', 'required');
            $this->form_validation->set_rules('permission_status', 'Permissão de status', 'required');
            $this->form_validation->set_rules('permission_user', 'Permissão de usuários', 'required');
            $this->form_validation->set_rules('permission_role', 'Permissão de perfis de usuário', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Roles_Model->add_role($form);
                echo site_url('role/all');
                redirect(site_url('role/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }

        $title = 'Novo perfil';
        $this->load->view('role/create', compact('title'));
    }

    public function edit($cid)
    {
        $this->Roles_Model->check_permission('role', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $form = $_POST;
        echo $form;
            $this->Roles_Model->set_role($form);
            redirect(site_url('role/all'));
        }
        $role = $this->Roles_Model->get_role($cid);

        $title = 'Edit Role';
        $this->load->view('role/edit', compact('role', 'title'));
    }

    public function remove($cid)
    {
        $this->Roles_Model->check_permission('role', 2);
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->Roles_Model->delete_role($cid);
        }
        redirect(site_url('role/all'));
    }

    public function all()
    {
        $roles = $this->Roles_Model->get_roles();

        $title = 'Perfis de usuário';
        $this->load->view('role/all', compact('roles', 'title'));
    }

}
