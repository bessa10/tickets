<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function selectArray($name, $title, $values, $value_name, $value_id, $default = null, $class = null, $js = null, $padrao = null, $form_error = null)
{
    if($name) {

        $error = ($form_error) ? 'is-invalid' : '';

        $class = ($class) ? ' class="'. $class .' form-control '.$error.' "' : 'class="form-control '.$error.'"';

        $js    = ($js) ? ' onchange="'. $js .'" ' : '';

        $padrao = ($padrao) ? $padrao : 'Selecione';

        if($title) {
            $html = '<label>'.$title.'</label>';
        } else {
            $html = '';
        }

        $html .= '<select name="'. $name .'" id="'.$name.'" ' . $class . $js .'><option value="">'.$padrao.'</option>';

        if($values) {

            foreach ($values as $list) {

                $select = '';

                if($list[$value_id] == $default) {

                    $select = ' selected';
                }

                $html .='<option value="'. $list[$value_id] .'"'. $select .'>'. $list[$value_name] ."</option>\n";
            }
        }

        $html .= "</select>\n";

        $html .='<div class="invalid-feedback">'.$form_error.'</div>';

        return $html;
    }
}

function selectDB($name, $title = null, $values, $value_name, $value_id, $default = null, $class = null, $js = null, $padrao = null, $form_error = null)
{
    if($name) {

        $error = ($form_error) ? 'is-invalid' : '';

        $class = ($class) ? ' class="'. $class .' form-control '.$error.' "' : 'class="form-control '.$error.'"';

        $js    = ($js) ? ' onchange="'. $js .'" ' : '';

        $padrao = ($padrao) ? $padrao : 'Selecione';

        if($title) {
            $html = '<label>'.$title.'</label>';
        } else {
            $html = '';
        }

        $html .= '<select name="'. $name .'" id="'.$name.'" ' . $class . $js .'><option value="">'.$padrao.'</option>';

        if($values) {

            foreach ($values as $list) {

                $select = '';

                if($list->{$value_id} == $default) {

                    $select = ' selected';
                }

                $html .='<option id="id_'. $list->{$value_id} .'" value="'. $list->{$value_id} .'"'. $select .'>'. $list->{$value_name} ."</option>\n";
            }
        }

        $html .= "</select>\n";

        $html .='<div class="invalid-feedback">'.$form_error.'</div>';

        return $html;
    }
}

function getIp() {

    if(!empty($_SERVER['HTTP_CLIENT_IP'])) {

        $ip = $_SERVER['HTTP_CLIENT_IP'];

    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

    } else{

        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

if (!function_exists('formata_preco')) {

    function formata_preco($valor) {
        $moeda = number_format($valor, 2, ',', '.');

        return $moeda;
    }

}
if (!function_exists('formata_cnpj')) {

    function formata_cnpj($valor) {
        $cnpj = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})/", "$1.$2.$3/$4-$5", $valor);

        return $cnpj;
    }

}
if (!function_exists('formata_cep')) {

    function formata_cep($valor) {
        $cep = substr($valor, 0, 5) . '-' . substr($valor, 5, 3);

        return $cep;
    }

}
if (!function_exists('formata_data')) {

    function formata_data($valor) {
        $data = date('d/m/Y', strtotime($valor));

        return $data;
    }

}

if (!function_exists('formata_porc')) {

    function formata_porc($valor) {

        $valor = round($valor,2);

        return $valor;
    }
}

function formata_data_hora($valor) {
    $data = date('d/m/Y H:i:s', strtotime($valor));

    return $data;
}

function getPublicado() {

    return array(
        array('valor' => '0', 'nome' => 'Não'),
        array('valor' => '1', 'nome' => 'Sim')
    );

}

function str_encrypt($texto) {

    if($texto){

        return $this->encryption->encrypt($texto);

    } else {

        return false;
    }
}

function str_decrypt($texto) {

    if($texto){

        return $this->encryption->decrypt($texto);

    } else {

        return false;
    }
}

function valor_gut($gravidade = null, $urgencia = null, $tendencia = null) {

    $valor1 = $gravidade * $urgencia;

    $valor_gut = $valor1 * $tendencia;

    return $valor_gut;
}

function texto_gut($valor_gut = 0) {

    if($valor_gut < 25) {

        $texto_gut = 'Resolver dentro dos prazos estabelecidos no catalogo de serviços';

    } elseif($valor_gut >= 25 && $valor_gut <= 39) {

        $texto_gut = 'Resolver em até 18 horas úteis';

    } elseif ($valor_gut > 39 && $valor_gut <= 79) {

        $texto_gut = 'Resolver em até 9horas úteis';

    } elseif ($valor_gut > 79 && $valor_gut <= 125) {

        $texto_gut = 'Resolver em até 4horas úteis e no mesmo dia';
    } else {

        $texto_gut = '';
    }

    return $texto_gut;
}

function horas_uteis_gut($valor_gut) {

    if($valor_gut >= 25 && $valor_gut <= 39) {

        $horas_uteis = 18;

    } elseif ($valor_gut > 39 && $valor_gut <= 79) {

        $horas_uteis = 9;

    } elseif ($valor_gut > 79 && $valor_gut <= 125) {

        $horas_uteis = 4;

    } else {

        $horas_uteis = 0;
    }

    return $horas_uteis;
}

function catalogo_servicos_gut($id_projeto, $id_categoria) {

    $instance = & get_instance();

    $result = $instance->db
                ->where(array('pid' => $id_projeto, 'cid' => $id_categoria))
                ->get('categories_projects')
                ->row();

    if(!$result) {

        $retorno['atendimento']     = '-';
        $retorno['solucao']         = '-';
        $retorno['txt_atendimento'] = '-';
        $retorno['txt_solucao']     = '-';

    } else {

        $retorno['atendimento']     = $result->atendimento;
        $retorno['solucao']         = $result->solucao;
        $retorno['txt_atendimento'] = ($result->plan_atendimento==1)?'planejado':'até '.$result->atendimento.'hs úteis';
        $retorno['txt_solucao']     = ($result->plan_solucao==1)?'planejado':'até '.$result->solucao.'hs úteis';
    }

    return $retorno;
}

function htmlCheckboxCategorias($pid = null)
{
    $instance = & get_instance();
    $categorias = $instance->db
        ->where(array('removed' => 'N' ))
        ->get('categories')
        ->result();

    $html = '';

    for ($i = 0; $i < count($categorias); $i++) {

        $sla                = '';
        $checked            = '';
        $valor_solucao      = '';
        $valor_atendimento  = '';
        $disabled           = 'disabled';
        $plan_atendimento   = '';
        $plan_solucao       = '';
        //$projetos = $instance->db->get('projects')->result();

        if($pid != null) {
            $projetos_categories = $instance->db
                ->where('pid' , $pid)
                ->get('categories_projects')
                ->result();
            foreach ($projetos_categories as $projeto) {
                if ($categorias[$i]->cid == $projeto->cid ) {
                    $valor_atendimento = $projeto->atendimento;
                    $valor_solucao = $projeto->solucao;
                    $checked = ' checked ';
                    //$sla = $row->sla;
                    $disabled = '';

                    if($projeto->plan_atendimento == 1) {
                        $plan_atendimento = 'checked';
                    }

                    if($projeto->plan_solucao == 1) {
                        $plan_solucao = 'checked';
                    }

                }
            }
        }

        /*
        $html .= '<div class="row" style="align-items: center">';

        $html .= '<div class="form-group col-lg-3" style="height: 20px;">';

        $html .= '<input type="checkbox" onclick="liberaSlaCategoria(this.id)" name="categoria[]" id="'.$categorias[$i]->cid.'" value="'.$categorias[$i]->cid.'" '.$checked.'>&nbsp&nbsp;'. $categorias[$i]->name;

        $html .= '</div>';

        $html .= '<div class="col-lg-9 form-group inputs_sla">';

        $html .= '<input type="number" placeholder="horas atendimento" name="atendimento[]" id="input1_'.$categorias[$i]->cid.'" value="'.$valor_atendimento.'"  '.$disabled.'>';

        $html .= '<input type="checkbox" onclick="liberaSlaCategoria(this.id)" name="plan_atendimento[]" id="plan_atendimento_'.$categorias[$i]->cid.'" value="">&nbsp&nbsp;planejado';


        $html .= '<input type="number" placeholder="horas solução" name="solucao[]" id="input2_'.$categorias[$i]->cid.'" value="'.$valor_solucao.'"  '.$disabled.'>';
        */

        $html .= '<div class="form-group row" style="align-items: center">';

        $html .= '  <div class="col-lg-4" style="height: 20px;">';
        $html .= '      <input type="checkbox" onclick="liberaSlaCategoria(this.id)" name="categoria[]" id="'.$categorias[$i]->cid.'" value="'.$categorias[$i]->cid.'" '.$checked.'>&nbsp&nbsp;'. $categorias[$i]->name;
        $html .= '  </div>';

        $html .= '  <div class="col-lg-2">';
        $html .= '      <input type="number" placeholder="horas atendimento" name="atendimento[]" id="input1_'.$categorias[$i]->cid.'" value="'.$valor_atendimento.'"  '.$disabled.'>';
        $html .= '  </div>';

        $html .= '  <div class="col-lg-2">';
        $html .= '      <input type="checkbox" onclick="planAtend('.$categorias[$i]->cid.')" name="plan_atendimento[]" id="plan_atendimento_'.$categorias[$i]->cid.'" value="1" '.$plan_atendimento.' '.$disabled.'>&nbsp&nbsp;planejado';
        $html .= '  </div>';

        $html .= '  <div class="col-lg-2">';
        $html .= '      <input type="number" placeholder="horas solução" name="solucao[]" id="input2_'.$categorias[$i]->cid.'" value="'.$valor_solucao.'"  '.$disabled.'>';
        $html .= '  </div>';

        $html .= '  <div class="col-lg-2">';
        $html .= '      <input type="checkbox" onclick="planSolu('.$categorias[$i]->cid.')" name="plan_solucao[]" id="plan_solucao_'.$categorias[$i]->cid.'" value="1" '.$plan_solucao.' '.$disabled.'>&nbsp&nbsp;planejado';
        $html .= '  </div>';

        $html .= '</div>';

    }
    return $html;
}


function limpa_html_email($message) {

    $message = preg_replace("/<img[^>]+\>/i", "", $message);

    $message = preg_replace("/<table[^>]+\>/", "", $message); 

    $message = str_replace('a:link', '', $message);

    return $message;

}

function verifica_data_feriado($data) {

    $instance = & get_instance();

    $row_dia = $instance->db
                    ->where('data', $data)
                    ->get('dias_ano')
                    ->row();

    if($row_dia) {

        if($row_dia->feriado == "S") {
    
            return true;
        
        } else {

            return false;
        }

    } else {

        return false;
    }
}

function proximo_dia_util($data)
{
    $timestamp = strtotime($data);

    // Calcula qual o dia da semana de $data
    // O resultado será um valor numérico:
    // 1 -> Segunda ... 7 -> Domingo
    $dia_semana = date('N', $timestamp);

    $data_nova = date('Y-m-d', strtotime($data . ' +1 Weekday'));

    // Verificando se o dia é feriado
    if(verifica_data_feriado($data_nova)==true) {
        // Caso for feriado iremo somar mais um dia
        $data_nova = date('Y-m-d', strtotime($data_nova . ' +1 Weekday'));
    }

    return $data_nova;
}

function diff_atraso($data1, $data2)
{
    $start_date = new DateTime($data1);
    $since_start = $start_date->diff(new DateTime($data2));
    
    $dados['diff_total_days']   = $since_start->days;
    $dados['diff_year']         = $since_start->y;
    $dados['diff_month']        = $since_start->m;
    $dados['diff_day']          = $since_start->d;
    $dados['diff_hour']         = $since_start->h;
    $dados['diff_minute']       = $since_start->i;
    $dados['diff_second']       = $since_start->s;
    
    return $dados;
}


function somar_horas_uteis($created, $horas_uteis) {

    $data_limite    = date('Y-m-d', strtotime($created));
    $data_criacao   = date('Y-m-d', strtotime($created));
    $hora_criacao   = date('H:i:s', strtotime($created));

    // Caso o horário de criação for menor que 8h da manhã iremos definir a criação para este horário
    $data_criacao2 = $data_criacao . ' 08:00:00';
    $hora_criacao = (strtotime($created) < strtotime($data_criacao2)) ? '08:00:00' : $hora_criacao;

    //echo $data_limite . ' - ' . $horas_uteis;die;

    // Fazendo um laço enquanto existir horas para ser adicionada
    while ($horas_uteis > 7) {

        $data_limite = proximo_dia_util($data_limite);

        $dia_semana = date('N', strtotime($data_limite));

        if($dia_semana == 5) {
            // Sexta
            $horas_uteis = $horas_uteis - 8;

        } else {
            // Seg a Qui
            $horas_uteis = $horas_uteis - 9;
        }
    }

    $horas_restantes = $horas_uteis;

    if($horas_restantes > 0) {
        // Somando horas caso existir sobra
        $hora_limite = date('H:i:s', strtotime('+'.$horas_restantes .' hour', strtotime($hora_criacao)));

    } else {

        $hora_limite = $hora_criacao;
    }

    // Verificando se após somar o horário ultrapassou o final do expediente que é 18h (seg a qui) e 17h (sex)
    $dia_semana_limite  = date('N', strtotime($data_limite));
    $inicio_expediente  = '08:00:00';
    $final_expediente   = ($dia_semana_limite == 5) ? '17:00:59' : '18:00:59';

    $dth_limite     = $data_limite . ' ' . $hora_limite; 
    $dth_limite_exp = $data_limite . ' ' . $final_expediente; // final expediente

    if(strtotime($dth_limite) > strtotime($dth_limite_exp)) {
        // Caso a data ultrapassar o expediente, iremos somar 1 dia útil e as horas após 08h
        $start_date = new DateTime($dth_limite);
        $since_start = $start_date->diff(new DateTime($dth_limite_exp));

        /*
        echo $since_start->days.' days total<br>';
        echo $since_start->y.' years<br>';
        echo $since_start->m.' months<br>';
        echo $since_start->d.' days<br>';
        echo $since_start->h.' hours<br>';
        echo $since_start->i.' minutes<br>';
        echo $since_start->s.' seconds<br>';
        */

        $dif_hour   = $since_start->h;
        $dif_min    = $since_start->i;

        $data_limite = proximo_dia_util($data_limite);

        // Somando a diferença em horas
        $hora_limite = date('H:i:s', strtotime('+'.$horas_restantes .' hour', strtotime($inicio_expediente)));

        $dth_limite = $data_limite . ' ' . $hora_limite;

        // Somando a diferença em minutos
        $dth_limite = date('Y-m-d H:i:s', strtotime('+'.$dif_min .' minutes', strtotime($dth_limite)));
    }

    $dia_semana2 = date('N', strtotime($dth_limite));
    $dth_limite2 = date('Y-m-d', strtotime($dth_limite));

    if(verifica_data_feriado($dth_limite2)==true) {
        if($dia_semana2 == 5) {
            
            $dth_limite = somar_horas_uteis($dth_limite, 8);
        
        } else {

            $dth_limite = somar_horas_uteis($dth_limite, 9);
        }
    }

    return $dth_limite;
}

function is_utf8($str) {
    $c=0; $b=0;
    $bits=0;
    $len=strlen($str);
    for($i=0; $i<$len; $i++){
        $c=ord($str[$i]);
        if($c > 128){
            if(($c >= 254)) return false;
            elseif($c >= 252) $bits=6;
            elseif($c >= 248) $bits=5;
            elseif($c >= 240) $bits=4;
            elseif($c >= 224) $bits=3;
            elseif($c >= 192) $bits=2;
            else return false;
            if(($i+$bits) > $len) return false;
            while($bits > 1){
                $i++;
                $b=ord($str[$i]);
                if($b < 128 || $b > 191) return false;
                $bits--;
            }
        }
    }
    return true;
}

if (!function_exists('convert_min_hr')) {
    function convert_min_hr($min) {

        $hora = floor($min / 60);

        return $hora;

    }
}

if (!function_exists('return_final_expediente')) {

    function return_final_expediente($dia) {

        $dia_num = date('N', strtotime($dia));

        // Verificando a quantidade de horas de acordo com o dia da semana
        if ($dia_num == 6 || $dia_num == 7) {

            $final_expediente = '00:00:00';
        
        } elseif ($dia_num == 5) {
            
            $final_expediente = '17:00:00';
        
        } else {

            $final_expediente = '18:00:00';
        }

        return $final_expediente;
    }
}

if (!function_exists('return_diff')) {

    function return_diff($data1, $data2, $tipo = 'd') {

        $data1 = new DateTime($data1);
        $data2 = new DateTime($data2);

        $diferenca  = $data1->diff($data2);

        $retorno = $diferenca->{$tipo}; 

        return $retorno;
    }
}


if (!function_exists('return_intervalo_datas')) {

    function return_intervalo_datas($data1, $data2, $tipo = 'P1D') {

        $data1 = new DateTime($data1);
        $data2 = new DateTime($data2);

        $frequencia = new DateInterval($tipo);
        
        $intervalo  = new DatePeriod($data1, $frequencia ,$data2);

        return $intervalo;
    }
}