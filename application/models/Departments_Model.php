<?php

class Departments_Model extends CI_Model {
  

    public function get_departments()
    {
        $this->db->order_by('name_dep', 'asc');
        $query = $this->db->get('departments');
        return $query->result();
    }

    public function get_department($id_department)
    {
        $this->db->where('id_department', $id_department);
        $query = $this->db->get('departments', 1);
        $row = $query->row();

        return $row;
    }

}
