<?php

class Categories_Model extends CI_Model {

    public $name;
    public $visivel_geral;

    public function add_category($form)
    {
        $this->name = $form['name'];
        $this->visivel_geral = $form['visivel_geral'];

        $this->db->insert('categories', $this);
    }

   public function get_categories($FORM = null)
    {
        $this->db->order_by('name', 'asc');
        $this->db->where('removed', 'N');
        $query = $this->db->get('categories');
        return $query->result();
    }

    public function get_categories_visivel($FORM = null)
    {
        $this->db->order_by('name', 'asc');
        $this->db
        ->where('removed', 'N')
        ->where('visivel_geral','1');
        $query = $this->db->get('categories');
        return $query->result();
    }

    public function get_categories_project($pid = null)
    {
        if($pid != null) {
            if($this->session->userdata('role') != 1){
                $this->db
                ->select('categories.name')
                ->select('b.*')
                ->where('categories.removed', 'N')
                ->where('categories.visivel_geral','1')
                ->where('b.pid', $pid)
                ->join('categories_projects as b', 'b.cid = categories.cid')
                ->group_by('b.cid');
        }else{
            $this->db
            ->select('categories.name')
            ->select('b.*')
            ->where('categories.removed', 'N')
            ->where('b.pid', $pid)
            ->join('categories_projects as b', 'b.cid = categories.cid')
            ->group_by('b.cid');
        }
            $query = $this->db->get('categories');

            return $query->result();

        } else {

            return false;
        }
    }

    public function get_category($cid)
    {
        $this->db->where('cid', $cid);
        $query = $this->db->get('categories', 1);
        return $query->row();
    }

    public function set_category($form)
    {
        $this->name = $_POST['name'];
        $this->visivel_geral = $form['visivel_geral'];
        $this->db->update('categories', $this, array('cid' => $_POST['cid']));
    }

    public function remove_category($cid)
    {
        $this->db->where('cid', $cid);
        $this->db->set('removed', 'Y');
        $this->db->update('categories');
    }

    public function get_catalogo_ticket($pid, $cid)
    {
        $catalogo = $this->db
                        ->where('pid', $pid)
                        ->where('cid', $cid)
                        ->get('categories_projects')
                        ->row();

        return $catalogo;
    }

}
