<?php

class Torre_Model extends CI_Model {

    public function get_torres()
    {
        $query = $this->db->get('torre_ticket');
        return $query->result();
    }


    public function get_Torre($torre_id)
    {
        $this->db->where('id_torre', $torre_id);
        $query = $this->db->get('torre_ticket', 1);
        return $query->row();
    }

}
