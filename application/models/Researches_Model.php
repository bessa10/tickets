<?php

class Researches_Model extends CI_Model {
    public $user_id;
    public $atend;
    public $satisf;

    public function add_research($dados)
    {

        $this->db->insert('pesquisa', $dados);
    }

    public function get_researche()
    {

        $this->db->select('p.pesquisa_id, p.user_id, p.ticket_id, p.atendimento, p.satisfacao, p.date, p.sugestao');
        $this->db->select('u.name');
        $this->db->select('tickets.title');
        $this->db->join('tickets', 'tickets.tid = p.ticket_id');
        $this->db->join('users as u', 'u.uid = tickets.author',  'left');


        $query = $this->db->get('pesquisa as p');

        return $query->result();



    }

    public function get_researches($filtro = null)
    {
        $this->db->select('p.pesquisa_id, p.user_id, p.ticket_id, p.atendimento, p.satisfacao, p.date');
        $this->db->select('u.name');
        $this->db->select('tickets.title');

        $this->db->where('status != 0');
        $uid = $this->session->userdata('uid');

        if($filtro['tid']) {
            $this->db->where('tickets.tid', $filtro['tid']);
        }

        if($filtro['title']) {
            $this->db->like('tickets.title', $filtro['title']);
        }


        if($filtro['author']) {
            $this->db->where('u.name', $filtro['author']);
        }

        if($filtro['data1']) {

            $data1 = $filtro['data1'].' 00:00:00';

            if($filtro['data2']) {

                $data2 = $filtro['data2'] . ' 23:59:59';

            } else {

                $data2 = date('Y-m-d H:i:s');
            }

            $this->db->where("tickets.created BETWEEN '$data1' AND '$data2'");
        }


        if($this->session->userdata('role') == "1") {

            //$this->db->where('worker', $uid)->or_where('author', $uid);

        } else {

            $this->db->where('author', $uid);
        }

        $this->db->join('users', 'author=uid', 'left');
        $this->db->join('users as w', 'tickets.worker=w.uid', 'left');

        $this->db->join('statuses', 'status=sid', 'left');


        $this->db->join('(select tid, created as modified from versions order by created desc limit 1) versions', 'tickets.tid=versions.tid', 'left');
        $this->db->order_by('tickets.created', 'desc');

        $query = $this->db->get('pesquisa');

        return $query->result();
    }

    public function valida_pesquisa($tid)
    {

        $this->db->where('p.ticket_id' , $tid);

        $query = $this->db->get('pesquisa as p');

        return $query->row();
    }
}
