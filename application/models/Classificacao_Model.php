<?php

class Classificacao_Model extends CI_Model {

    public function get_classificacoes()
    {
        $query = $this->db->get('classificacao_ticket');
        return $query->result();
    }


    public function get_classificacao($classificacao_id)
    {
        $this->db->where('id_classificacao', $classificacao_id);
        $query = $this->db->get('classificacao_ticket', 1);
        return $query->row();
    }

}
