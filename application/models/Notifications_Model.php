<?php

class Notifications_Model extends CI_Model {

    public $tid;
    public $uid;

    public function subscribe($tid, $uid)
    {
        $this->tid = $tid;
        $this->uid = $uid;

        $this->db->insert('notifications', $this);
    }

    public function unsubscribe($tid, $uid)
    {
        $this->db->where('tid', $tid);
        $this->db->where('uid', $uid);
        $this->db->delete('notifications');
    }

    public function get_subscribed($tid)
    {
        $this->db->select('nid, tid, username, email');
        $this->db->where('tid', $tid);
        $this->db->join('users', 'notifications.uid = users.uid', 'left');
        return $this->db->get('notifications')->result();
    }

    public function notify($tid, $status = null, $comment = null)
    {
        $this->load->library('email');
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');

        $subscibers = $this->get_subscribed($tid);

        $ticket = $this->Tickets_Model->get_ticket($tid);
        $version = $this->Versions_Model->get_latest_version($tid);

        $vlr_grav   = $ticket->valor_gravidade;
        $vlr_urg    = $ticket->valor_urgencia;
        $vlr_ten    = $ticket->valor_tendencia;
        $gut_env    = $ticket->gut_enviado;

        /*echo 'vlr_grav ' . $vlr_grav . '<br>
        vlr_urg ' . $vlr_urg . '<br>
        vlr_ten ' . $vlr_ten . '<br>
        gut_env ' . $gut_env;die;*/

        $subject            = '';
        $to_email           = '';
        $html_message       = '';
        $notificar_worker   = false;
        $email_worker       = null;


        if($this->session->userdata('uid') != $ticket->author_id && $this->session->userdata('uid') != $ticket->worker_uid) {

            $notificar_worker   = true;
            $email_worker       = $ticket->worker_email;
        }

        // Caso o ticket não estiver gut enviado, iremos verificar se foi preenchido
        if($gut_env == 0) {
            if($vlr_grav > 0 && $vlr_urg > 0 && $vlr_ten > 0) {
                // Enviar aviso GUT
                $subject = "[Priorização] - Ticket Nº {$ticket->tid}";
                $to_email = $ticket->author_email;
                $html_message = $this->load->view('email/informar_gut', compact('ticket'), TRUE);
                // Marcando o ticket com gut enviado
                $this->Tickets_Model->marcar_gut_enviado($tid);
                $this->envia_email($subject, $to_email, $html_message);
                return true;
            }

        } else {

            if($status && $comment) {

                $type = 'status_comment';
                $subject = "[Nova interação] {$ticket->status} - Ticket Nº {$ticket->tid}";
                $to_email = $ticket->author_email;
                $html_message = $this->load->view('email/ticket', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message);
                return true;

            } elseif($status) {

                $type = 'status';
                $subject = "[Nova interação] {$ticket->status} - Ticket Nº {$ticket->tid}";
                $to_email = $ticket->author_email;
                $html_message = $this->load->view('email/ticket', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message);
                return true;

            } elseif($comment) {

                $preview = substr($comment, 0, 50) . '...';
                $type = 'comment';
                $subject = "[Nova interação] - Ticket Nº {$ticket->tid}";
                $to_email = $ticket->author_email;
                $html_message = $this->load->view('email/ticket', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message, $notificar_worker, $email_worker);
                return true;
            }
        }
    }

    public function homolog_ticket($tid, $status = null, $comment = null)
    {
        $this->load->library('email');
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');

        $ticket = $this->Tickets_Model->get_ticket($tid);
        $version = $this->Versions_Model->get_latest_version($tid);
        $homolog_ticket = $this->Tickets_Model->get_timesheet_homolog($tid);
        $obter_email = $this->Tickets_Model->get_user_email($homolog_ticket[0]->uid);

        

       

        

        $subject            = '';
        $to_email           = $obter_email[0]->email;
        $html_message       = '';
        $notificar_worker   = true;
        $email_worker       = $ticket->worker_email;

       

        if($this->session->userdata('uid') != $ticket->author_uid && $this->session->userdata('uid') != $ticket->worker_uid) {
            
            $notificar_worker   = true;
            $email_worker       = $ticket->worker_email;
            
        }
        

        if($homolog_ticket) {
                
                $type = 'status_homolog';
                $subject = "[Nova interação] Homologação";
                $html_message = $this->load->view('email/ticket_homologacao', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message,$notificar_worker,$email_worker);
                return true;

        }


    }

     public function ajuste_ticket($tid, $status = null, $comment = null)
    {

        $this->load->library('email');
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');

        $ticket = $this->Tickets_Model->get_ticket($tid);
        $version = $this->Versions_Model->get_latest_version($tid);
        $ajuste_ticket = $this->Tickets_Model->get_timesheet_homolog($tid);

        
       
        $obter_email = $this->Tickets_Model->get_user_email($ajuste_ticket[0]->uid);

        

        $subject            = '';
        $to_email           =  $ticket->worker_email;
        $html_message       = '';
        $notificar_worker   = false;
        $email_worker       = null;

       

        if($this->session->userdata('uid') != $ticket->author_uid && $this->session->userdata('uid') != $ticket->worker_uid) {
            
            $notificar_worker   = true;
            $email_worker       = $obter_email[0]->email;

           
        }

        if($ajuste_ticket) {

                $type = 'solicitacao_ajuste';
                $subject = "[Nova interação] Solicitação de ajuste";
                $html_message = $this->load->view('email/ticket_homologacao', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message,$notificar_worker,$email_worker);
                return true;

        }


    }

     public function homologacao_finalizada($tid, $status = null, $comment = null)
    {
        
        $this->load->library('email');
        $this->load->model('Tickets_Model');
        $this->load->model('Versions_Model');

        $ticket = $this->Tickets_Model->get_ticket($tid);
        $version = $this->Versions_Model->get_latest_version($tid);
        $homolog_finalizada = $this->Tickets_Model->get_timesheet_homolog($tid);

        $obter_email = $this->Tickets_Model->get_user_email($homolog_finalizada[0]->uid);

        

        $subject            = '';
        $to_email = $ticket->worker_email;
        $html_message       = '';
        $notificar_worker   = false;
        $email_worker       = null;

        if($this->session->userdata('uid') != $ticket->author_uid && $this->session->userdata('uid') != $ticket->worker_uid) {

            $notificar_worker   = true;
            $email_worker       = $obter_email[0]->email;

        }

        if($homolog_finalizada) {

                $type = 'homolog_finalizada';
                $subject = "[Nova interação] Homologação aprovada";
                $html_message = $this->load->view('email/ticket_homologacao', compact('ticket', 'version', 'type'), TRUE);
                $this->envia_email($subject, $to_email, $html_message,$notificar_worker,$email_worker);
                return true;

        }


    }

    public function envia_email($subject, $to_email, $html_message, $notificar_worker = false, $email_worker = null)
    {
        /* 
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();

        $mail->Host     = 'smtp.office365.com';
        $mail->SMTPAuth = true;

        $mail->Username = 'sistemas@cetrus.com.br';
        $mail->Password = '@Sys#20cetrus';

        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;

        $mail->setFrom('sistemas@cetrus.com.br', 'Sistemas');

        if(ENVIRONMENT != 'development') {

            //$mail->addCC('ricardo.ferreira@cetrus.com.br', 'Ricardo');
            //$mail->addBCC('ricardo.ferreira@cetrus.com.br');

        } else {

            //$mail->addCC('marcus.neri@cetrus.com.br');
            //$mail->addBCC('bruno.chaves@cetrus.com.br');
        }
        
       
        if($notificar_worker == true) {

            $mail->addCC($email_worker);
        }
        // Add a recipient

        $mail->addAddress($to_email);

        // Email subject
        $mail->Subject = utf8_decode($subject);

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = $html_message;
        $mail->Body = utf8_decode($mailContent);

        // Send email
        if(!$mail->send()) {

            return false;

        } else{

            return true;
        }

        */
    }
}
