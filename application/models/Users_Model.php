<?php

class Users_Model extends CI_Model {

    public $name;
    public $username;
    public $password;
    public $email;
    public $role;
    public $reset_token;
    public $reset_token_expire;

    public function login($username, $password)
    {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->limit(1);

        $user = $this->db->get()->first_row();

        if($user && password_verify($password, $user->password))
        {
            return $user;
        }
        else
        {
            return false;
        }
    }

    public function login_rede($usuario_rede, $senha_rede)
    {
        if($usuario_rede && $senha_rede) {

            $user = $this->db
                        ->where('username', $usuario_rede)
                        ->where('removed', 'N')
                        ->get('users')
                        ->row();

            if($user) {

                // Caso o usuário existir, iremos atualizar a senha (pois a pessoa pode ter alterado a mesma na rede)
                $update = $this->atualizar_senha($user->uid, $senha_rede);

                if($update) {

                    $retorno = array(
                        'uid'           => $user->uid,
                        'name'          => $user->name,
                        'username'      => $user->username,
                        'email'         => $user->email,
                        'id_department' => $user->id_department,
                        'role'          => $user->role,
                        'atualizar'     => false
                    );

                    return $retorno;

                } else {

                    return false;
                }

            } else {

                // Realizar um cadastro de usuário com os dados da rede
                $uid = $this->cadastrar_usuario_rede($usuario_rede, $senha_rede);

                if($uid) {

                    $retorno = array(
                        'uid'           => $uid,
                        'name'          => null,
                        'username'      => $usuario_rede,
                        'email'         => null,
                        'id_department' => null,
                        'role'          => 2,
                        'atualizar'     => true
                    );

                    return $retorno;

                } else {

                    return false;
                }
            }

        } else {

            return false;
        }
    }

    public function atualizar_senha($uid, $senha_rede)
    {
        if($uid && $senha_rede) {

            $field['password'] = password_hash($senha_rede, PASSWORD_DEFAULT);

            $this->db
                ->where('uid', $uid)
                ->update('users', $field);

            return true;

        } else {

            return false;
        }
    }

    public function cadastrar_usuario_rede($usuario_rede, $senha_rede, $email = null, $name = null)
    {
        if($usuario_rede && $senha_rede) {

            $field['username']  = $usuario_rede;
            $field['name']      = $name;
            $field['password']  = password_hash($senha_rede, PASSWORD_DEFAULT);
            $field['email']     = $email;
            $field['role']      = 2;

            $this->db->insert('users', $field);

            $uid = $this->db->insert_id('users');

            if($uid) {

                return $uid;

            } else {

                return false;
            }

        } else {

            return false;
        }
    }

    public function add_user($form)
    {
        
        $this->name = $form['name'];
        $this->username = $form['username'];
        $this->password = password_hash($form['password'], PASSWORD_DEFAULT);
        $this->email = $form['email'];
        $this->role = $form['role'];
        $this->id_department = $form['id_department'];
        $this->department = $form['department'];

        $this->db->insert('users', $this);
    }

    public function remove_user($uid)
    {
        {
            $this->db->where('uid', $uid);
            $this->db->set('removed', 'Y');
            $this->db->update('users');
        }
    }

    public function get_users($somente_adm = false, $filtro = null)
    {
        if($somente_adm==true) {
            $this->db->where('users.role',1);
        }
        $this->db->where('users.removed', 'N');

        if($filtro['name']) {
            $this->db->like('name', trim($filtro['name']));
        }

        if($filtro['username']) {
            $this->db->like('username', trim($filtro['username']));
        }

        if($filtro['role']) {
            $this->db->where('role', $filtro['role']);
        }

        $this->db->order_by('name', 'asc');

        $this->db->join('roles', 'roles.rid=users.role');

        $query = $this->db->get('users');
        return $query->result();
    }

    public function get_user($uid)
    {
        $this->db->where('uid', $uid);
        $query = $this->db->get('users', 1);
        $row = $query->row();

        $this->username = $row->username;
        $this->password = $row->password;
        $this->email = $row->email;
        $this->role = $row->role;
        $this->id_department = $row->id_department;
        $this->department = $row->department;

        return $row;
    }

    public function set_user($form)
    {

        $this->get_user($form['uid']);
        foreach ($form as $key => $value) {
            if(property_exists($this, $key))
            {
                if($key == 'password')
                {
                    $value = password_hash($value, PASSWORD_DEFAULT);
                }
                $this->{$key} = $value;
            }
        }

        $this->db->update('users', $this, array('uid' => $form['uid']));
    }

    public function get_uid_by_email($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('users', 1);
        $row = $query->row();
        return $row->uid;
    }

    public function get_user_by_token($token)
    {
        $this->db->where('reset_token', $token);
        $query = $this->db->get('users', 1);
        $row = $query->row();
        return $row;
    }

    public function find_user_imap($login_user, $email)
    {
        $user = $this->db
                    ->where('username', $login_user)
                    ->where('email', $email)
                    ->where('removed', 'N')
                    ->get('users')
                    ->row();

        return $user;
    }
}
