<?php

class Projects_Model extends CI_Model {

    public $name;
    public $visivel_geral;

    public function add_project($form)
    {
        $this->name = $form['name'];
        $this->visivel_geral = $form['visivel_geral'];

        $this->db->insert('projects', $this);

        $id = $this->db->insert_id('projects');

        return $id;
    }

    public function set_categories_projects($pid, $dados)
    {
        $this->db->insert('categories_projects', $dados);
    }

    public function remove_categories_projects($pid)
    {
        $this->db->where('pid', $pid)
                 ->delete('categories_projects');

        return true;
    }



    public function get_projects()
    {
        $this->db->order_by('name', 'asc');
        $this->db->where('removed', 'N');
        $query = $this->db->get('projects');
        return $query->result();
    }

    public function get_projects_visivel()
    {
        $this->db->order_by('name', 'asc');
        $this->db->where('removed', 'N');
        $this->db->where('visivel_geral', '1');
        $query = $this->db->get('projects');
        return $query->result();
    }


    public function get_project($pid)
    {
        $this->db->where('pid', $pid);
        $query = $this->db->get('projects', 1);
        return $query->row();
    }

    public function set_project($pid, $form)
    {

        $this->name = $form['name'];
        $this->visivel_geral = $form['visivel_geral'];
        $this->db->where('pid', $pid)
                 ->update('projects', $this);
    }

    public function remove_project($pid)
    {
        $this->db->where('pid', $pid);
        $this->db->set('removed', 'Y');
        $this->db->update('projects');
    }

}
