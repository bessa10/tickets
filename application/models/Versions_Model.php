<?php

class Versions_Model extends CI_Model {

    public $tid;
    public $user;
    public $comment;
    public $difference;

    public function add_version($tid, $comment, $before, $after, $notify = TRUE)
    {
        $this->tid = $tid;
        $this->user = $_SESSION['uid'];
        $this->comment = $comment;

        $difference = new stdClass();
        foreach ($before as $key => $value) {
            if($before->{$key} != $after->{$key})
            {
                $difference->{$key} = array(
                    'antes' => $before->{$key},
                    'depois' => $after->{$key}
                );
            }
        }

        $this->difference = json_encode($difference);
        if($this->difference != '{}' || $comment)
        {
            $this->db->insert('versions', $this);
        }

        if($notify)
        {
            $this->load->model('Notifications_Model');
            $status = $before->sid != $after->sid ? $after->status : null;
            $this->Notifications_Model->notify($tid, $status, $comment);
        }
    }

    public function get_latest_version($tid)
    {
        $this->db->select('versions.*, username');
        $this->db->join('users', 'user=uid', 'left');
        $this->db->where('tid', $tid);
        $this->db->order_by('vid', 'desc');
        $query = $this->db->get('versions', 1);
        return $query->first_row();
    }

    public function get_latest_versions($limit)
    {
        $this->db->select('versions.tid');
        $this->db->select_max('versions.created');
        $this->db->join('tickets', 'tickets.tid = versions.tid', 'inner');

        if($this->session->userdata('role')!="1") {
            $this->db->where('tickets.author', $this->session->userdata('uid'));
        }

        $this->db->group_by('versions.tid');
        $this->db->order_by('versions.created', 'desc');
        $query = $this->db->get('versions', $limit);
        return $query->result();
    }

    public function get_versions($tid)
    {
        $this->db->select('versions.*, username');
        $this->db->join('users', 'user=uid', 'left');
        $this->db->where('tid', $tid);
        $this->db->order_by('vid', 'desc');
        $query = $this->db->get('versions');
        return $query->result();
    }

    public function get_last_version($tid)
    {
        $this->db->select('versions.*, username');
        $this->db->join('users', 'user=uid', 'left');
        $this->db->where('tid', $tid);
        $this->db->order_by('vid', 'desc');
        $query = $this->db->get('versions');
        return $query->row();
    }

    public function get_last_status($tid)
    {
        $this->load->model('Statuses_Model');

        $this->db->where('tid', $tid);
        $this->db->like('difference', '"status":');
        $query = $this->db->get('versions', 1);
        if($query)
        {
            $version = json_decode($query->row()->difference);
            $status = $this->Statuses_Model->get_status_by_label($version->status->before);

            return $status;
        }
        return false;
    }

}
