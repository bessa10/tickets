<?php

class Tickets_Model extends CI_Model {

    public $title;
    public $description;
    public $status;
    public $category;
    public $project;
    public $author;
    public $started;
    public $completed;
    public $worker;
    public $horas_previstas;

    public function add_ticket($form)
    {
        
        $this->load->model('Settings_Model');
        $this->load->model('Attachments_Model');
        $this->title = $form['title'];
        $this->description = $form['description'];
        $this->status = $this->Settings_Model->get_setting('start_status');
        $this->category = $form['category'];
        $this->project = $form['project'];
        $this->author = ($form['author']) ? $form['author'] : $this->session->userdata('uid');
        $this->previsao_analise = $form['previsao_analise'];
        $this->previsao_estudo = $form['previsao_estudo'];
        $this->previsao_dev = $form['previsao_dev'];
        $this->previsao_homolog = $form['previsao_homolog'];
        $this->previsao_ajustes = $form['previsao_ajustes'];
        $this->previsao_op_assistida = $form['previsao_op_assistida'];
        $this->horas_previstas = $form['horas_previstas'];
        $this->torre = ($form['torre'])? $form['torre']: null ;
        $this->classificacao = ($form['classificacao'])? $form['classificacao']: null;

        $this->db->insert('tickets', $this);

        $tid = $this->db->insert_id();

        if(isset($form['attachments']))
        {
            foreach ($form['attachments'] as $attachment) {
                $data = array(
                    'tid' => $tid,
                    'filename' => $attachment['file_name'],
                    'title' => null,
                    'description' => null,
                    'upload_by' => $this->session->userdata('uid')
                );
                $this->Attachments_Model->add_attachment($data);
            }
        }

        return $tid;
    }

    public function add_ticket_email($dados = null)
    {
        if($dados != null) {

            $this->db->insert('tickets', $dados);

            $tid = $this->db->insert_id();

            return $tid;

        } else {

            return false;
        }
        
    }

    public function get_ticket($tid)
    {
        $uid = $this->session->userdata('uid');
        $this->db->select('tickets.tid, tickets.title, tickets.created, tickets.description, tickets.description, tickets.started, tickets.completed, tickets.valor_gravidade, tickets.valor_urgencia, tickets.valor_tendencia, tickets.gut_enviado, tickets.dth_gut_enviado, tickets.status as ticket_tid, tickets.worker, tickets.previsao_analise, tickets.previsao_estudo, tickets.previsao_dev, tickets.previsao_homolog, tickets.previsao_ajustes, tickets.previsao_op_assistida, tickets.horas_previstas, tickets.tkt_aprovado, tickets.uid_aprovado, tickets.dth_aprovado, tickets.torre, tickets.classificacao');
        $this->db->select('a.name as author, a.uid as author_uid, a.email as author_email');
        $this->db->select('w.name as worker, w.uid as worker_uid, w.email as worker_email');
        $this->db->select('s.label as status, s.sid, s.cor_status, s.place');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');
        $this->db->select('ap.name as name_aprovado');
        $this->db->where('tickets.tid', $tid);
        $this->db->join('users as a', 'author=a.uid', 'left');
        $this->db->join('users as w', 'worker=w.uid', 'left');
        $this->db->join('statuses s', 'status=sid', 'left');
        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');
        $this->db->join('users as ap', 'uid_aprovado=ap.uid', 'left');
        if($uid)
        {
            $this->db->select('n.nid as subscribed');
            $this->db->join('notifications n', 'n.tid=tickets.tid AND n.uid=' . $uid, 'left');
        }

        $query = $this->db->get('tickets', 1);
        return $query->row();
    }

    public function get_tickets($filtro = null)
    {
        $this->db->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, w.name as worker, users.username, users.name as autor, tickets.created, sid, versions.modified,tickets.previsao_op_assistida ');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');

        $this->db->where('status != 0');
        $uid = $this->session->userdata('uid');

        if($filtro['tid']) {
            $this->db->where('tickets.tid', $filtro['tid']);
        }

        if($filtro['title']) {
            $this->db->like('tickets.title', $filtro['title']);
        }

        if($filtro['project']) {
            $this->db->where('tickets.project', $filtro['project']);
        }

        if($filtro['author']) {
            $this->db->where('tickets.author', $filtro['author']);
        }

        if($filtro['worker']) {
            $this->db->where('tickets.worker', $filtro['worker']);
        }

        if($filtro['data1']) {

            $data1 = $filtro['data1'].' 00:00:00';

            if($filtro['data2']) {

                $data2 = $filtro['data2'] . ' 23:59:59';

            } else {

                $data2 = date('Y-m-d H:i:s');
            }

            $this->db->where("tickets.created BETWEEN '$data1' AND '$data2'");
        }


        if($this->session->userdata('role') == "1") {

            //$this->db->where('worker', $uid)->or_where('author', $uid);

        } else {

            $this->db->where('author', $uid);
        }

        $this->db->join('users', 'author=uid', 'left');
        $this->db->join('users as w', 'tickets.worker=w.uid', 'left');

        $this->db->join('statuses', 'status=sid', 'left');

        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');

        $this->db->join('(select tid, created as modified from versions order by created desc limit 1) versions', 'tickets.tid=versions.tid', 'left');
        $this->db->order_by('tickets.created', 'desc');

        $query = $this->db->get('tickets');

        return $query->result();
    }

    public function get_kanban_tickets( $tickets_kanban = false)
    {
        $kanban_statuses = $this->Settings_Model->get_setting('kanban_statuses');

        if(!$kanban_statuses)
        {
            return null;
        }

        $kanban_statuses = explode(',', $kanban_statuses);

        $this->db->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, worker, username, tickets.created, sid, versions.modified');
        $this->db->where_in('tickets.status', $kanban_statuses);
        $this->db->join('users', 'author=uid', 'left');
        $this->db->join('statuses', 'status=sid', 'left');
        $this->db->join('(select tid, created as modified from versions order by created desc limit 1) versions', 'tickets.tid=versions.tid', 'left');
        $this->db->order_by('tickets.created', 'desc');

        if($tickets_kanban == true) {
            $this->db->where('tickets.category', 28);
        } else {
            $this->db->where('tickets.category <>', 28);
        }

        $query = $this->db->get('tickets');
        $result = $query->result();

        $kanban_tickets = array();
        foreach ($kanban_statuses as $sid) {
            $status = $this->Statuses_Model->get_status($sid);
            $kanban_tickets[$status->label] = array();
        }
        foreach ($result as $ticket) {
            $kanban_tickets[$ticket->label][] = $ticket;
        }

        return $kanban_tickets;
    }

    public function get_status_kanban($status)
    {
        $this->db->where('tickets.status' , $status);

        $query = $this->db->get('tickets');

        return $query->row();
    }

    public function set_ticket($tid, $form)
    {
        $this->load->model('Statuses_Model');
        $data = array();
        $before = $this->Tickets_Model->get_ticket($tid);

        //var_dump($form);die;

        $form['comment'] = !empty($form['comment']) ? $form['comment'] : null;

        // Verifique se é necessário passar por vários status
        if($form['status'] != -1)
        {
            $statuses = $this->Statuses_Model->get_statuses();
            $before_status_place = -1;
            $after_status_place = -1;
            $prerequisite_statuses = array();

            foreach ($statuses as $key => $status) {
                if($form['status'] == $status->sid)
                {
                    $after_status_place = $status->place;
                    break;
                }
                if($before_status_place != -1)
                {
                    $prerequisite_statuses[] = $status->sid;
                }
                if($before->sid == $status->sid)
                {
                    $before_status_place = $status->place;
                }
            }

            if($after_status_place - $before_status_place > 1)
            {
                foreach ($prerequisite_statuses as $sid)
                {
                    $data = array('status' => $sid);

                    $start_status = $this->Settings_Model->get_setting('work_start_status');
                    if($sid == $start_status)
                    {
                        $data['started'] = date("Y-m-d H:i:s");
                    }

                    $this->db->where('tid', $tid);
                    $this->db->update('tickets', $data);
                    $after = $this->Tickets_Model->get_ticket($tid);
                    $this->Versions_Model->add_version($tid, $form['comment'], $before, $after, FALSE);
                    $before = $after;
                }
            }
        }

        $from_status = $this->Statuses_Model->get_status($before->sid);
        $to_status = $this->Statuses_Model->get_status($form['status']);

        // Verifique se a mudança de status é inválida
        if($before->sid != $form['status'])
        {
            // Não é possível alterar o status se cancelado
            if($from_status->sid == -1)
            {
                $form['status'] = -1;
            }
        }

        // Verifique se a data de início ou a data completa precisam ser desmarcadas devido ao retrocesso do status
        // if($to_status || ( $to_status->sid != -1 && $to_status->place < $from_status->place ) )
        // {
        //     $form['status'] = $from_status->sid;
        // }

        // Verifique se o ticket deve ser definido para iniciar
        $start_status = $this->Settings_Model->get_setting('work_start_status');
        if($before->sid != $form['status'] && $form['status'] == $start_status)
        {
            $form['started'] = date("Y-m-d H:i:s");
        }

        // Verifique se o ticket deve ser definido como concluído
        $complete_status = $this->Settings_Model->get_setting('work_complete_status');
        if($form['status'] == $complete_status)
        {
            $form['completed'] = date("Y-m-d H:i:s");
        }

        if(isset($form['valor_gravidade'])) {

            $data['valor_gravidade'] = $form['valor_gravidade'];
        }

        if(isset($form['valor_urgencia'])) {

            $data['valor_urgencia'] = $form['valor_urgencia'];
        }

        if(isset($form['valor_tendencia'])) {

            $data['valor_tendencia'] = $form['valor_tendencia'];
        }

         if(isset($form['previsao_analise'])) {

            $data['previsao_analise'] = $form['previsao_analise'];
        }

        if(isset($form['previsao_estudo'])) {

            $data['previsao_estudo'] = $form['previsao_estudo'];
        }

        if(isset($form['previsao_dev'])) {

            $data['previsao_dev'] = $form['previsao_dev'];
        }

        if(isset($form['previsao_homolog'])) {

            $data['previsao_homolog'] = $form['previsao_homolog'];
        }

        if(isset($form['previsao_ajustes'])) {

            $data['previsao_ajustes'] = $form['previsao_ajustes'];
        }

        if(isset($form['previsao_op_assistida'])) {

            $data['previsao_op_assistida'] = $form['previsao_op_assistida'];
        }

        if(isset($form['horas_previstas'])) {

            $data['horas_previstas'] = $form['horas_previstas'];
        }
         if(isset($form['torre'])) {

            $data['torre'] = ($form['torre'])?$form['torre']:null;
        }
        if(isset($form['classificacao'])) {

            $data['classificacao'] = ($form['classificacao'])?$form['classificacao']: null;
        }
        foreach($form as $key=>$value)
        {
            if(property_exists('Tickets_Model', $key))
            {
                $data[$key] = $value;
            }
        }

        $this->db->where('tid', $tid);
        $this->db->update('tickets', $data);
        $after = $this->Tickets_Model->get_ticket($tid);
        $this->Versions_Model->add_version($tid, $form['comment'], $before, $after);

        if(!empty($form['attachments']))
        {
            foreach ($form['attachments'] as $attachment) {
                if(!empty($attachment->aid))
                {
                    continue;
                }
                $data = array(
                    'tid' => $tid,
                    'title' => $attachment['orig_name'],
                    'filename' => $attachment['file_name'],
                    'title' => null,
                    'description' => null,
                    'upload_by' => $this->session->userdata('uid')
                );
                $this->Attachments_Model->add_attachment($data);
            }
        }
    }

    public function get_by_status($sid = NULL)
    {
        if($sid)
        {
            $this->db->where('sid', $sid);
        }
        $this->db->order_by('sid', 'asc');
        return $this->get_tickets();
    }

    public function get_by_user($uid = NULL)
    {
        if($uid)
        {
            $this->db->where('author', $uid);
        }
        $this->db->order_by('author', 'asc');
        return $this->get_tickets();
    }

    public function get_by_category($cid = NULL)
    {
        if($cid)
        {
            $this->db->where('category', $cid);
        }
        $this->db->order_by('category', 'asc');
        return $this->get_tickets();
    }

    public function get_by_project($pid = NULL)
    {
        if($pid)
        {
            $this->db->where('project', $pid);
        }
        $this->db->order_by('project', 'asc');
        return $this->get_tickets();
    }

    public function group_by_status()
    {
        if($this->session->userdata('role')!="1") {
            $this->db->where('author', $this->session->userdata('uid'));
        }

        $this->db->select('status, cor_status, label, COUNT(*) as count');
        $this->db->group_by('status');
        $this->db->join('statuses', 'status=sid');
        $query = $this->db->get('tickets');
        return $query->result();
    }

    public function get_recently_updated()
    {
        $this->db->limit(5);
        $this->db->order_by('modified', 'desc');
        return $this->get_tickets();
    }

    public function search($form)
    {

        $exclude = !empty($form['exclude']) ? $form['exclude'] : array();

        if(!empty($form['author']))
        {
            if( !in_array('author', $exclude ))
            {
                $this->db->where_in('author', $form['author']);
            }
            else
            {
                $this->db->where_not_in('author', $form['author']);
            }
            if(!empty($form['and-author']))
            {
                $this->db->where_in('author', $form['author_and']);
            }
        }

        if(!empty($form['worker']))
        {
            if( !in_array('worker', $exclude ))
            {
                $this->db->where_in('worker', $form['worker']);
            }
            else
            {
                $this->db->where_not_in('worker', $form['worker']);
            }
            if(!empty($form['and-worker']))
            {
                $this->db->where_in('worker', $form['worker_and']);
            }
        }

        if(!empty($form['status']))
        {
            if( !in_array('status', $exclude ))
            {
                $this->db->where_in('status', $form['status']);
            }
            else
            {
                $this->db->where_not_in('status', $form['status']);
            }
            if(!empty($form['and-status']))
            {
                $this->db->where_in('status', $form['status_and']);
            }
        }

        if(!empty($form['category']))
        {
            if( !in_array('category', $exclude ))
            {
                $this->db->where_in('category', $form['category']);
            }
            else
            {
                $this->db->where_not_in('category', $form['category']);
            }
            if(!empty($form['and-category']))
            {
                $this->db->where_in('category', $form['category_and']);
            }
        }

        if(!empty($form['project']))
        {
            if( !in_array('project', $exclude ))
            {
                $this->db->where_in('project', $form['project']);
            }
            else
            {
                $this->db->where_not_in('project', $form['project']);
            }
            if(!empty($form['and-project']))
            {
                $this->db->where_in('project', $form['project_and']);
            }
        }

        if(!empty($form['created_from']))
        {
            $created_from = $form['created_from'] . ' 00:00:00';

            if(!empty($form['created_to']))
            {
                $created_to = $form['created_to'] . ' 23:59:59';

                $this->db->where("tickets.created BETWEEN '$created_from' AND '$created_to'");
            }
            else
            {
                $created_to = date('Y-m-d', strtotime($created_from . '+1 day')) . ' 23:59:59';
                $this->db->where("tickets.created BETWEEN '$created_from' AND '$created_to'");
            }
        }

        if(!empty($form['modified_from']))
        {
            $modified_from = $form['modified_from'] . ' 00:00:00';

            if(!empty($form['modified_to']))
            {
                $modified_to = $form['modified_to'] . ' 23:59:59';
                $this->db->where("modified BETWEEN '$modified_from' AND '$modified_to'");
            }
            else
            {
                $modified_to = date('Y-m-d', strtotime($modified_from . '+1 day')) . ' 23:59:59';

                $this->db->where("modified BETWEEN '$modified_from' AND '$modified_to'");
            }
        }

        return $this->Tickets_Model->get_tickets();
    }

    public function tickets_atribuidos($filtro = null, $tickets_kanban = false)
    {
        $uid = $this->session->userdata('uid');

        $this->db->select('
            tickets.tid,
            tickets.title,
            tickets.created,
            tickets.description,
            tickets.started,
            tickets.completed,
            tickets.valor_urgencia,
            tickets.valor_gravidade,
            tickets.valor_tendencia
        ');
        $this->db->select('a.name as author');
        $this->db->select('s.label as status, s.sid, s.cor_status');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');
        $this->db->join('users as a', 'author=a.uid', 'left');
        $this->db->join('statuses s', 'status=sid', 'left');
        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');

        $this->db->where('tickets.worker', $uid);
        $this->db->where('tickets.status NOT IN(-1,4)');

        if($tickets_kanban == true) {
            $this->db->where('c.cid', 28);
        } else {
            $this->db->where('c.cid <>', 28);
        }

        if($filtro['tid']) {
            $this->db->where('tickets.tid', $filtro['tid']);
        }

        if($filtro['title']) {
            $this->db->like('tickets.title', $filtro['title']);
        }

        if($filtro['project']) {
            $this->db->where('tickets.project', $filtro['project']);
        }

        if($filtro['author']) {
            $this->db->where('tickets.author', $filtro['author']);
        }
        if($filtro['worker']) {
            $this->db->where('tickets.worker', $filtro['worker']);
        }

        if($filtro['data1']) {

            $data1 = $filtro['data1'].' 00:00:00';

            if($filtro['data2']) {

                $data2 = $filtro['data2'] . ' 23:59:59';

            } else {

                $data2 = date('Y-m-d H:i:s');
            }

            $this->db->where("tickets.created BETWEEN '$data1' AND '$data2'");
        }

        $this->db->order_by('tickets.tid', 'DESC');

        $query = $this->db->get('tickets');

        return $query->result();
    }

    public function tickets_em_apoio($filtro = null)
    {
        $uid = $this->session->userdata('uid');

        $this->db->select('
            tickets.tid,
            tickets.title,
            tickets.created,
            tickets.description,
            tickets.started,
            tickets.completed,
            tickets.valor_urgencia,
            tickets.valor_gravidade,
            tickets.valor_tendencia
        ');

        $this->db->select('a.name as author');
        $this->db->select('s.label as status, s.sid, s.cor_status');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');
        $this->db->join('users as a', 'author=a.uid', 'left');
        $this->db->join('statuses s', 'status=sid', 'left');
        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');
        $this->db->join('apoio_tickets ap', 'ap.tid = tickets.tid');
        //$this->db->where('tickets.author <>', $uid);
        $this->db->where('ap.uid', $uid);
        $this->db->where('ap.excluido', 0);
        $this->db->where('tickets.status NOT IN(-1,4)');

        if($filtro['tid']) {
            $this->db->where('tickets.tid', $filtro['tid']);
        }

        if($filtro['title']) {
            $this->db->like('tickets.title', $filtro['title']);
        }

        if($filtro['project']) {
            $this->db->where('tickets.project', $filtro['project']);
        }

        if($filtro['author']) {
            $this->db->where('tickets.author', $filtro['author']);
        }
        if($filtro['worker']) {
            $this->db->where('tickets.worker', $filtro['worker']);
        }

        if($filtro['data1']) {

            $data1 = $filtro['data1'].' 00:00:00';

            if($filtro['data2']) {

                $data2 = $filtro['data2'] . ' 23:59:59';

            } else {

                $data2 = date('Y-m-d H:i:s');
            }

            $this->db->where("tickets.created BETWEEN '$data1' AND '$data2'");
        }

        $this->db->group_by('tickets.tid');

        $this->db->order_by('tickets.tid', 'DESC');

        $query = $this->db->get('tickets');

        return $query->result();
    }

    public function meus_tickets()
    {
        $uid = $this->session->userdata('uid');

        $this->db->select('
            tickets.tid,
            tickets.title,
            tickets.created,
            tickets.description,
            tickets.started,
            tickets.completed,
            tickets.valor_urgencia,
            tickets.valor_gravidade,
            tickets.valor_tendencia
        ');
        $this->db->select('a.name as author');
        $this->db->select('s.label as status, s.sid, s.cor_status');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');
        $this->db->join('users as a', 'author=a.uid', 'left');
        $this->db->join('statuses s', 'status=sid', 'left');
        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');

        $this->db->where('tickets.author', $uid);
        $this->db->where('tickets.status NOT IN(-1,4)');

        $this->db->order_by('tickets.tid', 'DESC');

        $query = $this->db->get('tickets');

        return $query->result();
    }

    public function tickets_nao_atribuidos()
    {
        $uid = $this->session->userdata('uid');

        $this->db->select('
            tickets.tid,
            tickets.title,
            tickets.created,
            tickets.description,
            tickets.started,
            tickets.completed
        ');
        $this->db->select('a.name as author');
        $this->db->select('s.label as status, s.sid, s.cor_status');
        $this->db->select('c.name as category, c.cid');
        $this->db->select('p.name as project, p.pid');
        $this->db->where('tickets.worker IS NULL')->or_where('tickets.worker', '');
        $this->db->where('tickets.status NOT IN(-1,4)');
        $this->db->join('users as a', 'author=a.uid', 'left');
        //$this->db->join('users as w', 'worker=w.uid', 'left');
        $this->db->join('statuses s', 'status=sid', 'left');
        $this->db->join('categories c', 'category=cid', 'left');
        $this->db->join('projects p', 'project=pid', 'left');
        $this->db->order_by('tickets.tid', 'DESC');

        $query = $this->db->get('tickets');

        return $query->result();
    }

    public function list_gravidade()
    {
        $gravidade = $this->db
                        ->order_by('valor_gravidade')
                        ->get('gut_gravidade')
                        ->result();

        return $gravidade;
    }

    public function list_urgencia()
    {
        $urgencia = $this->db
                        ->order_by('valor_urgencia')
                        ->get('gut_urgencia')
                        ->result();

        return $urgencia;
    }

    public function list_tendencia()
    {
        $tendencia = $this->db
                        ->order_by('valor_tendencia')
                        ->get('gut_tendencia')
                        ->result();

        return $tendencia;
    }

    public function find_atend_iniciado($tid)
    {
        $atend = $this->db
                    ->select('timesheet.*')
                    ->select('users.name')
                    ->join('users', 'users.uid = timesheet.uid')
                    ->where('timesheet.tid', $tid)
                    ->where('timesheet.uid', $this->session->userdata('uid'))
                    ->where('timesheet.started IS NOT NULL')
                    ->where('timesheet.finished IS NULL')
                    ->order_by('timesheet.started','DESC')
                    ->get('timesheet')
                    ->row();

        if($atend) {

            return $atend;

        } else {

            return false;
        }
    }

    public function find_atend_finalizado($tid)
    {
        $atend = $this->db
                    ->select('timesheet.*')
                    ->select('users.name')
                    ->join('users', 'users.uid = timesheet.uid')
                    ->where('timesheet.tid', $tid)
                    ->where('timesheet.uid', $this->session->userdata('uid'))
                    ->where('timesheet.started IS NOT NULL')
                    ->where('timesheet.finished IS NOT NULL')
                    ->order_by('timesheet.started','DESC')
                    ->get('timesheet')
                    ->row();

        if($atend) {

            return $atend;

        } else {

            return false;
        }
    }

    public function set_atendimento($form = null, $finaliza = false)
    {
        if($form != null) {

            // finalizar atendimento
            if($finaliza == true) {

                $dados['finished'] = date('Y-m-d H:i:s');

                $this->db
                    ->where('tid', $form['tid'])
                    ->where('uid', $this->session->userdata('uid'))
                    ->where('finished IS NULL')
                    ->update('timesheet', $dados);

                return true;

            } else {
                // inicia atendimento
                $dados['tid']           = $form['tid'];
                $dados['uid']           = $this->session->userdata('uid');
                $dados['started']       = date('Y-m-d H:i:s');
                $dados['local_id']      = $form['locais'];
                $dados['id_atividade']  = $form['id_atividade'];

                $this->db->insert('timesheet', $dados);

                return true;
            }

        } else {

            return false;
        }
    }

    public function fechar_atendimento_outro_ticket($tid)
    {
        $dados['finished'] = date('Y-m-d H:i:s');

        $this->db
            ->where('tid <>', $tid)
            ->where('uid', $this->session->userdata('uid'))
            ->where('started IS NOT NULL')
            ->where('finished IS NULL')
            ->update('timesheet', $dados);

        return true;
    }

    public function marcar_gut_enviado($tid = null)
    {
        if($tid != null) {

            $dados['gut_enviado']       = 1;
            $dados['dth_gut_enviado']   = date('Y-m-d H:i:s');

            $this->db->where('tid', $tid)->update('tickets', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function set_ticketid($tid, $dados)
    {
        if($tid != null) {


            $this->db->where('tid', $tid)->update('tickets', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function set_local_trabalho($tid, $dados)
    {
        if($tid != null) {


            $this->db->where('tid', $tid)->update('timesheet', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function set_worked($tid)
    {
        if ($tid != null) {
            $dados['worker'] = $this->session->userdata('uid');
            $this->db->where('tid', $tid)->update('tickets', $dados);
            return true;
        }
        else {
            return false;
        }
    }

    function valida_dias_uteis($tid, $dados)
    {

        if($tid != null) {

            $this->db->where('tid', $tid)->update('tickets', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function get_locais_trabalho()
    {
        $this->db->select('locais_trabalho.local_id, locais_trabalho.locais');
        $this->db->select('preferencia_local_usuario.local_id, preferencia_local_usuario.id_user, preferencia_local_usuario.preferencia');
        $this->db->join('preferencia_local_usuario', 'preferencia_local_usuario.local_id = locais_trabalho.local_id');
        $this->db->where('preferencia_local_usuario.id_user', $this->session->userdata('uid'));

        $query = $this->db->get('locais_trabalho');
        return $query->result();
    }

    public function list_locais_trabalho()
    {
        $locais = $this->db->get('locais_trabalho')->result();

        return $locais;
    }

    public function list_tickets_atrasados()
    {   
        $status_not = array(-1, 4);

        $tickets = $this->db
                        //->where_in('tid', $tids)
                        ->where_not_in('status', $status_not)
                        ->order_by('created ASC')
                        //->limit(10)
                        ->get('tickets')
                        ->result();

        return $tickets;
    }

    public function primeiro_atendimento($tid)
    {
        $atend = $this->db
                    ->select('MIN(started) AS data_atend')
                    ->where('tid', $tid)
                    ->get('timesheet')
                    ->row();

        return $atend;
    }

    public function atualiza_ticket($tid, $dados_update = null)
    {
        
        if($dados_update != null) {

            $this->db->where('tid', $tid)->update('tickets', $dados_update);

            return true;

        } else {

            return false;
        }
    }

     public function list_tickets_priorizacao()
    {
        $sql = "
            SELECT
                pri.ordem AS ordem,
                tk.tid AS tid,
                usa.name AS name_author,
                dp.name_dep AS dep_author,
                tk.torre AS torre_ticket,
                tk.title AS titulo_ticket,
                pr.name AS project,
                tk.created AS created,
                st.label AS status,
                st.cor_status,
                tk.author AS author_id,
                tk.worker AS worker_id,
                tk.horas_previstas AS horas_previstas,
                (SELECT ROUND(sum(TIMESTAMPDIFF(minute,tm.started,tm.finished))/60) FROM timesheet tm where tm.tid = tk.tid) AS horas_utilizadas,
                (SELECT name FROM users usw WHERE usw.uid = tk.worker) AS dev_alocado,
                (SELECT MIN(started) FROM timesheet WHERE tid = tk.tid LIMIT 1) AS primeira_interacao
            FROM tickets tk
                JOIN users usa ON usa.uid = tk.author
            LEFT JOIN departments dp ON dp.id_department = usa.id_department
                JOIN projects pr ON pr.pid = tk.project
                JOIN statuses st ON st.sid = tk.status
                LEFT JOIN priorizacao pri ON pri.tid = tk.tid 
            WHERE category = 28 AND status not IN(4,-1)
            ORDER BY pri.ordem, tk.tid
        ";

        //echo $sql;die;
        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function receber_ultimo_status($tid = null)
    {
        if($tid != null){
            $sql = "SELECT 
                        timesheet.*, 
                        atividades_ticket.*,
                        tickets.*
                        FROM timesheet
                        JOIN atividades_ticket ON atividades_ticket.id_atividade = timesheet.id_atividade
                        JOIN tickets ON tickets.tid = timesheet.tid 
                        WHERE timesheet.tid = $tid
                        ORDER BY timesheet.id_timesheet DESC";
        }

        $result = $this->db->query($sql)->row();
        if($result != null){
           return $result; 
        }else{
            return false;
        }
    }

    public function receber_detalhes_modal($tid = null)
    {
        $sql = "SELECT 
atv.nome_atividade,
atv.ordem,
(SELECT tickets.previsao_analise 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_analise,
(SELECT tickets.previsao_estudo 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_estudo,
(SELECT tickets.previsao_dev 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_dev,
(SELECT tickets.previsao_homolog 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_homolog,
(SELECT tickets.previsao_ajustes 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_ajustes,
(SELECT tickets.previsao_op_assistida 
    FROM tickets 
    WHERE tid = $tid
LIMIT 1) AS previsao_op_assistida,
(SELECT SUM(TIMESTAMPDIFF(hour,started,finished)) 
    FROM timesheet
   WHERE id_atividade = atv.id_atividade
        AND tid = $tid
    LIMIT 1
) AS horas_utilizadas,
(SELECT max(started) 
    FROM timesheet 
    WHERE id_atividade = atv.id_atividade 
        AND tid = $tid
    ORDER BY id_timesheet ASC 
LIMIT 1) AS ultima_locacao,
(SELECT DATE_FORMAT(min(started), '%d/%m/%Y')
    FROM timesheet
    WHERE id_atividade = atv.id_atividade
        AND tid = $tid
    ORDER BY id_timesheet ASC
LIMIT 1) AS primeira_locacao,
(SELECT users.name 
    FROM timesheet
    JOIN users ON users.uid = timesheet.uid 
    WHERE timesheet.id_atividade = atv.id_atividade 
        AND timesheet.tid = $tid
    ORDER BY timesheet.id_timesheet DESC 
LIMIT 1) AS responsavel
FROM atividades_ticket atv 
WHERE visivel_dev = 1";

            $result = $this->db->query($sql)->result();
                if($result != null){
                    return $result; 
                }else{
                    return false;
                }
    }

     public function receber_ultimo_status_detalhes($tid = null)
    {
        if($tid != null){
            $sql = "SELECT 
                        timesheet.*, 
                        atividades_ticket.*,
                        tickets.*
                        FROM timesheet
                        JOIN atividades_ticket ON atividades_ticket.id_atividade = timesheet.id_atividade
                        JOIN tickets ON tickets.tid = timesheet.tid 
                        WHERE timesheet.tid = $tid
                        AND visivel_dev = 1
                        ORDER BY atividades_ticket.ordem DESC";
        }

        $result = $this->db->query($sql)->result();
        if($result != null){
           return $result; 
        }else{
            return false;
        }
    }

    public function timesheet_7dias($tid = null)
    {

    }

    public function delete_ordem_tid($tid = null)
    {
        if($tid != null) {

            $this->db->where('tid', $tid)->delete('priorizacao');

            return true;

        } else {

            return false;
        }
    }

    public function get_ordem_tid($tid = null)
    {
        if($tid != null) {

            $ordem = $this->db
                        ->where('tid', $tid)
                        ->get('priorizacao')
                        ->row();

            return $ordem;

        } else {

            return false;
        }
    }

    public function grava_ordem_tid($dados = null,  $tid = null)
    {
        if($dados != null) {

            //if($tid != null) {
                // update
                //$this->db->where('tid', $tid)->update('priorizacao', $dados);

            //} else {
                // insert
                $this->db->insert('priorizacao', $dados);
            //}

            return true;

        } else {

            return false;
        }
    }

    public function delete_ordens()
    {
        $this->db->where('ordem >', 0)->delete('priorizacao');

        return true;
    }

        public function list_workes_apoio($tid, $users_all = false)
    {
        $sql = "
            SELECT
                users.*
            FROM users
            WHERE removed = 'N'
               
        ";
         /*AND uid NOT IN(SELECT uid FROM apoio_tickets WHERE tid = ".$tid." AND excluido = 0) retirar da querie para que quem estiver em apoio também possa homologar*/  
        if (!$this->session->userdata('uid')) {

            redirect('user/login');
            
        } else {
            
            $sql .= " AND uid <> " . $this->session->userdata('uid') ."";
        }

        if($users_all == true) {

            /* liberar para todos os usuarios homologarem.
            $sql .= " AND role <> 1";
            */

        } else {
            
            $sql .= " AND role = 1";
        }

        $sql .= "
            ORDER BY name ASC
        ";

        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function find_apoio_ticket($tid)
    {
        $apoios = $this->db
                    ->select('apoio_tickets.*')
                    ->select('users.username, users.name, users.role')
                    ->join('users', 'users.uid = apoio_tickets.uid')
                    ->where('apoio_tickets.tid', $tid)
                    ->where('apoio_tickets.excluido', 0)
                    ->order_by('users.name')
                    ->get('apoio_tickets')
                    ->result(); 

        return $apoios;
    }

    public function verifica_apoio_ticket($uid = null, $tid = null)
    {
        if($uid != null && $tid != null) {

            $apoio = $this->db
                        ->where('uid', $uid)
                        ->where('tid', $tid)
                        ->where('excluido', 0)
                        ->get('apoio_tickets')
                        ->row();

            return $apoio;

        } else {

            return false;
        }
    }

    public function insert_apoio_ticket($dados = null)
    {
        if($dados != null) {

            $this->db->insert('apoio_tickets', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function excluir_apoio_ticket($dados = null, $uid = null, $tid = null)
    {
        if($dados != null && $uid != null && $tid != null) {

            $this->db
                ->where('uid', $uid)
                ->where('tid', $tid)
                ->update('apoio_tickets', $dados);

            return true;

        } else {

            return false;
        }
    }

    public function users_apoio_ticket($tid)
    {
        $apoio = $this->db
                    ->select('apoio_tickets.*')
                    ->select('users.username')
                    ->select('users.name, users.id_department, users.funcao')
                    ->join('users', 'users.uid = apoio_tickets.uid')
                    ->where('apoio_tickets.tid', $tid)
                    ->where('apoio_tickets.excluido', 0)
                    ->get('apoio_tickets')
                    ->result();

        return $apoio;
    }

    
    

    public function horas_timesheet_apoio($uid, $tid)
    {
        $sql = "
            SELECT
                atv.nome_atividade AS 'atividade',
                ROUND(SUM(TIMESTAMPDIFF(minute,tm.started,tm.finished)) / 60) AS horas
            FROM timesheet tm
            LEFT JOIN atividades_ticket atv ON atv.id_atividade = tm.id_atividade
            WHERE tid = ".$tid." AND uid = ".$uid."
            GROUP BY tm.id_atividade
        ";

        $result = $this->db->query($sql)->result();



        return $result;
    }

    public function get_atividades($cid = null)
    {
        if($this->session->userdata('role') == "1") {

            $visivel = array(1, 0);

        } else {

            $visivel = array(1);
        }

        // verificando se o ticket é dev para mostrar somente as atividades para isso
        if ($cid == 28) {

            $this->db->where('visivel_dev', 1);

        } else {

            $this->db->where('visivel_tkt', 1);
        }

        $this->db
            ->where_in('visivel_geral', $visivel)
            ->order_by('ordem');

        $atividades = $this->db->get('atividades_ticket')->result();

        return $atividades;
    }

    public function grava_timesheet_homolog($dados)
    {
        if($dados){
            $this->db->insert('timesheet_homolog',$dados);
            return true;
        }else{
            return false;
        }
    }

    public function marca_timesheet($dados2)
    {
        if($dados2){
            $this->db->insert('timesheet',$dados2);
            return true;
        }else{
            return false;
        }
    }

    public function valida_timesheet_homolog($tid,$uid)
    {

        $sql = "SELECT * 
                FROM timesheet_homolog
                WHERE tid = $tid
                AND uid = $uid
                AND finished IS NULL
                ";

        $result = $this->db->query($sql)->result();

        var_dump($result);
        
        return $result;
    
    }

   public function inserir_ajuste_comentario($dados = null)
    {
        if($dados != null) {

            $this->db->insert('versions', $dados);

            return true;

        } else {

            return false;
        }
    }

     public function encerra_homolog_ticket($dados_ajuste = null)
    {
        if($dados_ajuste != null) {
            
            $this->db
                    ->where('tid', $dados_ajuste['tid'])
                    ->where('uid', $this->session->userdata('uid'))
                    ->update('timesheet_homolog', $dados_ajuste);

            return true;

        } else {

            return false;
        }
    }

    public function get_timesheet_homolog($tid = null, $tipo = 0)
    {
        $sql = "SELECT * 
                FROM timesheet_homolog
                WHERE tid = $tid
                AND tipo = $tipo
                ";

        $result = $this->db->query($sql)->result();

       

        return $result;
    }

    public function get_timesheet_homolog_user($uid = null,$tid = null, $tipo = 0)
    {
        $sql = "SELECT * 
                FROM timesheet_homolog
                WHERE tid = $tid
                AND tipo = $tipo
                AND uid = $uid
                AND finished is null
                ";

        $result = $this->db->query($sql)->result();

       

        return $result;
    }
    public function get_user_email($uid = null)
    {
        if($uid){
            $sql = "SELECT users.email 
            FROM users
            WHERE uid = $uid";

            $result = $this->db->query($sql)->result();
            return $result;
        }else
        return false;
    }

    public function calcula_horas_timesheet_homolog($tid)
    {
        $timesheet = $this->get_timesheet_homolog($tid);
            if($dados->finished == null){
                $dados->finished = date('Y-m-d H:i:s');
            }
            $sql = "
                SELECT SUM(TIMESTAMPDIFF(hour,started,finished)) 
                FROM timesheet_homolog
                WHERE tid = $tid
                ";

        $result = $this->db->query($sql)->result();
        
        return $result;
    }

    public function horas_homolog_op($tid = null, $tipo = 0)
    {
        $timesheet = $this->get_timesheet_homolog($tid, $tipo);

       

        $inicio_expediente  = '08:00:00';

        $total_horas = 0;

        if ($timesheet) {

            //echo '<pre>';

            foreach ($timesheet as $key => $value) {
               

                $dia_inicio       = date('Y-m-d', strtotime($value->started));
                $dia_fim          = ($value->finished != null) ? date('Y-m-d', strtotime($value->finished)) : date('Y-m-d');
                $final_expediente = return_final_expediente($dia_fim);

                $dth_inicio = $value->started;
                $dth_fim    = ($value->finished != null) ? $value->finished : date('Y-m-d H:i:s');



                $dth_inicio_expediente  = $dia_inicio . ' 08:00:00';
                $dth_fim_expediente     = $dia_fim . ' ' . $final_expediente;

                $started    = new DateTime($value->started);
                $finished   = new DateTime($value->finished);

                // Caso a data inicio for igual a final, iremos pegar a diferença em horas
                if ($dia_inicio == $dia_fim) {

                    $final_expediente = return_final_expediente($dia_fim);

                    // Verificando se a data final é maior que o final do expediente
                    if (strtotime($dth_fim) > strtotime($dth_fim_expediente)) {
                        // caso for maior, iremos pegar o horário final do expediente
                        $dth_fim = $dth_fim_expediente;
                    }

                    $horas = return_diff($dth_inicio, $dth_fim, 'h');

                    $total_horas += $horas;

                    /*echo 'igual<br>';
                    echo 'inicio - ' . $dia_inicio . '<br>fim - ' . $dia_fim . '<br>';
                    print_r($value);
                    echo 'diferença horas - ' . $total_horas . '<br><hr>';*/

                } else {

                    $array_datas    = array();
                    $dias_intervalo = array();

                   

                    /*
                    * Pode acontecer o started pode ser maior que o inicio do expediente, e o finished ser menor que o final do expediente
                    * portanto, precisamos subtrair essas diferenças de horas para retornar um valor correto
                    */
                    $diff_horas_inicio  = return_diff($dth_inicio_expediente, $dth_inicio, 'h');
                    $diff_horas_fim     = return_diff($dth_fim_expediente, $dth_fim, 'h');

                   
                    

                    $intervalo = return_intervalo_datas($dth_inicio, $dth_fim, 'P1D');

                    // Montando um array com todos os dias do intervalo
                    foreach ($intervalo as $data) {

                        $dias_intervalo[] = $data->format("Y-m-d");
                    }

                    // Verificando se a data final está no intervalo, pois dependendo do horário ele não entra na regra de diferença por dias
                    if (!in_array($dia_fim, $dias_intervalo)) {

                        $dias_intervalo[] = $dia_fim;
                    }

                    // Laço para definir horas de cada dia, para somar a quantidade total
                    foreach ($dias_intervalo as $dia) {

                        $feriado = false;
                        $dia_num = date('N', strtotime($dia));


                        // Verificando a quantidade de horas de acordo com o dia da semana
                        if ($dia_num == 6 || $dia_num == 7) {

                            $horas  = 0;

                        } elseif ($dia_num == 5) {

                            $horas = 9;

                        } else {

                            $horas = 10;
                        }

                        // Caso for feriado, iremos atribuir o valor 0 em horas
                        if(verifica_data_feriado($dia)==true) {

                            $feriado  = true;
                            $horas    = 0;
                        }

                        // Caso o dia for diferente do atual, iremos apenar somar as horas no total
                        if ($dia != $dia_fim) {

                            $total_horas += $horas;



                        } else {
                           

                            /*
                            * caso contrário, iremos pegar a diferença entre o inciio do expediente e o horário atual
                            * desde que o mesmo for menor que o final do expediente
                            */
                            $dia_atual_inicio   = date('Y-m-d') . ' ' . $inicio_expediente;
                            $dia_atual_inicio2  = new DateTime($dia_atual_inicio);

                            $final_expediente   = return_final_expediente(date('Y-m-d'));
                            $dia_atual_fim      = date('Y-m-d') . ' ' . $final_expediente;
                            $dia_atual_fim2     = new DateTime($dia_atual_fim);

                            $dia_hora_atual     = date('Y-m-d H:i:s');
                            $dia_hora_atual2    = new DateTime($dia_hora_atual);

                            // Verificando se o horário atual é maior que o final do expediente
                            if (strtotime($dia_hora_atual) > strtotime($dia_atual_fim)) {

                                // caso for maior, iremos somar normalmente de acordo com as horas definiras acima
                                $total_horas += $horas;


                            } else {
                                

                                //$diferenca_dia_atual  = $dia_atual_inicio2->diff($dia_hora_atual2);
                                


                                //$horas = $diferenca_dia_atual->h;

                                $total_horas += $horas;


                            }
                        }

                        /*echo 'dia - ' . $dia . '<br>';
                        echo 'dia semana - ' . $dia_num . '<br>';
                        echo 'horas - '.$horas.'<br>';*/
                    }
                    
                    $total_diferenca_horas = $diff_horas_inicio + $diff_horas_fim;

                    


                    $total_horas -= $diff_horas_inicio;
                    $total_horas -= $diff_horas_fim;

                    //echo '<hr>';
                }

                //echo 'inicio - ' . $dia_inicio . '<br>' . 'fim = ' . $dia_fim;die;
            }

            //echo '</pre>';
        }

        return $total_horas;
    }

     public function min_homolog_op($tid = null, $tipo = 0)
    {
        $timesheet = $this->get_timesheet_homolog($tid, $tipo);

        $inicio_expediente  = '08:00:00';

        $total_horas = 0;

        if ($timesheet) {

            //echo '<pre>';

            foreach ($timesheet as $key => $value) {

                $dia_inicio       = date('Y-m-d', strtotime($value->started));
                $dia_fim          = ($value->finished != null) ? date('Y-m-d', strtotime($value->finished)) : date('Y-m-d');
                $final_expediente = return_final_expediente($dia_fim);

                $dth_inicio = $value->started;
                $dth_fim    = ($value->finished != null) ? $value->finished : date('Y-m-d H:i:s');

                $dth_inicio_expediente  = $dia_inicio . ' 08:00:00';
                $dth_fim_expediente     = $dia_fim . ' ' . $final_expediente;

                $started    = new DateTime($value->started);
                $finished   = new DateTime($value->finished);

                // Caso a data inicio for igual a final, iremos pegar a diferença em horas
                if ($dia_inicio == $dia_fim) {

                    $final_expediente = return_final_expediente($dia_fim);

                    // Verificando se a data final é maior que o final do expediente
                    if (strtotime($dth_fim) > strtotime($dth_fim_expediente)) {
                        // caso for maior, iremos pegar o horário final do expediente
                        $dth_fim = $dth_fim_expediente;
                    }

                    $horas = return_diff($dth_inicio, $dth_fim, 'i');

                    $total_horas += $horas;

                    /*echo 'igual<br>';
                    echo 'inicio - ' . $dia_inicio . '<br>fim - ' . $dia_fim . '<br>';
                    print_r($value);
                    echo 'diferença horas - ' . $total_horas . '<br><hr>';*/

                } else {

                    $array_datas    = array();
                    $dias_intervalo = array();

                    /*echo 'diferente<br>';
                    echo 'inicio - ' . $dth_inicio . '<br>fim - ' . $dth_fim . '<br><br>';
                    echo 'Final expediente - ' . $final_expediente . '<br>';
                    echo 'dth_inicio_expediente - ' . $dth_inicio_expediente . '<br>';
                    echo 'dth_fim_expediente - ' . $dth_fim_expediente . '<br><br>';*/

                    /*
                    * Pode acontecer o started pode ser maior que o inicio do expediente, e o finished ser menor que o final do expediente
                    * portanto, precisamos subtrair essas diferenças de horas para retornar um valor correto
                    */
                    $diff_horas_inicio  = return_diff($dth_inicio_expediente, $dth_inicio, 'i');
                    $diff_horas_fim     = return_diff($dth_fim_expediente, $dth_fim, 'i');

                    $intervalo = return_intervalo_datas($dth_inicio, $dth_fim, 'P1D');

                    // Montando um array com todos os dias do intervalo
                    foreach ($intervalo as $data) {

                        $dias_intervalo[] = $data->format("Y-m-d");
                    }

                    // Verificando se a data final está no intervalo, pois dependendo do horário ele não entra na regra de diferença por dias
                    if (!in_array($dia_fim, $dias_intervalo)) {

                        $dias_intervalo[] = $dia_fim;
                    }

                    // Laço para definir horas de cada dia, para somar a quantidade total
                    foreach ($dias_intervalo as $dia) {

                        $feriado = false;
                        $dia_num = date('N', strtotime($dia));

                        // Verificando a quantidade de horas de acordo com o dia da semana
                        if ($dia_num == 6 || $dia_num == 7) {

                            $horas  = 0;

                        } elseif ($dia_num == 5) {

                            $horas = 480;

                        } else {

                            $horas = 540;
                        }

                        // Caso for feriado, iremos atribuir o valor 0 em horas
                        if(verifica_data_feriado($dia)==true) {

                            $feriado  = true;
                            $horas    = 0;
                        }

                        // Caso o dia for diferente do atual, iremos apenar somar as horas no total
                        if ($dia != $dia_fim) {

                            $total_horas += $horas;

                        } else {
                            /*
                            * caso contrário, iremos pegar a diferença entre o inciio do expediente e o horário atual
                            * desde que o mesmo for menor que o final do expediente
                            */
                            $dia_atual_inicio   = date('Y-m-d') . ' ' . $inicio_expediente;
                            $dia_atual_inicio2  = new DateTime($dia_atual_inicio);

                            $final_expediente   = return_final_expediente(date('Y-m-d'));
                            $dia_atual_fim      = date('Y-m-d') . ' ' . $final_expediente;
                            $dia_atual_fim2     = new DateTime($dia_atual_fim);

                            $dia_hora_atual     = date('Y-m-d H:i:s');
                            $dia_hora_atual2    = new DateTime($dia_hora_atual);

                            // Verificando se o horário atual é maior que o final do expediente
                            if (strtotime($dia_hora_atual) > strtotime($dia_atual_fim)) {
                                // caso for maior, iremos somar normalmente de acordo com as horas definiras acima
                                $total_horas += $horas;

                            } else {

                                $diferenca_dia_atual  = $dia_atual_inicio2->diff($dia_hora_atual2);

                                $horas = $diferenca_dia_atual->i;

                                $total_horas += $horas;
                            }
                        }

                        /*echo 'dia - ' . $dia . '<br>';
                        echo 'dia semana - ' . $dia_num . '<br>';
                        echo 'horas - '.$horas.'<br>';*/
                    }

                    $total_horas -= $diff_horas_inicio;
                    $total_horas -= $diff_horas_fim;

                    //echo '<hr>';
                }

                //echo 'inicio - ' . $dia_inicio . '<br>' . 'fim = ' . $dia_fim;die;
            }

            //echo '</pre>';
        }

        return $total_horas;
    }


}
