<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Editar Categoria</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post">
		<input type="hidden" name="cid" value="<?php echo $category->cid ?>">
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('name')) echo 'has-danger' ?>">
		        <label for="">Nome da categoria</label>
		        <input type="text" class="form-control" id="name" name="name" placeholder="Nome da categoria" value="<?php echo $category->name ?>">
		        <?php echo form_error('name') ?>
		    </div>
		</div>
        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('visivel')) echo 'has-danger' ?>">
                <label>Projeto visível</label>
                <div>
                    <div class="form-check form-check-inline">
                        <input type="radio" id=""  name="visivel_geral" value="0" checked>Não
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" id=""  name="visivel_geral" value="1" <?= $category->visivel_geral == '1' ? 'checked' : '' ?>>Sim
                    </div>
                </div>
                <?php echo form_error('visivel') ?>
            </div>
        </div>
        <br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?php echo base_url() ?>category/all/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>
