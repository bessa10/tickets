<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Categorias</h1>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nome categoria</th>
				<th>Ações</th>
                <th>Visível</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $categories as $category ): ?>
				<tr>
					<td><?php echo $category->name ?></td>
					<td>
						<?php if($this->Roles_Model->has_permission('category', 2)): ?>
							<form method="post" action="<?php echo base_url() ?>/category/remove/<?php echo $category->cid ?>">
								<a href="<?php echo base_url() ?>category/edit/<?php echo $category->cid ?>"><button type="button" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir a categoria?')"><i class="fa fa-times" aria-hidden="true"></i></button>
							</form>
						<?php endif ?>
					</td>
                    <td><?= ($category->visivel_geral == '1' ? 'Sim' : 'Não') ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br><a href="<?php echo base_url() ?>category/create/"><button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nova categoria</button></a>
</div>

<?php $this->load->view('footer') ?>
