<?php
	$nome_arquivo = 'exportacao_cronograma_'.date('Y-m-d').'.xls';

	header("Content-type: application/vnd.ms-excel");
	header("Content-type: application/force-download");
	header("Content-Disposition: attachment; filename=$nome_arquivo");
	header("Expires: 0");
	header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Exportar Cronograma</title>
</head>
<body>
	<style type="text/css">
	  table {
	     width:100%;
	     white-space: nowrap;
	  }
	</style>
	<table border="1" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC">
		<tr style="background-color:#1c5170; color:#FFF; text-align:center;">
			<th class="text-center"><strong>Ordem</strong></th>
			<th class="text-center"><strong><?= utf8_decode('Nº Ticket') ?></strong></th>
			<th>Solicitante</th>
			<th>Depto. Solicitante</th>
			<th>Torre</th>
			<th><?= utf8_decode('Título Ticket') ?></th>
			<th>Criado</th>
			<th>Sistema</th>
			<th>Fase atual</th>
			<th>inicio F.A</th>
			<th class="text-center">Horas previstas</th>
			<th class="text-center">Horas utilizadas</th>
			<th class="text-center">%</th>
			<th class="text-center">Prev. Soluc</th>
		</tr>
		<tbody>
		<?php 
		foreach($tickets as $ticket) {
			if($ticket['status'] != 'Operação Assistida') {
			
				$porc_horas = 0;

				if ($ticket['horas_previstas'] > 0) {
					$porc_horas = ($ticket['horas_utilizadas'] * 100) / $ticket['horas_previstas'];
				}
		?>
			<tr <?= ($ticket['atrasado'] == 1) ? 'style="background-color:#ffcdd2"' : '' ?>>
				<td class="text-center"><?= $ticket['ordem'] ?></td>
				<td class="text-center"><strong><?= $ticket['tid'] ?></strong></td>
				<td><?
										$partes = explode(' ', $ticket['name_author']);
										$primeiroNome = array_shift($partes);
										$ultimoNome = array_pop($partes);
										?>
										<?= utf8_decode($primeiroNome.' '.$ultimoNome) ?>
											
			    </td>
				<td><?= utf8_decode($ticket['dep_author']) ?></td>
				<td><?= utf8_decode(($ticket['torre'] != null) ? $ticket['torre']->torre : '')?></td>
				<td><?= utf8_decode($ticket['titulo_ticket']) ?></td>
				<td><?= formata_data($ticket['created']) ?></td>
				<td><?= utf8_decode($ticket['project']) ?></td>
				<td class="text-center"><?= utf8_decode($ticket['status']) ?></td>
				<td class="text-center"><?= ($ticket['iniciado_em'] != utf8_decode('Não iniciado'))?formata_data($ticket['iniciado_em']): utf8_decode('Não iniciado') ?></td>
				<td class="text-center"><?= $ticket['horas_previstas'] ?></td>
				<td class="text-center"><?= ($ticket['horas_utilizadas'] > 0) ? $ticket['horas_utilizadas'] : 0 ?></td>
				<td class="text-center"><?= formata_porc($porc_horas).'%' ?></td>
				<td class="text-center"><?= ($ticket['data_prevista']!=null)?formata_data($ticket['data_prevista']):'' ?></td>
			</tr>
			<?php }  }?>
		</tbody>
	</table>
</body>
</html>