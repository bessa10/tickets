<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Status</h1>

	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Título</th>
				<th>Descrição</th>
				<th>Ordem</th>
				<th class="text-center">Editar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $statuses as $status ): ?>
				<tr id="<?php echo $status->sid ?>">
					<td><?php echo $status->label ?></td>
					<td><?php echo $status->description ?></td>
					<td><?php echo $status->place ?></td>
					<td>
						<?php if($this->Roles_Model->has_permission('status', 2)): ?>
							<form method="post" action="<?php echo base_url() ?>/status/remove/<?php echo $status->sid ?>">
								<a href="<?php echo base_url() ?>status/edit/<?php echo $status->sid ?>"><button type="button" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
								<!--
								<button type="submit" class="btn btn-link" onclick="return confirm('Are you sure you want to delete the status?')"><i class="fa fa-times" aria-hidden="true"></i></button>
								-->
							</form>
						<?php endif ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<br><a href="<?php echo base_url() ?>status/create/"><button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Novo status</button></a>
</div>
<?php $this->load->view('footer') ?>