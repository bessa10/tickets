<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Novo Status</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post">
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('label')) echo 'has-danger' ?>">
		        <label for="">Título</label>
		        <input type="text" class="form-control" id="label" name="label" placeholder="Título">
		    	<?php echo form_error('label') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('description')) echo 'has-danger' ?>">
		        <label for="">Descrição</label>
		        <input type="text" class="form-control" id="description" name="description" placeholder="Descrição">
		    	<?php echo form_error('description') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('place')) echo 'has-danger' ?>">
		        <label for="">Ordem</label>
		        <input type="text" class="form-control" id="place" name="place" placeholder="Ordem">
		    	<?php echo form_error('place') ?>
		    </div>
		</div><br>
		<div class="row">
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?php echo base_url() ?>status/all/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>