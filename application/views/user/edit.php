<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Atualizar Cadastro</h1>
	<!--
	<div class="alert alert-success text-center">
		<p><strong>Atualize o seu cadastro com o nome e e-mail para receber as notificações dos tickets.</strong></p>
	</div>
	-->
	<form method="post">
		<input type="hidden" name="uid" value="<?php echo $user->uid ?>">

		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Nome</label>
		        <input type="text" class="form-control" name="name" value="<?php echo $user->name ?>">
		    </div>
		</div>

		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Login</label>
		        <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $user->username ?>" readonly="true">
		    </div>
		</div>

		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">E-mail</label>
		        <input type="text" class="form-control" name="email" placeholder="E-mail address" value="<?php echo $user->email ?>">
		    </div>
		</div>
		<?php if(ENVIRONMENT=='development'): ?>
		<div class="row">
		    <div class="form-group col-lg-3">
		        <label for="">Senha</label>
			        <input type="password" class="form-control" name="password">
		    </div>
		    <div class="form-group col-lg-3">
		    	<label for="">Confirmar senha</label>
			    <input type="password" class="form-control" name="password2">
		    </div>
		</div>
		<?php endif ?>
		
	    <?php if($this->Roles_Model->has_permission('user', 2)): ?>
	    <div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Perfil de usuário</label>
		        <select class="form-control" name="role">
		        	<?php foreach($roles as $role): ?>
		        		<option value="<?php echo $role->rid ?>" <?php echo $role->rid == $user->role ? "selected" : ""?> ><?php echo $role->label ?></option>
		        	<?php endforeach ?>
		        </select>
		    </div>
		</div>
		 <div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Departamento do usuário</label>
		        <select class="form-control" name="id_department">
		        		<?php foreach($departments as $department): ?>
		        		<option value="<?php echo $department->id_department ?>" <?php echo $department->id_department == $user->id_department ? "selected" : ""?> ><?php echo $department->name_dep ?></option>
		        	<?php endforeach ?>
		        </select>
		    </div>
		</div>
		<?php endif ?>
		<br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?php echo base_url()?>user/all/"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>