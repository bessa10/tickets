<?php $this->load->view('header') ?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-6 offset-lg-3 div-login">
			<center>
				<img src="<?= base_url().'images/logo.png' ?>">
			</center>
			<?php if($this->session->flashdata('error')): ?>
				<div class="alert alert-danger" role="alert" id="alert">
					<?= $this->session->flashdata('error') ?>
				</div>
			<?php endif ?>
			<form method="post">
				<div class="card">
		  			<div class="card-header text-center bg-primary"><h3>Help</h3></div>
		  			<div class="card-body">
		  				<div class="row">
			  				<div class="form-group col-lg-12 <?= (form_error('username'))?'has-danger':'' ?>">
			        			<label for="">Usuário</label>
			        			<input type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>">
			        			<?= form_error('username') ?>
			    			</div>
		    			</div>
		    			<div class="row">
		    				<div class="form-group col-lg-12 <?= (form_error('password'))?'has-danger':'' ?>">
		        				<label for="">Senha</label>
		        				<input type="password" class="form-control" name="password" value="<?php echo set_value('password') ?>">
		        				<?= form_error('password') ?>
		    				</div>
		    			</div>
		    			<div class="row">
		    				<div class="form-group col-lg-12 text-right">
		    					<a href="<?= base_url() ?>forgotpassword">Esqueceu a senha?</a>
		    				</div>
		    			</div>
		    			<div class="row">
		    				<div class="form-group col-lg-12">
		    					<br><button type="submit" class="btn btn-lg btn-primary btn-block">Entrar</button>
		    				</div>
		    			</div>
		  			</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('footer') ?>