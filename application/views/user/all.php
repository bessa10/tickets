<?php $this->load->view('header') ?>
<div class="row">
	<div class="col-lg-12">
		<form method="post">
			<div class="row">
				<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<input type="text" class="form-control" name="name" placeholder="Nome" value="<?=$filtro['name']?>">
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<input type="text" class="form-control" name="username" placeholder="Login" value="<?=$filtro['username']?>">
				</div>

				<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<?= selectDB('role',null,$roles,'label','rid',$filtro['role'],null,null,'Perfil de usuário') ?>
		    	</div>
            </div>
            
			<div class="row">
				<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button type="submit" class="btn btn-primary btn-load"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
				</div>
				<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right	">
					<a href="<?=base_url()?>user/all" class="btn btn-outline-secondary btn-load"><i class="fa fa-trash"></i>&nbsp;&nbsp;Limpar</a>
				</div>
			</div>
		</form>
	</div>
</div><hr>
<div class="row">
	<div class="col-sm-12">
		<h1>Usuários</h1>
		<table class="table table-striped table-bordered" id="tabela-users">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Login</th>
					<th>Perfil</th>
					<th>Departamento</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $users as $user ): ?>
					<tr>
						<td><?php echo $user->name ?></td>
						<td><?php echo $user->username ?></td>
						<td><?php echo $user->label ?></td>
						<td><?php echo $user->department ?></td>
						<?php if($this->Roles_Model->has_permission('user', 2)): ?>
						<td>
							<form method="post" style="display: flex;" action="<?php echo base_url() ?>user/remove/<?php echo $user->uid ?>">
								<a href="<?php echo base_url() ?>user/edit/<?php echo $user->uid ?>"><button type="button" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="submit"  onclick="return confirm('Tem certeza que deseja excluir o usuário?')" class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i></button>
							</form>
						</td>
						<?php endif ?>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<br><a href="<?php echo base_url() ?>user/create/"><button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Novo Usuário</button></a>
	</div>
</div>
<?php $this->load->view('footer') ?>
