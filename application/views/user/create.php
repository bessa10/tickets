<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Novo Usuário</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post">
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('name')) echo 'has-danger' ?>">
		        <label for="">Nome</label>
		        <input type="text" class="form-control" name="name" value="<?php echo set_value('name') ?>">
		    	<?php echo form_error('name') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('username')) echo 'has-danger' ?>">
		        <label for="">Login</label>
		        <input type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>">
		    	<?php echo form_error('username') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('email')) echo 'has-danger' ?>">
		        <label for="">E-mail</label>
		        <input type="email" class="form-control" name="email" value="<?php echo set_value('email') ?>">
		    	<?php echo form_error('email') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-4 <?php if(form_error('password')) echo 'has-danger' ?>">
		        <label for="">Senha</label>
		        <input type="password" class="form-control" name="password">
		    	<?php echo form_error('password') ?>
		    </div>
		    <div class="form-group col-lg-4 <?php if(form_error('password2')) echo 'has-danger' ?>">
		        <label for="">Confirmar Senha</label>
		        <input type="password" class="form-control" name="password2">
		    	<?php echo form_error('password2') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('role')) echo 'has-danger' ?>">
		        <label for="">Perfil</label>
		        <select class="form-control" name="role">
		        	<?php foreach($roles as $role): ?>
		        		<option value="<?php echo $role->rid ?>" <?php echo set_select('role', $role->rid) ?> ><?php echo $role->label ?></option>
		        	<?php endforeach ?>
		        </select>
		    	<?php echo form_error('role') ?>
		    </div>
		</div><br>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('department')) echo 'has-danger' ?>">
		        <label for="">Departamento</label>
		        <select class="form-control" name="id_department">
		        	<?php foreach($departments as $department): ?>
		        		<option value="<?php echo $department->id_department ?>" <?php echo set_select('departments', $department->id_department) ?> ><?php echo $department->name_dep ?></option>
		        	<?php endforeach ?>
		        </select>
		    	<?php echo form_error('department') ?>
		    </div>
		</div><br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?php echo base_url() ?>user/all/"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>
<?php $this->load->view('footer') ?>