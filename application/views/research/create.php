<?php $this->load->view('header') ?>
<div class="col-sm-12" style="height: 100vh">
    <h1>Pesquisa de satisfação</h1>

    <form method="post">

        <div class="form-group col-lg-12 <?php if(form_error('atendimento')) echo 'has-danger' ?>">
            <div>
                <input type="hidden"  name="atendimento" value="s">
            </div>
            <?php echo form_error('atendimento') ?>
        </div>
        <div class="form-group col-lg-12 <?php if(form_error('satisfacao')) echo 'has-danger' ?>">
            <label>De 0 a 5, onde 5 é totalmente satisfeito e 0 é insatisfeito, como você classifica sua percepção do atendimento feito para sua solicitação?</label>
            <div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="1"  name="satisfacao" value="0">0
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="2"  name="satisfacao" value="1">1
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="3"  name="satisfacao" value="2">2
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="4"  name="satisfacao" value="3">3
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="5"  name="satisfacao" value="4">4
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" id="7" name="satisfacao" value="5">5
                </div>
            </div>
            <?php echo form_error('satisfacao') ?>
        </div>
        <div class="form-group col-lg-12 ajudar-form" style="display: none;">
            <label for="exampleFormControlTextarea1">Como podemos melhorar?</label>
            <textarea class="form-control" name="sugestao" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
        </div>
    </form>
    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pesquisa de satisfação</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Antes de sair, podemos considerar que sua nota é 5?</h2>
                    <form class="" id="" method="post">
                        <input type="hidden" name="atendimento" value="s">
                        <p>
                            <input type="hidden" name="satisfacao" value="5">
                        </p>
                        <p class="button">
                            <button type="submit" class="btn btn-success btn-lg">Nota 5</button>
                            <button type="button" data-dismiss="modal" id="6" class="btn btn-lg btn-danger">Sair</button>
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("body").one('mouseleave' , function (){
                if($.cookie("dialogDisplayed") !==1){
                    $("#form-modal").modal('show');
                    $.cookie("dialogDisplayed", '1', { expires: 30 });
                }
            });
            $(".close").click(function (){
                $("#form-modal").modal('hide');
            });
            $("#1").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#2").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#3").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#4").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#5").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#6").click(function() {
                $(".ajudar-form").css("display", "block");
            });
            $("#7").click(function() {
                $(".ajudar-form").css("display", "none");
            });
        })
    </script>
    <script>
        var modal = document.getElementById("myModal");
        function myFunction() {
            modal.onclick = function() {
                modal.style.display = "block";
            }
            modal.onclick = function() {
                modal.style.display = "block";
            }
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
    </script>


    <?php $this->load->view('footer') ?>
