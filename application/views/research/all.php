<?php $this->load->view('header') ?>

<div class="col-sm-12 content">
    <h1>Todos os Formulários de satisfação</h1>
    <!--<div class="row">
            <div class="col-lg-12">
                <form method="post">
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="tid" placeholder="Nº ticket" value="<?=$filtro['tid']?>">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="title" placeholder="Título" value="<?=$filtro['title']?>">
                        </div>
                        <?php if($this->session->userdata('role')==1): ?>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <?= selectDB('author',null,$users,'name','uid',$filtro['author'],null,null,'Autor') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-primary" id="basic-addon1">DE</span>
                                </div>
                                <input type="date" class="form-control" name="data1" value="<?=$filtro['data1']?>" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-primary" id="basic-addon1">ATÉ</span>
                                </div>
                                <input type="date" class="form-control" name="data2" value="<?=$filtro['data2']?>" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
                            <button type="submit" class="btn btn-block btn-primary btn-load"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
                            <a href="<?=base_url()?>" class="btn btn-block btn-outline-secondary btn-load"><i class="fa fa-trash"></i>&nbsp;&nbsp;Limpar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        -->
    <hr>
    <?php if($forms): ?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th class="text-center"><strong>TID</strong></th>
                <th>Ticket</th>
                <th>Autor</th>
                <th class="text-center">Sua solicitação foi integralmente atendida(S/N)</th>
                <th class="text-center">Como você classifica sua percepção do atendimento feito para sua solicitação? 0 a 5</th>
                <th class="text-center">Como podemos melhorar?(Nota abaixo de 5)</th>
                <th class="text-center">Data de preenchimento</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach( $forms as $form ): ?>
                <tr>
                    <td><?= $form->ticket_id ?></td>
                    <td><a href="<?= base_url() ?>ticket/view/<?= $form->ticket_id ?>"><?= $form->title ?></a></td>
                    <td><?= $form->name ?></td>
                    <td class="text-center"><?= ($form->atendimento == 's' ? 'Sim' : 'Não') ?></td>
                    <td class="text-center"><?= $form->satisfacao ?></td>
                    <td class="text-center"><?= $form->sugestao ?></td>
                    <td class="text-center"><?= date('d/m/Y', strtotime($form->date)) ?></td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
        <?php $this->load->view('ticket/pagination') ?>
    <?php else: ?>
        <p>Nenhum formulário foi encontrado...</p>
    <?php endif ?>
</div>
<?php $this->load->view('footer') ?>
