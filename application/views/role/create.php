<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Novo perfil</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?= $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post">
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('label')) echo 'has-danger' ?>">
		        <label for="">Título do perfil</label>
		        <input type="text" class="form-control" id="label" name="label" placeholder="Título do perfil">
		    	<?= form_error('label') ?>
		    </div>
		</div>

	    <legend>Permissões</legend>
	    <div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('permission_ticket')) echo 'has-danger' ?>">
		        <label for="">Tickets</label>
		        <select class="form-control" name="permission_ticket">
		        	<option value="5">Tudo</option>
		        	<option value="4">Editar todos os tickets</option>
		        	<option value="3">Criar e editar os próprios tickets</option>
		        	<option value="2">Alterar status e comentar</option>
		        	<option value="1">Visualizar</option>
		        	<option value="0">Sem permissão</option>
		        </select>
		    	<?= form_error('permission_ticket') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('permission_category')) echo 'has-danger' ?>">
		        <label for="">Categoria</label>
		        <select class="form-control" name="permission_category">
		        	<option value="3">Tudo</option>
		        	<option value="2">Criar e editar</option>
		        	<option value="1">Visualizar</option>
		        	<option value="0">Sem permissão</option>
		        </select>
		    	<?= form_error('permission_category') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('permission_status')) echo 'has-danger' ?>">
		        <label for="">Status</label>
		        <select class="form-control" name="permission_status">
		        	<option value="3">Tudo</option>
		        	<option value="2">Criar e editar</option>
		        	<option value="1">Visualizar</option>
		        	<option value="0">Sem permissão</option>
		        </select>
		    	<?= form_error('permission_status') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('permission_user')) echo 'has-danger' ?>">
		        <label for="">Usuários</label>
		        <select class="form-control" name="permission_user">
		        	<option value="3">Tudo</option>
		        	<option value="2">Criar e editar</option>
		        	<option value="1">Visualizar</option>
		        	<option value="0">Sem permissão</option>
		        </select>
		    	<?= form_error('permission_user') ?>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('permission_role')) echo 'has-danger' ?>">
		        <label for="">Perfis de usuário</label>
		        <select class="form-control" name="permission_role">
		        	<option value="3">Tudo</option>
		        	<option value="2">Criar e editar</option>
		        	<option value="1">Visualizar</option>
		        	<option value="0">Sem permissão</option>
		        </select>
		    	<?= form_error('permission_role') ?>
		    </div>
		</div><br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?= base_url() ?>role/all/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>