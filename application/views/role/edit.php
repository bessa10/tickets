<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Editar Perfil</h1>

	<form method="post">
		<input type="hidden" name="rid" value="<?= $role->rid ?>">
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Título do perfil</label>
		        <input type="text" class="form-control" id="label" name="label" value="<?= $role->label ?>">
		    </div>
		</div>

	    <legend>Permissões</legend>
	    <div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Ticket</label>
		        <select class="form-control" name="permission_ticket">
		        	<option value="5" <?= $role->permission_ticket == 5 ? 'selected' : ''?>>Tudo</option>
		        	<option value="4" <?= $role->permission_ticket == 4 ? 'selected' : ''?>>Editar todos os tickets</option>
		        	<option value="3" <?= $role->permission_ticket == 3 ? 'selected' : ''?>>Criar e editar os próprios tickets</option>
		        	<option value="2" <?= $role->permission_ticket == 2 ? 'selected' : ''?>>Alterar status e comentar</option>
		        	<option value="1" <?= $role->permission_ticket == 1 ? 'selected' : ''?>>Visualizar</option>

		        	<option value="0" <?= $role->permission_ticket == 0 ? 'selected' : ''?>>Sem permissão</option>
		        </select>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Categoria</label>
		        <select class="form-control" name="permission_category">
		        	<option value="3" <?= $role->permission_category == 3 ? 'selected' : ''?>>Tudo</option>
		        	<option value="2" <?= $role->permission_category == 2 ? 'selected' : ''?>>Criar e editar</option>
		        	<option value="1" <?= $role->permission_category == 1 ? 'selected' : ''?>>Visualizar</option>
		        	<option value="0" <?= $role->permission_category == 0 ? 'selected' : ''?>>Sem permissão</option>
		        </select>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Status</label>
		        <select class="form-control" name="permission_status">
		        	<option value="3" <?= $role->permission_status == 3 ? 'selected' : ''?>>Tudo</option>
		        	<option value="2" <?= $role->permission_status == 2 ? 'selected' : ''?>>Criar e editar</option>
		        	<option value="1" <?= $role->permission_status == 1 ? 'selected' : ''?>>Visualizar</option>
		        	<option value="0" <?= $role->permission_status == 0 ? 'selected' : ''?>>Sem permissão</option>
		        </select>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Users</label>
		        <select class="form-control" name="permission_user">
		        	<option value="3" <?= $role->permission_user == 3 ? 'selected' : ''?>>Tudo</option>
		        	<option value="2" <?= $role->permission_user == 2 ? 'selected' : ''?>>Criar e editar</option>
		        	<option value="1" <?= $role->permission_user == 1 ? 'selected' : ''?>>Visualizar</option>
		        	<option value="0" <?= $role->permission_user == 0 ? 'selected' : ''?>>Sem permissão</option>
		        </select>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Roles</label>
		        <select class="form-control" name="permission_role">
		        	<option value="3" <?= $role->permission_role == 3 ? 'selected' : ''?>>Tudo</option>
		        	<option value="2" <?= $role->permission_role == 2 ? 'selected' : ''?>>Criar e editar</option>
		        	<option value="1" <?= $role->permission_role == 1 ? 'selected' : ''?>>Visualizar</option>
		        	<option value="0" <?= $role->permission_role == 0 ? 'selected' : ''?>>Sem permissão</option>
		        </select>
		    </div>
		</div><br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?= base_url() ?>role/all/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>