<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Perfis de usuário</h1>

	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nome do perfil</th>
				<th>Editar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $roles as $role ): ?>
				<tr>
					<td><?php echo $role->label ?></td>

					<?php if($this->Roles_Model->has_permission('role', 2)): ?>
					<td>
						<form method="post" action="<?php echo base_url() ?>/role/remove/<?php echo $role->rid ?>">
							<a href="<?php echo base_url() ?>role/edit/<?php echo $role->rid ?>"><button type="button" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>&nbsp;&nbsp;&nbsp;&nbsp;
							<!--<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i></button>-->
						</form>
					</td>
					<?php endif ?>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br><a href="<?php echo base_url() ?>role/create/"><button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Novo Perfil</button></a>
</div>
<?php $this->load->view('footer') ?>