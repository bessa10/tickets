<link rel="stylesheet" href="<?= base_url() ?>vendor/bootstrap-4.5.2/css/bootstrap.min.css">
<div class="col-sm-12" style="font-size:24px;">
    <?php if($type == 'new'): ?>Um novo ticket foi criado<?php endif ?>
    <?php if($type == 'status'): ?>O ticket teve uma mudança no status<?php endif ?>
    <?php if($type == 'comment'): ?>O seu ticket acabou de receber um comentário<?php endif ?>
    <?php if($type == 'status_comment'): ?>Este ticket mudou de status e recebeu um comentário<?php endif ?>
</div>
<br>
<div class="col-sm-12">
    <h3>Ticket Nº: <?= $ticket->tid .' - '. $ticket->title?></h3><br>
    <h2>Mudanças recentes</h2>
    <table style="line-height:30px;">
        <tr>
            <td align="left">
                <?= date('d/m/y h:iA', strtotime($version->created)) ?> por <b><?= $version->username ?></b>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='status' || $type=='status_comment'): ?>
                    <span style="font-size:18px;"><b>Status:&nbsp;<span style="color:#00008B"><?= $ticket->status ?></span></b></span>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='comment' || $type=='status_comment'): ?>
                    <?php if(!empty($version->comment)): ?>
                        <span style="font-size:18px;color:#00008B;"><b><?= $version->comment ?></b></span>
                    <?php endif ?>
                <?php endif ?>
            </td>
        </tr>
    </table>
    <table style="line-height:30px;">
        <?php if($type == 'comment') { ?>
        <tr>
            <td align="left"><b>Atendente:</b>&nbsp;<?= $ticket->worker ? $ticket->worker : '-' ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td align="left"><b>Projeto:&nbsp;</b><?= $ticket->project ?></td>
        </tr>
        <tr>
            <td align="left"><b>Categoria:</b>&nbsp;<?= $ticket->category ?></td>
        </tr>
        <?php if($type == 'comment') { ?>
        <tr>
            <td align="left"><b>Status:&nbsp;</b><?= $ticket->status ?></td>
        </tr>
        <?php } ?>
        <?php
        $fechado = ($type == 'status') == 'finalizado';
        if($fechado == true){ ?>
            <tr>
                <td align="left">A solução aplicada lhe atendeu? <br>
                </td>
            </tr>
            <tr>
               <td align="left">
                    <table align="left" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td align="left" style="padding: 10px;">
                                <table border="0" class="mobile-button" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" bgcolor="#28a745" style="background-color: #28a745; margin: auto; max-width: 600px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding: 15px 20px; " width="100%">
                                            <!--[if mso]>&nbsp;<![endif]-->
                                            <a href="<?= URL_PESQUISA?>research/create/<?=$ticket->tid ?>" target="_blank" style="16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; text-align:center; background-color: #28a745; text-decoration: none; border: none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: inline-block;">
                                                <span style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; line-height:1.5em; text-align:center;">Sim</span>
                                            </a>
                                            <!--[if mso]>&nbsp;<![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" style="padding: 10px;">
                                <table border="0" class="mobile-button" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" bgcolor="#dc3545" style="background-color: #dc3545; margin: auto; max-width: 600px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding: 15px 20px; " width="100%">
                                            <!--[if mso]>&nbsp;<![endif]-->
                                            <a href="<?= URL_PESQUISA?>/ticket/comment/<?=$ticket->tid ?>" target="_blank" style="16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; text-align:center; background-color: #dc3545; text-decoration: none; border: none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: inline-block;">
                                                <span style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; line-height:1.5em; text-align:center;">Não</span>
                                            </a>
                                            <!--[if mso]>&nbsp;<![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php } ?>
    </table>
    <br>
    <p>
    Att,<br>
    <h4 style="color:#00008B">Equipe de Sistemas Cetrus</h4>
    </p>
</div>
