<link rel="stylesheet" href="<?= base_url() ?>vendor/bootstrap-4.5.2/css/bootstrap.min.css">
<div class="col-sm-12" style="font-size:24px;">
    <?php if($type == 'status_homolog'): ?>Este ticket se encontra em homologação<?php endif ?>
    <?php if($type == 'solicitacao_ajuste'): ?>O ticket necessita de mais ajustes<?php endif ?>
    <?php if($type == 'homolog_finalizada'): ?>A homologação foi aprovada.<?php endif ?>
</div>
<br>
<div class="col-sm-12">
    <h3>Ticket Nº: <?= $ticket->tid .' - '. $ticket->title?></h3><br>
    <h2>Mudanças recentes</h2>
    <table style="line-height:30px;">
        <tr>
            <td align="left">
                <?php if($type=='status_homolog'): ?>
                <p>O ticket aberto passou da etapa de desenvolvimento/ajustes para a etapa de homologação.<br><br>É importante que ao iniciar a homologação, acesse o Ticket em nosso sistema e registre através do botão "Iniciar homologação". Ao para de homologar, da mesma foma, clique no botão "Parar". Com isso, ficará registrado exatamente o tempo utilizado na Homologação.<br><br>
                Após homologar e caso o entregue esteja de acordo com o que esperava, favor clicar no botão de "Ticket Aprovado".<br><br>Caso necessite de novos ajustes ou percebeu que o que foi solicitado inicialmente precisa de pequenas alterações, clique no botão "Novos Ajustes" e preencha com o máximo de detalhes o que precisa ser alterado.<br><br>
                Agradecemos sua participação no processo de melhoria contínua.</p>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='solicitacao_ajuste'): ?>
                <p>O ticket aberto necessita de ajustes.</p>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='homolog_finalizada'): ?>
                <p>A homologação foi aprovada, por tanto o ticket agora entrará na etapa de operação assistida.</p>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='status' || $type=='status_comment'): ?>
                    <span style="font-size:18px;"><b>Status:&nbsp;<span style="color:#00008B"><?= $ticket->status ?></span></b></span>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td align="left">
                <?php if($type=='solicitacao_ajuste'): ?>
                    <?php if(!empty($version->comment)): ?>
                        <span style="font-size:18px;color:#00008B;"><b><?= $version->comment ?></b></span>
                    <?php endif ?>
                <?php endif ?>
            </td>
        </tr>
    </table>
    <table style="line-height:30px;">
        <?php if($type == 'comment') { ?>
        <tr>
            <td align="left"><b>Atendente:</b>&nbsp;<?= $ticket->worker ? $ticket->worker : '-' ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td align="left"><b>Projeto:&nbsp;</b><?= $ticket->project ?></td>
        </tr>
        <tr>
            <td align="left"><b>Categoria:</b>&nbsp;<?= $ticket->category ?></td>
        </tr>
        <?php if($type == 'comment') { ?>
        <tr>
            <td align="left"><b>Status:&nbsp;</b><?= $ticket->status ?></td>
        </tr>
        <?php } ?>
        <?php
        $fechado = ($type == 'status') == 'finalizado';
        if($fechado == true){ ?>
            <tr>
                <td align="left">A solução aplicada lhe atendeu? <br>
                </td>
            </tr>
            <tr>
               <td align="left">
                    <table align="left" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td align="left" style="padding: 10px;">
                                <table border="0" class="mobile-button" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" bgcolor="#28a745" style="background-color: #28a745; margin: auto; max-width: 600px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding: 15px 20px; " width="100%">
                                            <!--[if mso]>&nbsp;<![endif]-->
                                            <a href="<?= URL_PESQUISA?>research/create/<?=$ticket->tid ?>" target="_blank" style="16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; text-align:center; background-color: #28a745; text-decoration: none; border: none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: inline-block;">
                                                <span style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; line-height:1.5em; text-align:center;">Sim</span>
                                            </a>
                                            <!--[if mso]>&nbsp;<![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" style="padding: 10px;">
                                <table border="0" class="mobile-button" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" bgcolor="#dc3545" style="background-color: #dc3545; margin: auto; max-width: 600px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding: 15px 20px; " width="100%">
                                            <!--[if mso]>&nbsp;<![endif]-->
                                            <a href="<?= URL_PESQUISA?>/ticket/comment/<?=$ticket->tid ?>" target="_blank" style="16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; text-align:center; background-color: #dc3545; text-decoration: none; border: none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: inline-block;">
                                                <span style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-weight:normal; line-height:1.5em; text-align:center;">Não</span>
                                            </a>
                                            <!--[if mso]>&nbsp;<![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php } ?>
    </table>
    <br>
    <p>
    Att,<br>
    <h4 style="color:#00008B">Equipe de Sistemas Cetrus</h4>
    </p>
</div>
