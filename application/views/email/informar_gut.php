<link rel="stylesheet" href="<?= base_url() ?>vendor/bootstrap-4.5.2/css/bootstrap.min.css">
<?php
    $valor_gut = valor_gut($ticket->valor_gravidade,$ticket->valor_urgencia,$ticket->valor_tendencia);
    $txt_gut = texto_gut($valor_gut);
    $servico_gut = catalogo_servicos_gut($ticket->pid, $ticket->cid);
?>
<div class="col-lg-12">
	<p>Olá <strong><?= $ticket->author ?>!</strong><br><br>

	Recebemos a sua solicitação que foi classificada como <strong>GUT <?=$valor_gut?></strong> - <?= $txt_gut ?>.<br>
	(para entender melhor a classificação GUT, <a href="http://192.168.0.9/tickets_sistemas-pesquisa/gut" target="_blank" style="color:#00008B"><strong>clique aqui</strong></a>)<br><br>

	Caso nossa classificação GUT não atenda suas expecativas, por favor, acesse o chamado através do link <a href="<?=BASEURL.'ticket/view/'.$ticket->tid?>" target="_blank" style="color:#00008B"><strong><?=BASEURL.'ticket/view/'.$ticket->tid?></strong></a>  e acrescente informações adicionais para reclassificarmos.</p>

	<table class="table-gut" style="line-height:30px;">
        <tr>
            <td align="left">Nº Ticket:&nbsp;<strong><?= $ticket->tid ?></strong></td>
        </tr>
        <tr>
            <td align="left">Título do chamado:&nbsp;<strong><?= $ticket->title ?></strong></td>
        </tr>
        <tr>
            <td align="left">Projeto:&nbsp;<?= $ticket->project ?></td>
        </tr>
        <tr>
            <td align="left">Categoria:&nbsp;<?= $ticket->category ?></td>
        </tr>

        <?php if($valor_gut < 25): ?>
        <tr>
            <td align="left"><a href="http://192.168.0.9/tickets_sistemas-pesquisa/gut">Catálogo de serviços. </a></td>
        </tr>

    	<?php endif ?>
        <tr>
            <td align="left">Atendente:&nbsp;<strong><?= $ticket->worker?$ticket->worker:'-' ?></strong></td>
        </tr>
    </table>
    <br>
	<p>
	Att,<br>
	<h4 style="color:#00008B">Equipe de Sistemas Cetrus</h4>
	</p>
</div>
