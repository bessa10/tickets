<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<div class="card">
		<div class="card-body">
			<p style="font-size:16px;line-height:30px;"><i>Se precisar de uma mãozinha para uma ideia inovadora, clique no botão "Melhoria & Inovação" no topo dessa tela. Mas se precisar de ajuda para um erro, dúvida ou apoio operacional, nos conte mais logo abaixo.&nbsp;</i><img src="<?= base_url().'images/winking-face_1f609.png' ?>" style="width:30px;height:30px;">&nbsp;<img src="<?= base_url().'images/person-with-folded-hands_1f64f.png' ?>" class="img-emoji" style="width:30px;height:30px;"></p>
		</div>
	</div>


	<h1>Criar Novo Ticket</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?= $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post" enctype="multipart/form-data">
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Em qual problema, dúvida ou apoio operacional podemos ajudar?</label>
		        <input type="text" class="form-control <?= (form_error('title'))?'is-invalid':'' ?>" id="title" name="title" placeholder="" value="<?= set_value('title') ?>">
		        <div class="invalid-feedback">
		        	<?= form_error('title') ?>
		    	</div>
		    </div>
		</div>
		<div class="row">
		    <div class="form-group col-lg-12">
		        <label for="">Por favor, conte mais sobre o problema, dúvida ou apoio que necessita. Não esqueça de enviar prints se possível.</label>
		        <textarea class="form-control <?= (form_error('description'))?'is-invalid':'' ?>" id="description" name="description" rows="3"><?= set_value('description') ?></textarea>
			    <div class="invalid-feedback">
		        	<?= form_error('description') ?>
		    	</div>
		    </div>
		</div>
        <?php if($this->session->userdata('role')==1): ?>
		<div class="row">
		    <div class="form-group col-lg-12">
		    	<?= selectDB('project','Projeto',$projects,'name','pid',set_value('project'),null,'verifyCategory(this.value)',null,form_error('project')) ?>
		    </div>
		</div>
        <?php endif ?>
        <?php if($this->session->userdata('role')==2): ?>
            <div class="row">
                <div class="form-group col-lg-12">
                    <?= selectDB('project','Projeto',$projects_comum,'name','pid',set_value('project'),null,'verifyCategory(this.value)',null,form_error('project')) ?>
                </div>
            </div>
        <?php endif ?>
		<div class="row">
		    <div class="form-group col-lg-12">
		    	<div id="div-category"></div>


		    	<!--<?= selectDB('category','Categoria',$categories,'name','cid',set_value('category'),null,null,null,form_error('category')) ?>-->
		    </div>
		</div>

		<?php if($this->session->userdata('role')==1): ?>
		<div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('author','Autor do ticket',$users,'name','uid',set_value('author'),null,null,null,form_error('author')) ?>
            </div>
        </div><br>
    	<?php endif ?>
    	<?php
    		$dp_hp = (form_error('horas_previstas')) ? 'block' : 'block';
    	?>
    	<div id="div_torre" style="display:none;">
    		<div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('torre','Torre',$torre,'torre','id_torre',set_value('torre'),null,null,null,form_error('torre')) ?>
            </div>
        </div><br>
    	</div>
    	<div id="div_classificacao" style="display:none;">
    		<div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('classificacao','Classificação',$classificacao,'classificacao','id_classificacao',set_value('classificacao'),null,null,null,form_error('classificacao')) ?>
            </div>
        </div><br>
    	</div>
    	<div id="div-horas_previstas" style="display:<?= $dp_hp ?>;">
	    	<div class="row">
	    		<div class="col-lg-12 text-center">
	    			<h3>Horas previstas</h3><hr>
	    		</div>
	    		<div class="col-lg-2 text-center">
	    			<label for="">Análise da Demanda</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_analise'))?'is-invalid':'' ?>" id="previsao_analise" name="previsao_analise" placeholder="Previsto" value="<?= (set_value('previsao_analise')) ? set_value('previsao_analise') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_analise') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 text-center">
	    			<label for="">Estudo Técnico</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_estudo'))?'is-invalid':'' ?>" id="previsao_estudo" name="previsao_estudo" placeholder="Previsto" value="<?= (set_value('previsao_estudo')) ? set_value('previsao_estudo') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_estudo') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 text-center">
	    			<label for="">Desenvolvimento</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_dev'))?'is-invalid':'' ?>" id="previsao_dev" name="previsao_dev" placeholder="Previsto" value="<?= (set_value('previsao_dev')) ? set_value('previsao_dev') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_dev') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 text-center">
	    			<label for="">Homologação</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_homolog'))?'is-invalid':'' ?>" id="previsao_homolog" name="previsao_homolog" placeholder="Previsto" value="<?= (set_value('previsao_homolog')) ? set_value('previsao_homolog') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_homolog') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 text-center">
	    			<label for="">Ajustes</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_ajustes'))?'is-invalid':'' ?>" id="previsao_ajustes" name="previsao_ajustes" placeholder="Ajustes" value="<?= (set_value('previsao_ajustes')) ? set_value('previsao_ajustes') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_ajustes') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 text-center">
	    			<label for="">Operação assistida</label>
			        <input type="text" class="form-control text-center <?= (form_error('previsao_op_assistida'))?'is-invalid':'' ?>" id="previsao_op_assistida" name="previsao_op_assistida" placeholder="Previsto" value="<?= (set_value('previsao_op_assistida')) ? set_value('previsao_op_assistida') : 0 ?>" onblur="calculaHorasPrevistas()">
			        <div class="invalid-feedback">
			        	<?= form_error('previsao_op_assistida') ?>
			    	</div>
	    		</div>

	    		<div class="col-lg-2 mx-auto mt-4 text-center">
	    			<label for="">Total Previsto</label>
			        <input type="text" class="form-control text-center <?= (form_error('horas_previstas'))?'is-invalid':'' ?>" id="horas_previstas" name="horas_previstas" placeholder="Total previsto" value="<?= (set_value('horas_previstas')) ? set_value('horas_previstas') : 0 ?>" readonly="true">
			        <div class="invalid-feedback">
			        	<?= form_error('horas_previstas') ?>
			    	</div>
	    		</div>
	    	</div><br>
    	</div>
	    <!--
	    <div class="form-group col-lg-12 <?php if(form_error('cc')) echo 'has-danger' ?>">
	        <label for="">CC</label>
            <select name="cc[]" id="cc" multiple="multiple" class="form-control">
                <?php foreach($users as $user): ?>
                	<?php if($user->uid != $this->session->userdata('uid')): ?>
	                    <option value="<?= $user->uid ?>"><?= $user->username ?></option>
	                <?php endif ?>
                <?php endforeach ?>
            </select>
	        <?= form_error('cc') ?>
	    </div>
		-->
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('attachments')) echo 'has-danger' ?>">
		        <label for="attachments">Anexos</label>
				<input name="attachments[]" id="attachments" type="file" class="form-control" multiple="">
		        <?= form_error('attachments') ?>
				<small>Pode selecionar 1 arquivo ou mais</small>
			</div>
		</div><br>
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary btn-load"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar Ticket</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?= base_url() ?>"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		<?php if(set_value('project')) {
			$set_category = (set_value('category')) ? set_value('category') : 'null';
			$error_category = (form_error('category')) ? form_error('category') : null;
		?>
			verifyCategory(<?= set_value('project') ?>,<?= $set_category ?>,'<?=$error_category?>');
		<?php } ?>

	});
</script>

<?php $this->load->view('footer') ?>
