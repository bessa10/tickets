<?php $this->load->view('header') ?>

<div class="col-sm-12 content">
	<h1>Todos os Tickets</h1>

	<div class="row">
		<div class="col-lg-12">
			<form method="post">
				<div class="row">
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="tid" placeholder="Nº ticket" value="<?=$filtro['tid']?>">
					</div>
					<div class="form-group col-lg-6 col-md-4 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="title" placeholder="Título" value="<?=$filtro['title']?>">
					</div>
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<?= selectDB('project',null,$projects,'name','pid',$filtro['project'],null,null,'Projetos') ?>
					</div>
                </div>
                <div class="row">
					<?php if($this->session->userdata('role')==1): ?>
						<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<?= selectDB('author',null,$users,'name','uid',$filtro['author'],null,null,'Autor') ?>
						</div>
					<?php endif ?>
                    <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <?= selectDB('worker',null,$workers,'name','uid',$filtro['worker'],null,null,'Worker') ?>
                    </div>
				</div>
				<div class="row">
					<div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
						<div class="input-group">
		  					<div class="input-group-prepend">
		    					<span class="input-group-text bg-primary" id="basic-addon1">DE</span>
		  					</div>
		  					<input type="date" class="form-control" name="data1" value="<?=$filtro['data1']?>" aria-label="Username" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
						<div class="input-group">
		  					<div class="input-group-prepend">
		    					<span class="input-group-text bg-primary" id="basic-addon1">ATÉ</span>
		  					</div>
		  					<input type="date" class="form-control" name="data2" value="<?=$filtro['data2']?>" aria-label="Username" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-block btn-primary btn-load"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
						<a href="<?=base_url()?>" class="btn btn-block btn-outline-secondary btn-load"><i class="fa fa-trash"></i>&nbsp;&nbsp;Limpar</a>
					</div>
				</div>
			</form>
		</div>
	</div><hr>
	<?php if($tickets): ?>
		<table class="table table-striped table-bordered" id="tabela-atribuido">
			<thead>
				<tr>
					<th class="text-center"><strong>Ticket</strong></th>
					<th>Título</th>
					<th>Autor</th>
                    <th>Worker</th>
					<th class="text-center">Status</th>
					<th class="text-center">Criado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $tickets as $ticket ): ?>
					<tr>
						<td><?= $ticket->tid ?></td>
						<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
						<td><?= $ticket->autor ?></td>
                        <td><?= $ticket->worker ?></td>
						<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->label ?></span></td>
						<td class="text-center"><?= date('d/m/Y', strtotime($ticket->created)) ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	<?php else: ?>
		<p>Nenhum ticket foi encontrado...</p>
	<?php endif ?>
</div>
<?php $this->load->view('footer') ?>
