<?php $this->load->view('header') ?>
<br>
<div class="col-sm-12">
    <h1>Adicionar comentário no Ticket - <?= $ticket->tid ?></h1>

    <?php if($this->session->flashdata('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $this->session->flashdata('error') ?>
        </div>
    <?php endif ?>
    <div class="update">

<?php if($last_version->created >= date('d/m/Y', strtotime('+7 days', strtotime($last_version->created))) && $ticket->sid == 4 ):?>
            <form method="post">
                <input type="hidden" name="tid" id="tid" value="<?= $ticket->tid ?>">

                <!--
                <h2>Atualizar status</h2><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-check">
                            <fieldset>
                              <div class="switch-toggle alert alert-light">
                                    <?php foreach($statuses as $status): ?>
                                        <input id="<?= $status->sid ?>" name="status" value="<?= $status->sid ?>" type="radio" <?php if($ticket->sid == $status->sid) echo 'checked' ?> <?php if($ticket->sid > $status->sid || $ticket->sid == -1) echo 'disabled' ?>>
                                        <label for="<?= $status->sid ?>"><?= $status->label ?></label>
                                    <?php endforeach ?>
                                    <input id="-1" name="status" value="-1" type="radio" <?php if($ticket->sid == -1) echo 'checked' ?>>
                                    <label for="-1">Cancelado</label>
                                <a class="btn btn-primary"></a>
                              </div>
                            </fieldset>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="form-group col-lg-12">
                        <?= selectDB('author','Autor do ticket',$users,'name','uid',$ticket->author_uid,null,null) ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="form-group col-lg-12">
                        <?= selectDB('worker','Atribuir para',$workers,'name','uid',$ticket->worker_uid,null,null) ?>
                    </div>
                </div><br>
                -->
                <input type="hidden" value="<?= $ticket->author_uid ?>" name="author">
                <input type="hidden" value="<?= $ticket->sid ?>" name="status">
                <input type="hidden" value="<?= $ticket->worker_uid ?>" name="worker">
                <div class="form-group">
                    <label for="">Comentário</label>
                    <textarea class="form-control" id="comment" name="comment" rows="10"></textarea>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button type="submit" class="btn btn-primary btn-load">Salvar comentário</button>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                        <a href="<?= base_url() ?>"><button type="button" class="btn btn-secondary">Voltar</button></a>
                    </div>
                </div>
            </form>
            <br>
        <?php else: ?>
    <p>O prazo para reabertura do chamado foi expirado ou ele ainda se encontra em execução.Caso tenha expirado o prazo de 7 dias após o chamado ter sido encerrado, clique <a href="/ticket/create">aqui</a>  para abrir um novo chamado.</p><br>
        <?php endif ?>
    </div>
    <div class="changes">
        <h2>Alterações</h2>
        <table class="table">
            <?php foreach($versions as $version): ?>
                <tr>
                    <td>
                        <?= date('d/m/y h:iA', strtotime($version->created)) ?> por <strong><?= $version->username ?></strong>
                        <br>
                        <?php if($this->session->userdata('role')==1): ?>
                            <?php foreach(json_decode($version->difference) as $key=>$value): ?>
                                <b><?= ucfirst($key) ?></b> <i class="text-muted">mudou de</i> <?= $value->antes ? $value->antes : "Nada" ?> <i class="text-muted">para</i> <?= $value->depois ? $value->depois : "Nada" ?><br />
                            <?php endforeach ?>
                        <?php endif ?>
                        <br>
                        <?php if(!empty($version->comment)): ?>
                            <strong>Comentário;</strong><br><?= $version->comment ?>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
<?php $this->load->view('footer') ?>
