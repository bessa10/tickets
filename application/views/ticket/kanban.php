<?php $this->load->view('header') ?>

<div class="col-sm-12 content">
	<h1>Kanban</h1>
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active item-tabs-tickets" id="home-tab" data-toggle="tab" href="#chamados" role="tab" aria-controls="home" aria-selected="true">CHAMADOS&nbsp;&nbsp;<span class="badge badge-secondary"></span></a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link item-tabs-tickets" id="profile-tab" data-toggle="tab" href="#kanban" role="tab" aria-controls="profile" aria-selected="false">DESENVOLVIMENTO&nbsp;&nbsp;<span class="badge badge-secondary"></span></a>
                    </li>
            </ul>
            <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="chamados" role="tabpanel" aria-labelledby="home-tab">
	<div class="kanban-board" id="kanban-board">
		<?php foreach($tickets_chamados as $status => $tickets): ?>
			<div class="kanban-column" style="width: <?php echo 100 / count($tickets_chamados) - 1 ?>%" id="<?php echo $status ?>">
				<div class="status-header"><?php echo $status ?></div>
				<?php foreach( $tickets as $ticket ): ?>
					<div class="kanban-ticket" data-id="<?php echo $ticket->tid ?>">
						<div class="id"><?php echo $ticket->tid ?></div>
						<div class="title"><a href="<?php echo base_url() ?>ticket/view/<?php echo $ticket->tid ?>"><?php echo $ticket->title ?></a></div>
						<div class="author"><?php echo $ticket->username ?></div>
						<div class="date"><?php echo date('d/m/Y', strtotime($ticket->created)) ?></div>
					</div>
				<?php endforeach ?>
			</div>
		<?php endforeach ?>
	</div>
    </div>
    <div class="tab-pane fade show active" id="kanban" role="tabpanel" aria-labelledby="home-tab">
        <div class="kanban-board" id="kanban-board-dev">
            <?php foreach($tickets_desenvolvimento as $status => $tickets): ?>
                <div class="kanban-column" style="width: <?php echo 100 / count($tickets_chamados) - 1 ?>%" id="<?php echo $status ?>">
                    <div class="status-header"><?php echo $status ?></div>
                    <?php foreach( $tickets as $ticket ): ?>
                        <div class="kanban-ticket" data-id="<?php echo $ticket->tid ?>">
                            <div class="id"><?php echo $ticket->tid ?></div>
                            <div class="title"><a href="<?php echo base_url() ?>ticket/view/<?php echo $ticket->tid ?>"><?php echo $ticket->title ?></a></div>
                            <div class="author"><?php echo $ticket->username ?></div>
                            <div class="date"><?php echo date('d/m/Y', strtotime($ticket->created)) ?></div>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>

<?php $this->load->view('footer') ?>
