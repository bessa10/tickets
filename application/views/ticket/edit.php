<?php $this->load->view('header') ?>
<br>
<div class="col-sm-12">
    <h1>Editar Ticket - <?= $ticket->tid ?></h1>

    <?php if($this->session->flashdata('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $this->session->flashdata('error') ?>
        </div>
    <?php endif ?>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="tid" id="tid" value="<?= $ticket->tid ?>">
        <div class="row">
            <div class="form-group col-lg-12">
                <label>Em qual problema, dúvida ou apoio operacional podemos ajudar?</label>
                <input type="text" class="form-control <?= (form_error('title'))?'is-invalid':'' ?>" id="title" name="title" placeholder="" value="<?php echo $ticket->title ?>">
                <div class="invalid-feedback">
                    <?= form_error('title') ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-12">
                <label for="">Por favor, conte mais sobre o problema, dúvida ou apoio que necessita. Não esqueça de enviar prints se possível.</label>
                <textarea class="form-control <?= (form_error('description'))?'is-invalid':'' ?>" id="description" name="description" rows="3"><?php echo $ticket->description ?></textarea>
                <div class="invalid-feedback">
                    <?= form_error('description') ?>
                </div>
            </div>
        </div>
        <?php if($this->session->userdata('role') == 1): ?>
        <div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('project','Projeto',$projects,'name','pid',$ticket->pid,null,'verifyCategory(this.value)',null,form_error('project')) ?>
            </div>
        </div><br>
        <div class="row">
            <div class="form-group col-lg-12">
                <div id="div-category"></div>
            </div>
        </div><br>

        <div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('author','Autor do ticket',$users,'name','uid',$ticket->author_uid,null,null,null,form_error('author')) ?>
            </div>
        </div><br>

        <?php
            $dp_hp = ($ticket->cid == 28) ? 'block' : 'none';
        ?>
        <div id="div_torre" style="display:<?= $dp_hp ?>;">
            <div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('torre','Torre',$torre,'torre','id_torre',$ticket->torre,null,null,null,form_error('torre')) ?>
            </div>
        </div><br>
        </div>
        <div id="div_classificacao" style="display:<?= $dp_hp ?>;">
            <div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('classificacao','Classificação',$classificacao,'classificacao','id_classificacao',$ticket->classificacao,null,null,null,form_error('classificacao')) ?>
            </div>
        </div><br>
        </div>
        <div id="div-horas_previstas" style="display:<?= $dp_hp ?>;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>Horas previstas</h3><hr>
                </div>
                <div class="col-lg-2 text-center">
                    <label for="">Análise da Demanda</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_analise'))?'is-invalid':'' ?>" id="previsao_analise" name="previsao_analise" placeholder="Previsto" value="<?= $ticket->previsao_analise ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_analise') ?>
                    </div>
                </div>

                <div class="col-lg-2 text-center">
                    <label for="">Estudo Técnico</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_estudo'))?'is-invalid':'' ?>" id="previsao_estudo" name="previsao_estudo" placeholder="Previsto" value="<?= $ticket->previsao_estudo ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_estudo') ?>
                    </div>
                </div>

                <div class="col-lg-2 text-center">
                    <label for="">Desenvolvimento</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_dev'))?'is-invalid':'' ?>" id="previsao_dev" name="previsao_dev" placeholder="Previsto" value="<?= $ticket->previsao_dev ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_dev') ?>
                    </div>
                </div>

                <div class="col-lg-2 text-center">
                    <label for="">Homologação</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_homolog'))?'is-invalid':'' ?>" id="previsao_homolog" name="previsao_homolog" placeholder="Previsto" value="<?= $ticket->previsao_homolog ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_homolog') ?>
                    </div>
                </div>

                <div class="col-lg-2 text-center">
                    <label for="">Ajustes</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_ajustes'))?'is-invalid':'' ?>" id="previsao_ajustes" name="previsao_ajustes" placeholder="Previsto" value="<?= $ticket->previsao_ajustes ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_ajustes') ?>
                    </div>
                </div>

                <div class="col-lg-2 text-center">
                    <label for="">Operação assistida</label>
                    <input type="text" class="form-control text-center <?= (form_error('previsao_op_assistida'))?'is-invalid':'' ?>" id="previsao_op_assistida" name="previsao_op_assistida" placeholder="Previsto" value="<?= $ticket->previsao_op_assistida ?>" onblur="calculaHorasPrevistas()">
                    <div class="invalid-feedback">
                        <?= form_error('previsao_op_assistida') ?>
                    </div>
                </div>

                <div class="col-lg-2 mx-auto mt-4 text-center">
                    <label for="">Total Previsto</label>
                    <input type="text" class="form-control text-center <?= (form_error('horas_previstas'))?'is-invalid':'' ?>" id="horas_previstas" name="horas_previstas" placeholder="Total previsto" value="<?= $ticket->horas_previstas ?>" readonly="true">
                    <div class="invalid-feedback">
                        <?= form_error('horas_previstas') ?>
                    </div>
                </div>
            </div><br>
        </div>

        <div class="row">
            <div class="form-group col-lg-12">
                <?= selectDB('worker','Atribuir para',$workers,'name','uid',$ticket->worker_uid,null,null) ?>
            </div>
        </div><br>

        <div class="row">
            <div class="col-lg-12">
                <h2>Atualizar GUT</h2><br>
            </div>
            <div class="form-group col-lg-12">
                <?= selectDB('valor_gravidade','Gravidade',$gravidade,'label_gravidade','valor_gravidade',$ticket->valor_gravidade,null,null,null,form_error('valor_gravidade')) ?>
            </div><br>

            <div class="form-group col-lg-12">
                <?= selectDB('valor_urgencia','Urgência',$urgencia,'label_urgencia','valor_urgencia',$ticket->valor_urgencia,null,null,null,form_error('valor_urgencia')) ?>
            </div><br>

            <div class="form-group col-lg-12">
                <?= selectDB('valor_tendencia','Tendência',$tendencia,'label_tendencia','valor_tendencia',$ticket->valor_tendencia,null,null,null,form_error('valor_tendencia')) ?>
            </div><br>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Atualizar status</h2><br>
                <div class="form-check">
                    <fieldset>
                      <div class="switch-toggle alert alert-light">
                            <?php foreach($statuses as $status): ?>
                                <input id="<?= $status->sid ?>" name="status" value="<?= $status->sid ?>" type="radio" <?php if($ticket->sid == $status->sid) echo 'checked' ?> <?php if($ticket->place > $status->place || $ticket->sid == -1) echo 'disabled' ?>>
                                <label for="<?= $status->sid ?>"><?= $status->label ?></label>
                            <?php endforeach ?>
                            <input id="-1" name="status" value="-1" type="radio" <?php if($ticket->sid == -1) echo 'checked' ?>>
                            <label for="-1">Cancelado</label>
                        <a class="btn btn-primary"></a>
                      </div>
                    </fieldset>
                </div>
            </div>
        </div><br>
        <!--
        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('status')) echo 'has-danger' ?>">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    <?php foreach($statuses as $status): ?>
                        <option value="<?php echo $status->sid ?>" <?php if($ticket->sid == $status->sid) echo 'selected' ?>><?php echo $status->label ?></option>
                    <?php endforeach ?>
                </select>
                <?php echo form_error('status') ?>
            </div>
        </div>
        -->
        <?php else: ?>
            <input type="hidden" value="<?= $ticket->author_uid ?>" name="author">
            <input type="hidden" value="<?= $ticket->worker_uid ?>" name="worker">
            <input type="hidden" value="<?= $ticket->sid ?>" name="status">
        <?php endif ?>
        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('attachments')) echo 'has-danger' ?>">
                <label for="attachments">Adicionar Anexo</label>
                <input name="attachments[]" id="attachments" type="file" class="form-control" multiple="" />
                <?php echo form_error('attachments') ?>
            </div>
        </div><br>
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-primary btn-load"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar ticket</button>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                <a href="<?= base_url().'ticket/view/'.$ticket->tid ?>"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
            </div>
        </div><br>
    </form>

    <?php if($attachments): ?>
        <div class="row">
            <div class="form-group col-lg-12">
                <h2>Anexos do ticket</h2>
                <div class="row attachments">
                    <?php foreach($attachments as $attachment): ?>
                        <div class="col-md-2">
                            <a href="<?php echo base_url("attachment/view/{$attachment->aid}") ?>">
                                <?php if($attachment->is_image == 'Y'): ?>
                                    <div class="attachment-mini" style="background-image:url('<?php echo base_url("attachments/{$attachment->filename}") ?>')"></div>
                                <?php else: ?>
                                    <div class="attachment-mini"><i class="fa fa-file-o" aria-hidden="true"></i></div>
                                <?php endif ?>
                            </a>
                        </div>
                        <div class="col-md-10 attachment-info">
                            <div class="attachment-title"><a href="<?php echo base_url("attachment/view/{$attachment->aid}") ?>" target="_blank"><?php echo $attachment->title ?></a></div>
                            <form method="post" action="<?php echo base_url() ?>/attachment/remove/<?php echo $attachment->tid ?>/<?php echo $attachment->aid ?>">
                                <button type="submit" class="btn-sm btn-outline-danger" onclick="return confirm('Are you sure you want to delete this attachment?')"><i class="fa fa-times" aria-hidden="true"></i> Remove</button>
                            </form>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        <?php if($ticket->pid) {
            $set_category = ($ticket->cid) ? $ticket->cid : 'null';
            $error_category = (form_error('category')) ? form_error('category') : null;
        ?>
            verifyCategory(<?= $ticket->pid ?>,<?= $set_category ?>,'<?=$error_category?>');
        <?php } ?>

    });
</script>



<?php $this->load->view('footer') ?>
