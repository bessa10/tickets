<?php $this->load->view('header') ?>
<br><br>
<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('subscribed')): ?>
        <div class="alert alert-success" role="alert">
            Você se inscreveu neste ticket. Você receberá e-mails sobre alterações.
        </div>
        <?php elseif($this->session->flashdata('unsubscribed')): ?>
        <div class="alert alert-success" role="alert">
            Você cancelou a inscrição neste ticket.
        </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="ticket-id">Ticket Nº: <?= $ticket->tid ?></h2><br>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <h1><?= $ticket->title ?></h1>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <?php if(!isset($ticket->subscribed) || !$ticket->subscribed): ?>
                    <form method="post" class="notifications-form" action="<?= base_url() ?>notification/subscribe/<?= $ticket->tid ?>">
                        <button type="submit" class="btn btn-link pull-right edit"><i class="fa fa-bell-o" aria-hidden="true"></i>&nbsp;&nbsp;Notificações</button>
                    </form>
                <?php else: ?>
                    <form method="post" class="notifications-form" action="<?= base_url() ?>notification/unsubscribe/<?= $ticket->tid ?>">
                        <button type="submit" class="btn btn-link pull-right edit"><i class="fa fa-bell-slash-o" aria-hidden="true"></i>&nbsp;&nbsp;Notificações</button>
                    </form>
                <?php endif ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">
                <?php if($this->session->userdata('role') == 1): ?>
                    <?php if($ticket->sid != "-1" && $ticket->sid != "4"): ?>
                        <a href="<?= base_url() ?>ticket/edit/<?= $ticket->tid ?>" class="btn btn-primary pull-right edit"><i class="fa fa-edit"></i>&nbsp;&nbsp;Editar</a>
                    <?php endif ?>
                <?php endif ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table">
                    <tr>
                        <th width="100">Criado</th>
                        <td><?= date('d/m/Y h:iA', strtotime($ticket->created)) ?></td>
                    </tr>
                    <tr>
                        <th width="100">Iniciado</th>

                        <td><?= ($ticket->started != NULL) ? date('d/m/Y h:iA', strtotime($ticket->started)) : '-' ?></td>
                    </tr>
                    <tr>
                        <th width="100">Modificado</th>
                        <?php if(isset($versions[0])): ?>
                            <td><?= date('d/m/Y h:iA', strtotime($versions[0]->created)) ?></td>
                        <?php else: ?>
                            <td><?= date('d/m/Y h:iA', strtotime($ticket->created)) ?></td>
                        <?php endif ?>
                    </tr>
                    <tr>
                        <th>Autor</th>
                        <td><?= $ticket->author ?></td>
                    </tr>
                    <tr>
                        <th>Atendente</th>
                        <td><?= $ticket->worker ? $ticket->worker : '<i class="text-muted">-</i>' ?></td>
                    </tr>
                    <tr>
                        <th>Categoria</th>
                        <td><?= $ticket->category ?></td>
                    </tr>
                    <tr>
                        <th>Projeto</th>
                        <td><?= $ticket->project ? $ticket->project : '<i class="text-muted">-</i>' ?></td>
                    </tr>
                    <tr>
                        <th>Torre</th>
                        <td><?= $torre->torre ? $torre->torre : '<i class="text-muted">-</i>' ?></td>
                    </tr>
                    <tr>
                        <th>Classificação</th>
                        <td><?= $classificacao->classificacao ? $classificacao->classificacao : '<i class="text-muted">-</i>' ?></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
                    </tr>
                    <?php
                        $valor_gut = valor_gut($ticket->valor_gravidade,$ticket->valor_urgencia,$ticket->valor_tendencia);
                        $txt_gut = texto_gut($valor_gut);
                        $servico_gut = catalogo_servicos_gut($ticket->pid, $ticket->cid);
                    ?>
                    <?php if($valor_gut > 0): ?>
                        <?php if($valor_gut > 25): ?>
                        <tr>
                            <th><h3><strong>GUT&nbsp;<?= $valor_gut ?></strong></h3></th>
                            <td><h3><?= $txt_gut ?></h3></td>
                        </tr>
                        <?php else: ?>
                        <tr>
                            <th><h3><strong>GUT&nbsp;<?= $valor_gut ?></strong></h3></th>
                            <td><h6><?= $txt_gut ?></h6></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6><strong>Prazo para 1º atendimento:</strong>&nbsp;&nbsp;<span class="badge badge-secondary"><?= $servico_gut['txt_atendimento'] ?></span></h6><br>
                                <h6><strong>Prazo para solução:</strong>&nbsp;&nbsp;<span class="badge badge-secondary"><?= $servico_gut['txt_solucao'] ?></span></h6>
                            </td>
                        </tr>
                        <?php endif ?>
                    <?php endif ?>
                    <?php if($ticket->cid == 28 && $ticket->horas_previstas > 0){ ?>
                    <tr>
                        <td colspan="2">
                            <h6><strong>Horas previstas:</strong>&nbsp;&nbsp; <?= $ticket->horas_previstas . ' horas' ?></h6>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <?php 
            if($this->session->userdata('role') == 1) {
                $exc_ap             = true;
                $mostrar_iniciar    = true;
            } else {
                $exc_ap             = false;
                $mostrar_iniciar    = false;
            }

            $uids_apoio         = null;
        ?>
        <div class="form-group row">
            <?php if($ticket->worker_uid == $this->session->userdata('uid') && $ticket->sid != "-1" && $ticket->sid != "4"): ?>
                <div class="col-lg-6">
                    <?php if(count($workers_apoio) > 0): ?>
                    <button type="button" id="btn-solicitar-apoio" onclick="abrirApoio(1)" class="btn btn-primary"><i class="fas fa-user-plus"></i>&nbsp;&nbsp;Apoio no atendimento</button>
                    <?php endif ?>

                    <?php if(count($users_apoio) > 0): ?>
                    <br><br><button type="button" id="btn-solicitar-apoio2" onclick="abrirApoio(2)" class="btn btn-secondary"><i class="fas fa-user-plus"></i>&nbsp;&nbsp;Homologação, requisitos etc</button>
                    <?php endif ?>
                </div>
            <?php endif ?>

            <?php if(count($apoio_ticket) > 0): ?>
                <div class="col-lg-6">
                    <h6>Pessoas que estão em apoio:</h6>
                    <ul>
                    <?php 
                    foreach ($apoio_ticket as $apoio):

                        $uids_apoio[] = $apoio->uid;
                    ?>
                        <?php if($exc_ap == true): ?>
                            <li>
                                <?= ($apoio->role == 1) ? '<strong>'.$apoio->name.'</strong>' : $apoio->name ?>&nbsp;&nbsp;&nbsp;<a href="javascript:" style="" class="text-danger" onclick="excluirApoio(<?=$apoio->uid?>, '<?=$apoio->name?>')"><i class="fa fa-times"></i></a>
                                <?php if($apoio->tipo == 1){ ?>
                                <a onclick="iniciarHomolog(<?= $apoio->tid ?>,<?= $apoio->uid ?>)" class="btn btn-success ml-4 mb-2">Iniciar homologação</a>
                                <?php } ?>
                            </li>
                        <?php else : ?>
                            <li><?= ($apoio->role == 1) ? '<strong>'.$apoio->name.'</strong>' : $apoio->name ?></li>

                        <?php endif ?>
                    <?php 
                    endforeach 
                    ?>
                    </ul>
                </div>
            <?php endif ?>
        </div><hr>
        <?php
            $user_apoio = 2;

            if($atend_iniciado) {
                $disp1 = 'none';
                $disp2 = 'block';
            } else {
                $disp1 = 'block';
                $disp2 = 'none';
            }

            if($ticket->sid == "-1" || $ticket->sid == "4") {
                $disp1 = 'none';
                $disp2 = 'none';
            }

            // Caso o user logado estiver apoiando o ticket iremos mostrar os botões de iniciar parar
            if($ticket->worker_uid != $this->session->userdata('uid')) {
                if($uids_apoio) {
                    if(in_array($this->session->userdata('uid'), $uids_apoio)) {
                        
                        $mostrar_iniciar    = true;
                        $user_apoio         = 1;

                        if($atend_iniciado) {
                            $disp1 = 'none';
                            $disp2 = 'block';
                        } else {
                            $disp1 = 'block';
                            $disp2 = 'none';
                        }
                    }
                } else {
                    $disp1 = 'none';
                    $disp2 = 'none';
                }
            }

            $txt_iniciar = (!$verifica_timesheet_homolog) ? 'Iniciar' : 'Iniciar Homologação ou Apoio';

        ?>
        <?php if($mostrar_iniciar == true): ?>
            <?php if($verifica_timesheet_homolog): ?>
                <div class="form-group row">
                    <?php if ($ticket->tkt_aprovado == 1) { ?>
                    <div class="col-lg-12">
                        <h6>O ticket foi aprovado dia <strong><?= formata_data($ticket->dth_aprovado) ?></strong> por <strong><?= $ticket->name_aprovado ?>.</strong></h6>
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-3">
                        <button type="button" onclick="apvTicket()" class="btn btn-success" ><i class="fa fa-check"></i>&nbsp;&nbsp;Aprovar homologação</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" onclick="novoAjuste('<?= $this->session->uid ?>','<?= $ticket->tid ?>')" class="btn btn-warning" ><i class="fa fa-check"></i>&nbsp;&nbsp;Solicitar ajuste</button>
                    </div>
                    <?php } ?>
                </div><br>
            <?php endif ?>
            <div class="form-group row">
                <div class="col-lg-4">
                    <button type="button" id="btn-inicia-time" onclick="initAtendTimesheet(1, <?=$user_apoio?>)" style="display:<?=$disp1?>" class="btn btn-info" ><i class="fa fa-play"></i>&nbsp;&nbsp;<?= $txt_iniciar ?></button>

                    <button type="button" id="btn-finaliza-time" style="display:<?=$disp2?>" class="btn btn-secondary" onclick="initTimesheet(2)"><i class="fa fa-pause"></i>&nbsp;&nbsp;Parar</button>
                </div>
                <div class="col-lg-8">
                    <div id="msg-alert">
                    </div>
                </div>
            </div>
        <?php endif ?>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <h2>Descrição</h2><br>
                <div class="card">
                    <div class="card-body">
                        <?= $ticket->description ?>
                    </div>
                </div>
            </div>
        </div><br><br>
        <?php if($attachments): ?>
            <h2>Anexos</h2>
            <div class="row attachments">
                <?php foreach($attachments as $attachment): ?>
                    <div class="col-md-3">
                        <a href="<?= base_url("attachment/view/{$attachment->aid}") ?>">
                            <?php if($attachment->is_image == 'Y'): ?>
                                <div class="attachment-preview" style="background-image:url('<?= base_url("/attachments/{$attachment->filename}") ?>')"></div>
                            <?php else: ?>
                                <div class="attachment-preview"><i class="fa fa-file-o" aria-hidden="true"></i></div>
                            <?php endif ?>
                        </a>
                    </div>
                    <!--
                    <div class="col-md-3 attachment-info">
                        <a href="<?= base_url("attachment/view/{$attachment->aid}") ?>">
                            <div class="attachment-title"><?= $attachment->title ?></div>
                        </a>
                    </div>
                    -->
                <?php endforeach ?>
            </div><br>
        <?php endif ?>

        <div class="update">
            <?php if($ticket->sid != -1 && $ticket->sid != 4) { ?>
            <form method="post">
                <input type="hidden" name="tid" id="tid" value="<?= $ticket->tid ?>">
                <input type="hidden" value="<?= $ticket->author_uid ?>" name="author">
                <input type="hidden" value="<?= $ticket->sid ?>" name="status">
                <input type="hidden" value="<?= $ticket->worker_uid ?>" name="worker">
                <div class="form-group">
                    <label for="">Comentário</label>
                    <textarea class="form-control" id="comment" name="comment" rows="10"></textarea>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button type="submit" class="btn btn-primary btn-load"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar comentário</button>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                        <a href="<?= base_url() ?>"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
                    </div>
                </div>
            </form>
            <br>
            <?php }elseif ($last_version->created >= date('d/m/Y', strtotime('+7 days', strtotime($last_version->created))) && $ticket->sid == 4 ){ ?>
                <form method="post">
                    <input type="hidden" name="tid" id="tid" value="<?= $ticket->tid ?>">
                    <input type="hidden" value="<?= $ticket->author_uid ?>" name="author">
                    <input type="hidden" value="<?= $ticket->sid ?>" name="status">
                    <input type="hidden" value="<?= $ticket->worker_uid ?>" name="worker">
                    <div class="form-group">
                        <label for="">Comentário</label>
                        <textarea class="form-control" id="comment" name="comment" rows="10"></textarea>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <button type="submit" class="btn btn-primary btn-load">Salvar comentário</button>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="<?= base_url() ?>"><button type="button" class="btn btn-secondary">Voltar</button></a>
                        </div>
                    </div>
                </form>
                <br>
            <?php } else { ?>
                <p>O prazo para reabertura do chamado foi encerrado, clique <a href="<?=base_url('ticket/create') ?>">aqui</a> para abrir um novo chamado.</p><br>
            <?php } ?>
        </div>
        <div class="changes">
            <h2>Alterações</h2>
            <table class="table">
                <?php foreach($versions as $version): ?>
                <tr>
                    <td>
                        <?= date('d/m/y h:iA', strtotime($version->created)) ?> por <strong><?= $version->username ?></strong>
                        <br>
                        <?php if($this->session->userdata('role')==1): ?>
                            <?php foreach(json_decode($version->difference) as $key=>$value): ?>
                                <b><?= ucfirst($key) ?></b> <i class="text-muted">mudou de</i> <?= $value->antes ? $value->antes : "Nada" ?> <i class="text-muted">para</i> <?= $value->depois ? $value->depois : "Nada" ?><br />
                            <?php endforeach ?>
                        <?php endif ?>
                        <br>
                        <?php if(!empty($version->comment)): ?>
                            <strong>Comentário;</strong><br><?= $version->comment ?>
                        <?php endif ?>
                    </td>
                </tr>
                <?php endforeach ?>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" data-backdrop="static" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Iniciar timesheet</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/inicia_atendimento/'.$ticket->tid ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <?= selectDB('id_atividade','Atividade no ticket',$atividades,'nome_atividade','id_atividade',null,null,null) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <?= selectDB('locais','Local de trabalho',$locais_trabalho,'locais','local_id',null,null,'verifylocal(this.value)') ?>
                        </div>
                    </div>
                    <input type="hidden" name="tid" id="tid" value="<?= $ticket->tid ?>">
                    <input type="hidden" value="<?= $ticket->author_uid ?>" name="author">
                    <input type="hidden" value="3" name="status">
                    <input type="hidden" value="1" name="tipo" id="tipo">
                    <input type="hidden" name="user_apoio" id="user_apoio">
                    <input type="hidden" value="<?= $this->session->userdata('uid') ?>" name="worker">
                    <div class="validation-locals" style="display: none">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>&nbsp;&nbsp;Salvar local</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalApoio" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Solicitar apoio</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/adicionar_apoio/'.$ticket->tid ?>" method="post" id="frm_add_apoio">
                    <input type="hidden" name="tipo" id="tipo_apoio">
                    <div id="aviso_apoio" class="form-group row" style="display:none;">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                Por favor selecione no mínimo um usuário
                            </div>
                        </div>
                    </div>
                    <div id="users_adm">
                        <h6>Lista de usuários administradores</h6><hr>
                        <?php foreach($workers_apoio as $apoio): ?>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="checkbox" name="worker_apoio[]" value="<?= $apoio->uid ?>">&nbsp;&nbsp;<strong><?= $apoio->name ?></strong>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <br><br>
                    </div>
                    <div id="users_all" >
                        <h6>Lista de usuários comuns</h6><hr>
                        <div class="list_users_all" style="max-height:300px; overflow-x:auto; overflow-x:hidden;">
                            <?php foreach($users_apoio as $apoio): ?>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <input type="checkbox" name="worker_apoio[]" value="<?= $apoio->uid ?>">&nbsp;&nbsp;<strong><?= $apoio->name ?></strong>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                        <br><br>
                    </div>
                    <?//= selectDB('worker_apoio','Selecionar usuário para apoio',$workers_apoio,'name','uid',null,null,'verifyUser(this.value)') ?>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users" style="display:block">
                                <button type="button" class="btn btn-primary" onclick="validaFrmApoio()"><i class="fas fa-save"></i>&nbsp;&nbsp;Adicionar usuário</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalExcApoio" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Remover apoio</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/excluir_apoio/'.$ticket->tid ?>" method="post">
                    <input type="hidden" name="exc_uid" id="exc_uid">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Deseja realmente remover o usuário <strong><span id="txt_username"></span></strong> do apoio?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-secondary"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim, desejo excluir</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-primary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalAprovTkt" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Aprovar ticket</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/aprovar_tkt_kanban/'.$ticket->tid ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Deseja realmente aprovar o desenvolvimento do ticket <?= $ticket->tid ?>?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim, desejo aprovar</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalAprovTkt" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Aprovar ticket</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/aprovar_tkt_kanban/'.$ticket->tid ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Deseja realmente aprovar o desenvolvimento do ticket <?= $ticket->tid ?>?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim, desejo aprovar</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalAprovTkt" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Aprovar ticket</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/aprovar_tkt_kanban/'.$ticket->tid ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Deseja realmente aprovar o desenvolvimento do ticket <?= $ticket->tid ?>?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim, desejo aprovar</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modaliniciarHomolog" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Iniciar homologação</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/iniciar_homologacao/'?>" method="post">
                    <input type="hidden" name="tid" id="tid_value" value="">
                    <input type="hidden" name="uid" id="uid_value">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Deseja iniciar a homologação do ticket <?= $ticket->tid ?>?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim, desejo iniciar</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modalNovoAjuste" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Iniciar Ajuste</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url().'ticket/solicitacao_ajuste/'?>" method="post">
                    <input type="hidden" name="tid" id="tid_ajuste" value="">
                    <input type="hidden" name="user" id="uid_ajuste">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <h6>Qual ajuste você gostaria no ticket <?= $ticket->tid ?>?</h6>
                        </div>
                    </div><br>
                    <div class="form-group row">
                        <div class="col-12">
                             <textarea class="form-control" name="comment" id="exampleFormControlTextarea1" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="validation-users">
                                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Enviar</button>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function verifylocal(id_local){
        if(id_local != '') {
            $(".validation-locals").css("display", "block");
        }else{
            $(".validation-locals").css("display", "none");
        }
    }

    function verifyUser(uid){
        if(uid != '') {
            $(".validation-users").css("display", "block");
        }else{
            $(".validation-users").css("display", "none");
        }
    }

    function excluirApoio(uid, name) {

        $("#exc_uid").val(uid);
        $("#txt_username").html(name);
        $("#modalExcApoio").modal('show');
    }

    function iniciarHomolog(tid,uid) {
        $("#tid_value").val(tid);
        $("#uid_value").val(uid);
        $("#modaliniciarHomolog").modal('show');
    }

    function abrirApoio(tipo = 1) {

        $("#aviso_apoio").hide();
        $("input[type='checkbox'][name='worker_apoio[]']").prop('checked', false);

        if(tipo == 1) {
            console.log('admin');
            $("#users_all").hide();
            $("#users_adm").show();
            $("#tipo_apoio").val(0);

            $("#modalApoio").modal();

        } else {
            console.log('não admin');
            $("#users_adm").hide();
            $("#users_all").show();
            $("#tipo_apoio").val(1);

            $("#modalApoio").modal();
        }
    }

    function validaFrmApoio() {

        var f = document.getElementById("frm_add_apoio");
        var erro = false;
        $("#aviso_apoio").hide();

        if (!$("input[type='checkbox'][name='worker_apoio[]']").is(':checked') ){

            $("#aviso_apoio").show();
            erro = true;
        }

        if(erro == false) {

            $('#blanket, #aguarde').css('display','block');

            f.submit();
        }
    }

    function apvTicket() {

        $("#modalAprovTkt").modal('show');

    }

     function novoAjuste(uid,tid) {

        $("#modalNovoAjuste").modal('show');
        $("#uid_ajuste").val(uid);
        $("#tid_ajuste").val(tid);

    }

</script>

<?php $this->load->view('footer') ?>
