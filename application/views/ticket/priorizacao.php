<div class="col-sm-12 content">
	<h1>Cronograma</h1>
	<form action="<?=base_url().'ticket/priorizacao'?>" method="POST" id="frm_alt_ordem" name="frm_alt_ordem">
		<?php if($this->session->userdata('role') == "1" || $this->session->userdata('id_department') == "7"): ?>
		<div class="form-group row">
			<div class="col-lg-6">
				<button type="button" onclick="initAltOrdem()" class="btn btn-sm btn-info btn-alt-ordem"><i class="fas fa-sync-alt"></i>&nbsp;&nbsp;Alterar ordem</button>
				<button type="submit" onclick="//savAltOrdem()" class="btn btn-sm btn-primary btn-sav-ordem btn-load" style="display:none;"><i class="fas fa-save"></i>&nbsp;&nbsp;Salvar ordem</button>
				&nbsp;&nbsp;&nbsp;<button type="button" onclick="cancAltOrdem()" class="btn btn-sm btn-danger btn-canc-ordem btn-load" style="display:none;"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar edição</button>
			</div>
			<input type="text" name="acao" value="01" style="display:none;">
		</div>
		<?php endif ?>
		<?php if($tickets): ?>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center"><strong>Ordem</strong></th>
						<!--<th class="text-center"><strong>ult ordem</strong></th>-->
						<th class="text-center"><strong>Nº Ticket</strong></th>
						<th>Solicitante</th>
						<th>Depto. Solicitante</th>
						<th>Título Ticket</th>
						<th>Sistema</th>
						<th>Criado em</th>
						<!--<th>1ª interação</th>-->
						<th>Status</th>
						<th class="text-center">Horas previstas</th>
						<th class="text-center">Horas utilizadas</th>
						<th class="text-center">%</th>
						<!--
						<th class="text-center">Ult Antes Prev. Soluc</th>
						<th class="text-center">Ult Prev. Soluc</th>
						-->
						<th class="text-center">Prev. Soluc</th>
						<th>Dev Owner</th>
						<th>Apoio</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach($tickets as $ticket): 
							$porc_horas = 0;
							$porc_horas = ($ticket['horas_utilizadas'] * 100) / $ticket['horas_previstas'];
					?>
						<input type="text" name="cod_tid[]" value="<?= $ticket['tid'] ?>" style="display:none;">
						<input type="text" name="title[]" value="<?= $ticket['titulo_ticket'] ?>" style="display:none;">
						<tr <?= ($ticket['atrasado'] == 1) ? 'style="background-color:#ffcdd2"' : '' ?>>
							<td class="text-center">
								<div class="txt-ordem">
									<?= ($ticket['ordem'] != null) ? $ticket['ordem'] : '<span style="display:none">999</span>' ?>
								</div>
								<div class="div-ordem" style="display:none;">
									<input type="text" class="form-control inp-ordem" name="ordem[]" value="<?= ($ticket['ordem']!=null)?$ticket['ordem']:'' ?>" style="width:50px;height:30px;" readonly="true">
								</div>
							</td>
							<!--<td><?= $ticket['ult_ordem'] ?></td>-->
							<td class="text-center"><strong><?= $ticket['tid'] ?></strong></td>
							<td><?= $ticket['name_author'] ?></td>
							<td><?= $ticket['dep_author'] ?></td>
							<td><a href="<?= base_url() ?>ticket/view/<?= $ticket['tid'] ?>" target="_blank"><?= $ticket['titulo_ticket'] ?></a></td>
							<td><?= $ticket['project'] ?></td>
							<td><?= formata_data($ticket['created']) ?></td>
							<!--
							<td><?= ($ticket['primeira_interacao'] != null) ? formata_data_hora($ticket['primeira_interacao']) : '' ?></td>
							-->
							<td class="text-center"><span class="badge badge-<?=$ticket['cor_status']?>"><?= $ticket['status'] ?></span></td>
							<td class="text-center"><?= $ticket['horas_previstas'] ?></td>
							<td class="text-center"><?= ($ticket['horas_utilizadas'] > 0) ? $ticket['horas_utilizadas'] : 0 ?></td>
							<td class="text-center"><?= formata_porc($porc_horas).'%' ?></td>
							<!--
							<td class="text-center"><?= ($ticket['ult_antes_dt_prev']!=null)?formata_data_hora($ticket['ult_antes_dt_prev']):'' ?></td>
							<td class="text-center"><?= ($ticket['ult_data_prevista']!=null)?formata_data_hora($ticket['ult_data_prevista']):'' ?></td>
							-->
							<td class="text-center"><?= ($ticket['data_prevista']!=null)?formata_data($ticket['data_prevista']):'' ?></td>
							<td><?= $ticket['dev_alocado'] ?></td>
							<td>
								<?php if($ticket['users_apoio']!='') { ?>
									&nbsp;&nbsp;<a href="javascript:" onclick="mostrarApoio('<?=$ticket['tid']?>','<?=$ticket['users_apoio']?>')" class=""><i class="fas fa-users"></i></a>
								<?php } ?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>Nenhum ticket foi encontrado...</p>
		<?php endif ?>
	</form>
</div>

<div class="modal fade" id="modalApoio" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            	<h5 class="modal-title">Apoio Ticket <strong><span id="txt_tid"></span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            	<div class="row">
                	<div class="col-lg-12">
                		<div id="div_table"></div>
                	</div>
            	</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#div-container").removeClass('container');
	});

	function initAltOrdem() {

		$(".txt-ordem").hide();
		$(".div-ordem").show();

		$(".inp-ordem").prop('readonly', false);
		$(".btn-alt-ordem").hide();
		$(".btn-canc-ordem").show();
		$(".btn-sav-ordem").show();
	}

	function cancAltOrdem() {
		$(".inp-ordem").prop('readonly', true);
		window.location.reload();
	}

	function savAltOrdem() {

		$("#frm_alt_ordem").submit();
	}

	function mostrarApoio(tid, users_apoio) {

		$("#txt_tid").html(tid);

		var html = '';

		html += '<table class="table table-bordered table-striped">';

		html += users_apoio;

		html += '</table>';

		$("#div_table").html(html);

		$("#modalApoio").modal('show');
	}

</script>