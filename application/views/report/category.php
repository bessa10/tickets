<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Tickets por Categoria</h1>

	<form method="GET" class="form-inline">
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group-inline">
					<select name="category" class="form-control">
						<?php foreach($categories as $category): ?>
							<option value="<?php echo $category->cid ?>" <?php if($this->input->get('category') == $category->cid) echo 'selected' ?>><?php echo $category->name ?></option>
						<?php endforeach ?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="form-control btn btn-primary"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
				</div>
			</div>
		</div>
	</form>
	<br>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Nº</th>
				<th>Título</th>
				<th>Status</th>
				<th>Autor</th>
				<th>Criado</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!count($tickets)): ?>
				<tr>
					<td>Nenhum ticket foi encontrado...</td>
				</tr>
			<?php endif ?>
			<?php foreach( $tickets as $ticket ): ?>
				<tr>
					<td><?php echo $ticket->tid ?></td>
					<td><a href="<?php echo base_url() ?>ticket/view/<?php echo $ticket->tid ?>"><?php echo $ticket->title ?></a></td>
					<td><?php echo $ticket->label ?></td>
					<td><?php echo $ticket->username ?></td>
					<td><?php echo date('d/m/Y', strtotime($ticket->created)) ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php $this->load->view('footer') ?>