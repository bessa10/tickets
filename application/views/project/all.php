<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Projetos</h1>

	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nome do projeto</th>
				<th>Ações</th>
                <th>Visível</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $projects as $project ): ?>
				<tr>
					<td><?php echo $project->name ?></td>
					<td>
						<?php //if($this->Roles_Model->has_permission('project', 2)): ?>
							<form method="post" action="<?php echo base_url() ?>/project/remove/<?php echo $project->pid ?>">
								<a href="<?php echo base_url() ?>project/edit/<?php echo $project->pid ?>"><button type="button" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja excluir o projeto?')"><i class="fa fa-times" aria-hidden="true"></i></button>
							</form>
						<?php //endif ?>
					</td>
                    <td><?= ($project->visivel_geral == '1' ? 'Sim' : 'Não') ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<a href="<?php echo base_url() ?>project/create/"><button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Novo Projeto</button></a>
</div>

<?php $this->load->view('footer') ?>
