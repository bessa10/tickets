<?php $this->load->view('header') ?>

<div class="col-sm-12">
	<h1>Novo Projeto</h1>

	<?php if($this->session->flashdata('error')): ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php endif ?>

	<form method="post">
		<div class="row">
		    <div class="form-group col-lg-12 <?php if(form_error('name')) echo 'has-danger' ?>">
		        <label for="">Nome do projeto</label>
		        <input type="text" class="form-control" id="name" name="name" placeholder="Nome do projeto">
		        <?php echo form_error('name') ?>
		    </div>
		</div>
        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('visivel')) echo 'has-danger' ?>">
                <label>Projeto visível</label>
                <div>
                    <div class="form-check form-check-inline">
                        <input type="radio" id=""  name="visivel_geral" value="0">Não
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" id=""  name="visivel_geral" value="1">Sim
                    </div>
                </div>
                <?php echo form_error('visivel') ?>
            </div>
        </div>
        <div class="row" style="flex-direction: column;margin: 0px;">
            <label>Categorias</label>
            <?= htmlCheckboxCategorias() ?>
        </div><br>
	    <div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	    		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
	    	</div>
	    	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
				<a href="<?php echo base_url() ?>project/all/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
			</div>
		</div>
	</form>
</div>

<?php $this->load->view('footer') ?>
