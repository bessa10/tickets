<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo empty($title) ? '' : $title . ' | ' ?>Help</title>

    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">-->
    <!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/bootstrap-4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>vendor/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/toggle-switch.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/style_novo.css?v=<?=VERSION.'.'.date('YmdHis')?>">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/dragula.css">
    <!--<link rel="stylesheet" href="<?php echo base_url() ?>css/datatables.css">-->
    <script src="<?php echo base_url() ?>/js/jquery.min.js"></script>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />-->

    <style type="text/css">
        <?php echo $this->css ?>
    </style>
</head>
<body>
    <style type="text/css">
        #blanket,#aguarde {
            position: fixed;
            display: none;
        }

        #blanket {
            left: 0;
            top: 0;
            background-color: #DCDCDC;
            filter: alpha(opacity = 65);
            height: 100%;
            width: 100%;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=65)";
            opacity: 0.30;
            z-index: 9998;
        }

        #aguarde {
            width: 500px;
            height: 300px;
            top: 35%;
            left: 45%;
            background: url("<?=base_url().'images/loading.gif'?>") no-repeat 0 50%; // o gif que desejar, eu geralmente uso um 20x20
            line-height: 30px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            z-index: 9999;
            padding-left: 27px;
        }
    </style>

    <div id="blanket"></div>
    <div id="aguarde"></div>

<?php if($this->session->userdata('uid')): ?>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="<?php echo base_url() ?>">
                <img src="<?= base_url().'images/logo.png' ?>" style="max-width:120px;" class=" align-top" loading="lazy">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="btn btn-primary btn-sm btn-novo-ticket text-light my-2 my-sm-0" href="<?php echo base_url() ?>ticket/create"><i class="fa fa-plus"></i>&nbsp;&nbsp;Ticket</a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="btn btn-info btn-sm btn-novo-ticket text-light my-2 my-sm-0" href="<?php echo base_url() ?>ticket/novo_desenvolvimento"><i class="fa fa-plus"></i>&nbsp;&nbsp;Melhoria & Inovação</a>
                    </li>
                    -->
                    <!--
                    <li class="nav-item">
                        <a class="btn btn-secondary btn-sm btn-novo-ticket text-light my-2 my-sm-0" href="<?php echo base_url() ?>ticket/migracao"><i class="fa fa-plus"></i>&nbsp;&nbsp;Migração</a>
                    </li>
                    -->
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url() ?>cronograma">Cronograma</a>
                    </li>
                    <?php if($this->session->userdata('role')=="1"): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownTicket" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tickets
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownTicket">
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/kanban">Kanban</a>
                                <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/all">Todos os tickets</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/status">Por status</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/user">Por usuário</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/category">Por categoria</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/project">Por projeto</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/advanced">Busca avançada</a>
                            <?php if($this->session->userdata('role')=="1"): ?>
                            <a class="dropdown-item" href="<?php echo base_url() ?>research/all">Pesquisa satisfação</a>
                            <?php  endif ?>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownConfig" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configurações
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownConfig">
                            <a class="dropdown-item" href="<?php echo base_url() ?>status/all">Status</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>category/all">Categorias</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>project/all">Projetos</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>user/all">Usuários</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>role/all">Perfis de usuário</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>settings/">Sistema</a>
                        </div>
                    </li>
                    <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url() ?>ticket/me">Meus tickets</a>
                    </li>
                    <?php endif ?>
                </ul>
            </div>
            <form class="form-inline my-2 my-lg-0">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuUser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $this->session->userdata('username') ?>
                        </a>
                        <div class="dropdown-menu user-nav" aria-labelledby="navbarDropdownMenuUser">
                            <a class="dropdown-item" href="<?php echo base_url() ?>ticket/me">Meus tickets</a>
                            <a class="dropdown-item" href="<?php echo base_url() ?>user/edit">Conta</a>
                            <?php if(ENVIRONMENT == 'development'): ?>
                                <a class="dropdown-item" href="<?php echo base_url() ?>logout/">Sair</a>
                            <?php endif ?>
                        </div>
                    </li>
                </ul>
                <!--
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                -->
            </form>
        </div>
    </nav>
<?php endif ?>
<br><br><br><br>
<div class="col-lg-12">
    <div id="div-container" class="container">
        <?php if($this->session->flashdata()): ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-<?=$this->session->flashdata('type')?>" id="alert">
                    <strong><?= $this->session->flashdata('msg') ?></strong>
                </div>
            </div>
        </div>
        <?php endif ?>
