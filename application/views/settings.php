<?php $this->load->view('header') ?>

<div class="col-sm-12">
    <h1>Configurações do sistema</h1>
    <form method="post" action="<?php echo base_url() ?>/settings/edit/">
        <div class="row">
            <div class="form-group col-lg-12 col-lg-12">
                <label for="">Estilo personalizado (CSS)</label>
                <textarea class="form-control" name="css" rows="10"><?php echo $settings->css ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('start_status')) echo 'has-danger' ?>">
                <label for="start_status">Status do ticket na criação</label>
                <select class="form-control" id="start_status" name="start_status">
                    <?php foreach($statuses as $status): ?>
                        <option value="<?php echo $status->sid ?>" <?php if($settings->start_status == $status->sid) echo 'selected' ?>><?php echo $status->label ?></option>
                    <?php endforeach ?>
                </select>
                <small class="form-text text-muted">Quando um ticket é criado, ele recebe este status</small>
                <?php echo form_error('start_status') ?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('work_start_status')) echo 'has-danger' ?>">
                <label for="work_start_status">Status de ticket em atendimento</label>
                <select class="form-control" id="work_start_status" name="work_start_status">
                    <?php foreach($statuses as $status): ?>
                        <option value="<?php echo $status->sid ?>" <?php if($settings->work_start_status == $status->sid) echo 'selected' ?>><?php echo $status->label ?></option>
                    <?php endforeach ?>
                </select>
                <small class="form-text text-muted">Status em que indica que o ticket foi para atendimento.</small>
                <?php echo form_error('work_start_status') ?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('work_complete_status')) echo 'has-danger' ?>">
                <label for="work_complete_status">Status do ticket na conclusão</label>
                <select class="form-control" id="work_complete_status" name="work_complete_status">
                    <?php foreach($statuses as $status): ?>
                        <option value="<?php echo $status->sid ?>" <?php if($settings->work_complete_status == $status->sid) echo 'selected' ?>><?php echo $status->label ?></option>
                    <?php endforeach ?>
                </select>
                <small class="form-text text-muted">Status que indica que foi concluído</small>
                <?php echo form_error('work_complete_status') ?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-12 <?php if(form_error('next_up_statuses')) echo 'has-danger' ?>">
                <label for="next_up_statuses">Página inicial status de "próximo..."</label>
                <select name="next_up_statuses[]" id="next_up_statuses" multiple="multiple" class="form-control">
                    <?php foreach($statuses as $status): ?>
                        <option value="<?php echo $status->sid ?>" <?php if(in_array($status->sid, $next_up_statuses)) echo 'selected' ?>><?php echo $status->label ?></option>
                    <?php endforeach ?>
                </select>
                <small class="form-text text-muted">Status que significam que o ticket deve ser atendido</small>
                <?php echo form_error('next_up_statuses') ?>
            </div>
        </div><br>
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                <a href="<?= base_url() ?>"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
            </div>
        </div>

    </form>
</div>

<?php $this->load->view('footer') ?>