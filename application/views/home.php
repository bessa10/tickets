<?php $this->load->view('header') ?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<br><h1>Sistema de gerenciamento de tickets</h1><br>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2><strong>Resumo Geral</strong></h2><br>
		<?php if(empty($summary)): ?>
			<p>Clique em "Novo ticket" acima para criar um chamado.</p>
		<?php else: ?>
		<table class="summary table table-bordered">
			<thead class="bg-primary text-white">
				<tr>
					<?php foreach($summary as $status): ?>
						<td class="text-center"><strong><?= $status->label ?></strong></td>
					<?php endforeach ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php foreach($summary as $status): ?>
						<td class="text-center"><strong><?= $status->count ?></strong></td>
					<?php endforeach ?>
				</tr>
			</tbody>
		</table>
		<?php endif ?>
	</div>
</div>
<br>
<?php if($this->session->userdata('role')=="1"): ?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2><strong>Tickets não atribuídos - <?= count($tickets_nao_atribuidos) ?></strong></h2>
		<?php if(!count($tickets_nao_atribuidos)): ?>
			<p>Não existem tickets para atribuir...</p>
		<?php else: ?>
			<table class="table table-striped table-bordered" id="tabela-nao-atribuido">
				<thead>
					<tr>
						<th class="text-center">Ticket</th>
						<th>Título</th>
						<th>Projeto</th>
						<th>Autor</th>
						<th class="text-center">Criado</th>
						<th class="text-center">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($tickets_nao_atribuidos as $ticket ): ?>
						<tr>
							<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
							<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
							<td><?= $ticket->project ?></td>
							<td><?= $ticket->author ?></td>
							<td class="text-center"><?= formata_data_hora($ticket->created) ?></td>
							<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
	</div>
</div><br>
<?php endif ?>

<div class="row">
	<div class="col-lg-12">
		<h2><strong>Mudanças Recentes</strong></h2><br>
		<?php if(!count($recent)): ?>
			<p>Não existem tickets alterados recentemente.</p>
		<?php else: ?>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th class="text-center">Ticket</th>
					<th>Título</th>
					<th>Projeto</th>
					<th>Autor</th>
					<th class="text-center">Status</th>
					<th class="text-center">Modificado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $recent as $ticket ): ?>
					<tr>
						<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
						<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
						<td><?= $ticket->project ?></td>
						<td><?= $ticket->author ?></td>
						<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
						<td class="text-center"><?= formata_data_hora($ticket->date) ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php endif ?>
	</div>
</div><br>

<?php if($this->session->userdata('role')=="1"): ?>
	<?php
		$count_tickets_atr 	= count($tickets_atribuidos);
		$count_tickets_kan 	= count($tickets_kanban);
		$count_tickets_ap	= count($tickets_em_apoio);
	?>
	<div class="row">
		<div class="col-lg-12">
			<h2>Tickets atribuídos a mim - <?= $count_tickets_atr + $count_tickets_kan ?></h2><br>
			<form method="post">
				<div class="row">
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="tid" placeholder="Nº ticket" value="<?=$filtro['tid']?>">
					</div>
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="title" placeholder="Título" value="<?=$filtro['title']?>">
					</div>
					<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<?= selectDB('project',null,$projects,'name','pid',$filtro['project'],null,null,'Projetos') ?>
					</div>
					<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<?= selectDB('author',null,$users,'name','uid',$filtro['author'],null,null,'Autor') ?>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
						<div class="input-group">
		  					<div class="input-group-prepend">
		    					<span class="input-group-text bg-primary" id="basic-addon1">DE</span>
		  					</div>
		  					<input type="date" class="form-control" name="data1" value="<?=$filtro['data1']?>" aria-label="Username" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-5">
						<div class="input-group">
		  					<div class="input-group-prepend">
		    					<span class="input-group-text bg-primary" id="basic-addon1">ATÉ</span>
		  					</div>
		  					<input type="date" class="form-control" name="data2" value="<?=$filtro['data2']?>" aria-label="Username" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-block btn-primary btn-load"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filtrar</button>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
						<a href="<?=base_url()?>" class="btn btn-block btn-outline-secondary btn-load"><i class="fa fa-trash"></i>&nbsp;&nbsp;Limpar</a>
					</div>
				</div>
			</form>
		</div>
	</div><br>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<?php if($count_tickets_atr > 0): ?>
	  			<li class="nav-item" role="presentation">
	    			<a class="nav-link active item-tabs-tickets" id="home-tab" data-toggle="tab" href="#chamados" role="tab" aria-controls="home" aria-selected="true">CHAMADOS&nbsp;&nbsp;<span class="badge badge-secondary"><?= $count_tickets_atr ?></span></a>
	  			</li>
	  			<?php endif ?>
	  			<?php if($count_tickets_kan > 0): ?>
	  			<li class="nav-item" role="presentation">
	    			<a class="nav-link item-tabs-tickets" id="profile-tab" data-toggle="tab" href="#kanban" role="tab" aria-controls="profile" aria-selected="false">DESENVOLVIMENTO&nbsp;&nbsp;<span class="badge badge-secondary"><?= $count_tickets_kan ?></span></a>
	  			</li>
	  			<?php endif ?>
	  			<?php if($count_tickets_ap > 0): ?>
	  			<li class="nav-item" role="presentation">
	    			<a class="nav-link item-tabs-tickets" id="profile-tab" data-toggle="tab" href="#apoio" role="tab" aria-controls="profile" aria-selected="false">TICKETS EM APOIO&nbsp;&nbsp;<span class="badge badge-secondary"><?= $count_tickets_ap ?></span></a>
	  			</li>
	  			<?php endif ?>
			</ul>
			<div class="tab-content" id="myTabContent">
				<?php if($count_tickets_atr > 0): ?>
	  			<div class="tab-pane fade show active" id="chamados" role="tabpanel" aria-labelledby="home-tab">
					<table class="table table-striped table-bordered" id="tabela-atribuido">
						<thead>
							<tr>
								<th class="text-center"><strong>Ticket</strong></th>
								<th>Título</th>
								<th>Projeto</th>
								<th>Autor</th>
								<th class="text-center">Criado</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($tickets_atribuidos as $ticket): ?>
								<tr>
									<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
									<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
									<td><?= $ticket->project ?></td>
									<td><?= $ticket->author ?></td>
									<td class="text-center"><?= formata_data($ticket->created) ?></td>
									<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
	  			</div>
	  			<?php endif ?>
	  			<?php if($count_tickets_kan > 0):
	            	$active_kan = ($count_tickets_kan > 0 && $count_tickets_atr == 0) ? 'active' : '';
	            ?>
	  			<div class="tab-pane fade <?= $active_kan ?>" id="kanban" role="tabpanel" aria-labelledby="profile-tab">
	  				<table class="table table-striped table-bordered" id="tabela-kanban">
						<thead>
							<tr>
								<th class="text-center"><strong>Ticket</strong></th>
								<th>Título</th>
								<th>Projeto</th>
								<th>Autor</th>
								<th class="text-center">Criado</th>
								<th class="text-center">GUT</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($tickets_kanban as $ticket): ?>
								<?php
									$valor_gut = 0;
									$valor_gut = valor_gut($ticket->valor_gravidade,$ticket->valor_urgencia,$ticket->valor_tendencia);
								?>
								<tr>
									<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
									<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
									<td><?= $ticket->project ?></td>
									<td><?= $ticket->author ?></td>
									<td class="text-center"><?= formata_data($ticket->created) ?></td>
									<td class="text-center"><strong><?= $valor_gut ?></strong></td>
									<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
	  			</div>
	  			<?php endif ?>

	  			<?php if($count_tickets_ap > 0):
	            	$active_ap = ($count_tickets_ap > 0 && $count_tickets_kan == 0 && $count_tickets_atr == 0) ? 'active' : '';
	            ?>
	  			<div class="tab-pane fade <?= $active_ap ?>" id="apoio" role="tabpanel" aria-labelledby="profile-tab">
	  				<table class="table table-striped table-bordered" id="tabela-apoio">
						<thead>
							<tr>
								<th class="text-center"><strong>Ticket</strong></th>
								<th>Título</th>
								<th>Projeto</th>
								<th>Autor</th>
								<th class="text-center">Criado</th>
								<th class="text-center">GUT</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($tickets_em_apoio as $ticket): ?>
								<?php
									$valor_gut = 0;
									$valor_gut = valor_gut($ticket->valor_gravidade,$ticket->valor_urgencia,$ticket->valor_tendencia);
								?>
								<tr>
									<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
									<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
									<td><?= $ticket->project ?></td>
									<td><?= $ticket->author ?></td>
									<td class="text-center"><?= formata_data($ticket->created) ?></td>
									<td class="text-center"><strong><?= $valor_gut ?></strong></td>
									<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
	  			</div>
	  			<?php endif ?>
			</div>
		</div>
	</div>
<?php else: ?>
	<?php
		$count_meus_tickets = count($meus_tickets);
		$count_tickets_ap	= count($tickets_em_apoio);
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<?php if($count_meus_tickets > 0): ?>
	  			<li class="nav-item" role="presentation">
	    			<a class="nav-link active item-tabs-tickets" id="home-tab" data-toggle="tab" href="#meus_chamados" role="tab" aria-controls="home" aria-selected="true">MEUS CHAMADOS&nbsp;&nbsp;<span class="badge badge-secondary"><?= $count_meus_tickets ?></span></a>
	  			</li>
	  			<?php endif ?>
	  			<?php if($count_tickets_ap > 0): ?>
	  			<li class="nav-item" role="presentation">
	    			<a class="nav-link item-tabs-tickets" id="profile-tab" data-toggle="tab" href="#em_apoio" role="tab" aria-controls="profile" aria-selected="false">TICKETS EM APOIO&nbsp;&nbsp;<span class="badge badge-secondary"><?= $count_tickets_ap ?></span></a>
	  			</li>
	  			<?php endif ?>
			</ul>
			<div class="tab-content" id="myTabContent">
				<?php if($count_meus_tickets > 0): ?>
	  			<div class="tab-pane fade show active" id="meus_chamados" role="tabpanel" aria-labelledby="home-tab">
					<table class="table table-striped table-bordered" id="tabela-atribuido">
						<thead>
							<tr>
								<th class="text-center"><strong>Ticket</strong></th>
								<th>Título</th>
								<th>Projeto</th>
								<th>Autor</th>
								<th class="text-center">Criado</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($meus_tickets as $ticket): ?>
								<tr>
									<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
									<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
									<td><?= $ticket->project ?></td>
									<td><?= $ticket->author ?></td>
									<td class="text-center"><?= formata_data($ticket->created) ?></td>
									<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
	  			</div>
	  			<?php endif ?>
	  			<?php if($count_tickets_ap > 0):
	            	$active_kan = ($count_tickets_ap > 0 && $count_meus_tickets == 0) ? 'active' : '';
	            ?>
	  			<div class="tab-pane fade <?= $active_kan ?>" id="em_apoio" role="tabpanel" aria-labelledby="profile-tab">
	  				<table class="table table-striped table-bordered" id="tabela-kanban">
						<thead>
							<tr>
								<th class="text-center"><strong>Ticket</strong></th>
								<th>Título</th>
								<th>Projeto</th>
								<th>Autor</th>
								<th class="text-center">Criado</th>
								<th class="text-center">GUT</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($tickets_em_apoio as $ticket): ?>
								<?php
									$valor_gut = 0;
									$valor_gut = valor_gut($ticket->valor_gravidade,$ticket->valor_urgencia,$ticket->valor_tendencia);
								?>
								<tr>
									<td class="text-center"><strong><?= $ticket->tid ?></strong></td>
									<td><a href="<?= base_url() ?>ticket/view/<?= $ticket->tid ?>"><?= $ticket->title ?></a></td>
									<td><?= $ticket->project ?></td>
									<td><?= $ticket->author ?></td>
									<td class="text-center"><?= formata_data($ticket->created) ?></td>
									<td class="text-center"><strong><?= $valor_gut ?></strong></td>
									<td class="text-center"><span class="badge badge-<?=$ticket->cor_status?>"><?= $ticket->status ?></span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
	  			</div>
	  			<?php endif ?>
			</div>
		</div>
	</div>
<?php endif ?>
<?php $this->load->view('footer') ?>
